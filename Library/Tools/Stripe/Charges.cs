﻿using System.Collections.Generic;

namespace Library.Stripe {
	public class Charges {

		public Charges(){}

		// Update, Delete charges not abailable..

		private static global::Stripe.StripeChargeService chargeService = new global::Stripe.StripeChargeService(StripeApiKey.ApiKey);

		public static global::Stripe.StripeCharge Get(string chargeId, global::Stripe.StripeRequestOptions requestOptions = null){
			global::Stripe.StripeCharge charge = chargeService.Get(chargeId);
			return charge;
		}

		public static IEnumerable<global::Stripe.StripeCharge> List(global::Stripe.StripeChargeListOptions listOptions = null, global::Stripe.StripeRequestOptions requestOptions = null){
            IEnumerable<global::Stripe.StripeCharge> charges = chargeService.List();
			return charges;
		}

		public static global::Stripe.StripeCharge Capture(string chargeId, int? captureAmount = null, int? applicationFee = null, global::Stripe.StripeRequestOptions requestOptions = null){
			global::Stripe.StripeCharge stripeCharge = chargeService.Capture(chargeId, captureAmount, applicationFee);
			return stripeCharge;
		}

		public static global::Stripe.StripeCharge Create(global::Stripe.StripeChargeCreateOptions charge, global::Stripe.StripeRequestOptions requestOptions = null){
			global::Stripe.StripeCharge stripeCharge = chargeService.Create(charge);
			return stripeCharge;
		}
	}
}
