﻿using System.Collections.Generic;

namespace Library.Stripe {
	public class Customer {
		public Customer(){}

		private static global::Stripe.StripeCustomerService customerService = new global::Stripe.StripeCustomerService(StripeApiKey.ApiKey);

		public static global::Stripe.StripeCustomer Get(string customerId, global::Stripe.StripeRequestOptions requestOptions = null){
			global::Stripe.StripeCustomer customer = customerService.Get(customerId);
			return customer;
		}

		public static IEnumerable<global::Stripe.StripeCustomer> List(global::Stripe.StripeCustomerListOptions listOptions = null, global::Stripe.StripeRequestOptions requestOptions = null){
            IEnumerable<global::Stripe.StripeCustomer> customers = customerService.List();
			return customers;
		}

		public static global::Stripe.StripeCustomer Create(global::Stripe.StripeCustomerCreateOptions customer, global::Stripe.StripeRequestOptions requestOptions = null){
			global::Stripe.StripeCustomer stripeCustomer = customerService.Create(customer);
			return stripeCustomer;
		}

		public static global::Stripe.StripeCustomer Update(string customerId, global::Stripe.StripeCustomerUpdateOptions customer, global::Stripe.StripeRequestOptions requestOptions = null){
			global::Stripe.StripeCustomer stripeCustomer = customerService.Update(customerId, customer);
			return stripeCustomer;
		}

		public static void Delete(string customerId, global::Stripe.StripeRequestOptions requestOptions = null){
			customerService.Delete(customerId);
		}
	}
}
