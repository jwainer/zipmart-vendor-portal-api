﻿namespace Library.Stripe {
	public static class StripeApiKey {
        public static string ApiKey = System.Configuration.ConfigurationManager.AppSettings["StripeApiKey"].ToString();
    }
}
