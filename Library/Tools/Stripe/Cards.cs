﻿using System.Collections.Generic;

namespace Library.Stripe {
	public class Cards {

		public Cards(){}

		private static global::Stripe.StripeCardService cardService = new global::Stripe.StripeCardService(StripeApiKey.ApiKey);

		public static global::Stripe.StripeCard Get(string customerOrRecipientId, string cardId, bool isRecipient = false, global::Stripe.StripeRequestOptions requestOptions = null){
			global::Stripe.StripeCard card = cardService.Get(customerOrRecipientId, cardId);
			return card;
		}

		public static IEnumerable<global::Stripe.StripeCard> List(string customerOrRecipientId, global::Stripe.StripeListOptions listOptions = null, bool isRecipient = false, global::Stripe.StripeRequestOptions requestOptions = null){
            IEnumerable<global::Stripe.StripeCard> cards = cardService.List(customerOrRecipientId);
			return cards;
		}

		public static global::Stripe.StripeCard Create(string customerOrRecipientId, global::Stripe.StripeCardCreateOptions card, bool isRecipient = false, global::Stripe.StripeRequestOptions requestOptions = null){
			global::Stripe.StripeCard stripeCard = cardService.Create(customerOrRecipientId, card);
			return stripeCard;
		}

		public static global::Stripe.StripeCard Update(string customerOrRecipientId, string cardId, global::Stripe.StripeCardUpdateOptions card, bool isRecipient = false, global::Stripe.StripeRequestOptions requestOptions = null){
			global::Stripe.StripeCard stripeCard = cardService.Update(customerOrRecipientId, cardId, card);
			return stripeCard;
		}

		public static void Delete(string customerOrRecipientId, string cardId, bool isRecipient = false, global::Stripe.StripeRequestOptions requestOptions = null){
			cardService.Delete(customerOrRecipientId, cardId);
		}
	}
}
