﻿using System.Drawing;
using System.Drawing.Drawing2D;

namespace Library.Tools {
    public class Image {
		public int Width = 0;
		public int Height = 0;
    
		public bool Resize(string source, int width, int height, string dest){
            System.Drawing.Image newImage = System.Drawing.Image.FromFile(source);
            System.Drawing.Bitmap MyImage = (System.Drawing.Bitmap)newImage;         
            this.Width = width;
            this.Height = height;
            		
			try {
				if (MyImage.Height == this.Height && MyImage.Width == this.Width){MyImage.Dispose(); return false;} 
				if (MyImage.Height >= Height || MyImage.Width >= Width){MyImage = this.sizeToFit(MyImage);}
				MyImage = this.insertBackground(MyImage);		
			} catch {}

            MyImage.Save(dest);
            MyImage.Dispose();
            return true;
		}    
           
		private System.Drawing.Bitmap sizeToFit(System.Drawing.Bitmap imgToResize){
			int sourceWidth = imgToResize.Width;
			int sourceHeight = imgToResize.Height;
			float nPercent = 0;
			float nPercentW = 0;
			float nPercentH = 0;

			nPercentW = ((float)Width / (float)sourceWidth);
			nPercentH = ((float)Height / (float)sourceHeight);

			if (nPercentH < nPercentW){nPercent = nPercentH;} else {nPercent = nPercentW;}

			int destWidth = (int)(sourceWidth * nPercent);
			int destHeight = (int)(sourceHeight * nPercent);

			System.Drawing.Bitmap b = new System.Drawing.Bitmap(destWidth, destHeight);
			Graphics g = Graphics.FromImage(b);
			g.InterpolationMode = InterpolationMode.HighQualityBicubic;
			g.DrawImage(imgToResize, 0, 0, destWidth, destHeight);
			g.Dispose();

			return b;
		}        

		private System.Drawing.Bitmap insertBackground(System.Drawing.Bitmap MyImage){
			int x = (this.Width - MyImage.Width) / 2;
			int y = (this.Height - MyImage.Height) / 2;
			
			System.Drawing.Bitmap image = new System.Drawing.Bitmap(this.Width, this.Height);

			using (var g = Graphics.FromImage(image)){
				g.Clear(Color.Black);
				g.InterpolationMode = InterpolationMode.HighQualityBicubic;
				g.DrawImage(MyImage, x, y, MyImage.Width, MyImage.Height);
			}

			return image;
		}
    }
}
