using System;

namespace Library.Tools {
	public static class Encryption {
		private static string HashName = "SHA1";
		private static int KeySize = 256;
		private static string Iv = "@1C2c3E4e5d3g7h5";
		private static int Iterations = 3;
		private static string Password = "s0ftsm@rt";
		private static string Salt = "s^$tem$";
	
		public static string Encrypt(string stringToEncript){
			byte[] ivBytes = System.Text.Encoding.UTF8.GetBytes(Library.Tools.Encryption.Iv);
			byte[] saltBytes = System.Text.Encoding.UTF8.GetBytes(Library.Tools.Encryption.Salt);
			byte[] stringToEncriptBytes  = System.Text.Encoding.UTF8.GetBytes(stringToEncript);
	        
			System.Security.Cryptography.PasswordDeriveBytes password = new System.Security.Cryptography.PasswordDeriveBytes(Library.Tools.Encryption.Password, saltBytes, Library.Tools.Encryption.HashName, Library.Tools.Encryption.Iterations);
			byte[] keyBytes = password.GetBytes(Library.Tools.Encryption.KeySize / 8);
	        
			System.Security.Cryptography.RijndaelManaged symmetricKey = new System.Security.Cryptography.RijndaelManaged();
			symmetricKey.Mode = System.Security.Cryptography.CipherMode.CBC;        
			System.Security.Cryptography.ICryptoTransform encryptor = symmetricKey.CreateEncryptor(keyBytes, ivBytes);
	        
			System.IO.MemoryStream memoryStream = new System.IO.MemoryStream();        
			System.Security.Cryptography.CryptoStream cryptoStream = new System.Security.Cryptography.CryptoStream(memoryStream, encryptor, System.Security.Cryptography.CryptoStreamMode.Write);
			cryptoStream.Write(stringToEncriptBytes, 0, stringToEncriptBytes.Length);
			cryptoStream.FlushFinalBlock();

			byte[] stringToDecriptBytes = memoryStream.ToArray();
			memoryStream.Close();
			cryptoStream.Close();
	        
			return Convert.ToBase64String(stringToDecriptBytes);
		}
	    
		public static string Decrypt(string stringToDecript){
			byte[] ivBytes = System.Text.Encoding.UTF8.GetBytes(Library.Tools.Encryption.Iv);
			byte[] saltBytes = System.Text.Encoding.UTF8.GetBytes(Library.Tools.Encryption.Salt);
			byte[] stringToDecriptBytes = System.Convert.FromBase64String(stringToDecript);
	        
			System.Security.Cryptography.PasswordDeriveBytes password = new System.Security.Cryptography.PasswordDeriveBytes(Library.Tools.Encryption.Password, saltBytes, Library.Tools.Encryption.HashName, Library.Tools.Encryption.Iterations);
			byte[] keyBytes = password.GetBytes(Library.Tools.Encryption.KeySize / 8);

			System.Security.Cryptography.RijndaelManaged symmetricKey = new System.Security.Cryptography.RijndaelManaged();
			symmetricKey.Mode = System.Security.Cryptography.CipherMode.CBC;
			System.Security.Cryptography.ICryptoTransform decryptor = symmetricKey.CreateDecryptor(keyBytes, ivBytes);
	        
			System.IO.MemoryStream memoryStream = new System.IO.MemoryStream(stringToDecriptBytes);
			System.Security.Cryptography.CryptoStream cryptoStream = new System.Security.Cryptography.CryptoStream(memoryStream, decryptor, System.Security.Cryptography.CryptoStreamMode.Read);

			byte[] stringToEncriptBytes = new byte[stringToDecriptBytes.Length];
			int decryptedByteCount = cryptoStream.Read(stringToEncriptBytes, 0, stringToEncriptBytes.Length);
			memoryStream.Close();
			cryptoStream.Close();
	        
			return System.Text.Encoding.UTF8.GetString(stringToEncriptBytes, 0, decryptedByteCount);
		}
	}
}