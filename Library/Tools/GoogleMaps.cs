﻿using System;
using System.Net;
using System.Xml;
using System.Xml.Linq;

namespace Library.Tools {
    public class GoogleMaps {
        public Boolean validateaddress(string address){
            string requestUri = string.Format("http://maps.googleapis.com/maps/api/geocode/xml?address={0}&sensor=false", Uri.EscapeDataString(address));
            var request = WebRequest.Create(requestUri);
            var response = request.GetResponse();
            var xdoc = new XmlDocument();
            xdoc.Load(response.GetResponseStream());
            var result = xdoc.GetElementsByTagName("GeocodeResponse");
            var final = result[0].ChildNodes[0].InnerText;
            return (final == "OK");
        }

        private static string addressAPI = "http://maps.googleapis.com/maps/api/geocode/xml?address={0}&sensor=false";

		public XElement GetGoogleMapsAPIResponse(string requestUri){
			XElement resGeocode = null;

			var request = WebRequest.Create(requestUri);
			var response = request.GetResponse();
			var xdoc = XDocument.Load(response.GetResponseStream());
			resGeocode = xdoc.Element("GeocodeResponse").Element("result");

			return resGeocode;
		}

		public bool VerifyAddress(string address){
			bool isValid = false;
			string requestUri = string.Format(addressAPI, Uri.EscapeDataString(address));
			XElement result = GetGoogleMapsAPIResponse(requestUri);
			string location_type = result.Element("geometry").Element("location_type").Value;

            if (!string.IsNullOrEmpty(location_type)){
				if ((location_type.ToUpper() == "ROOFTOP") || (location_type.ToUpper() == "RANGE_INTERPOLATED")){
					isValid = true;
				}
			}

			return isValid;
		}

		public string[] GetLongLat(string address){
			string[] longLat = null;
			string requestUri = string.Format(addressAPI, Uri.EscapeDataString(address));
			XElement result = GetGoogleMapsAPIResponse(requestUri);
            var locationElement = result.Element("geometry").Element("location");

            if (locationElement != null){
				var lat = locationElement.Element("lat");
				var lng = locationElement.Element("lng");

				longLat = new string[2];
				longLat[0] = lat.Value;
				longLat[1] = lng.Value;
			}

			return longLat;
		}
    }
}
