using System;

namespace Library.Tools {
	public class Validation {
		public static bool IsNumeric(string numericvalue){try {float.Parse(numericvalue);} catch {return false;} return true;}

		public static bool IsNull(object boolvalue){if (boolvalue == null){return true;} return false;}

		public static bool IsDate(string datevalue){try {DateTime.Parse(datevalue);} catch {return false;} return true;}

		public static string PhoneFormat(string phonevalue){
			phonevalue = StripNumber(phonevalue);
			if (IsNumeric(phonevalue) == false){return "";}
			long stripedNumber = long.Parse(phonevalue);
			string temp = "";
			if (stripedNumber.ToString().Length > 10){temp = stripedNumber.ToString("###-###-#### Ext. ####");} else {temp = stripedNumber.ToString("###-###-####");} return temp;}

		public static string StripNumber(string number){
			string temp = "";
			for (int row = 0; number.Length > row; row++){if (Validation.IsNumeric(number.Substring(row, 1).ToString()) == true){temp = temp + number.Substring(row, 1).ToString();}}
			return temp;
		}
	}
}
