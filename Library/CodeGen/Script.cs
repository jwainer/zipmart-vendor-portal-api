using System;
using System.IO;
using Library.Tools;
using Microsoft.SqlServer;
using System.Data.SqlClient;

namespace Library.Codegen {   
    public class ScriptColumn {
        public ScriptColumn(string name, string type, int length, bool inprimarykey, bool isnullable, bool isidentity){
            this.Name = name;
            this.dataType = type;
            this.Length = length;
            this.InPrimaryKey = inprimarykey;
            this.IsNullable = isnullable;
            this.IsIdentity = isidentity;
        }

        public string Name = "";
        public string dataType = "";
        public int Length = 0;
        public bool InPrimaryKey = false;
        public bool InUniqueKey = false;
        public bool IsNullable = true;
        public bool IsIdentity = false;
    }
    
}
