﻿using Library.Tools;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;

namespace Library.Codegen {
    public class Generate {
        public string DatabaseName = "";
        public int MaxLineLength = 108;
        public string User = "";
        public string Password = "";
        public string ObjectName = "";
        public string ClassName = "";
        public string ViewShortName = "";
        public string Schema = "";
        public string Extension = "";
        public string Params = "";
        public string Fields = "";

        public string Connection { get; set;}
        private ScriptColumn[] columns = new ScriptColumn[0];

        public bool IsTable { get; set;}

        public Generate(string connection, string objectName){
            SqlConnectionStringBuilder builder = new SqlConnectionStringBuilder(connection);
            this.User = builder.UserID; this.Password = builder.Password;
            this.DatabaseName = builder.InitialCatalog;

            this.Connection = connection;
            this.Schema = "dbo";
            this.IsTable = true;

            if (objectName.Contains(".")){
                var split = objectName.Split('.');
                this.Schema = split[0];
                this.ObjectName = split[1];
            } else {
                this.ObjectName = objectName;
            }

            this.ClassName = (this.ObjectName.IndexOf("_") >= 0 ? this.ConvertUpperToUpperLower(this.ObjectName) : this.ObjectName.Substring(0, 1).ToUpper() + this.ObjectName.Substring(1).ToLower());

            string query = "SELECT name FROM sys.Tables where name='" + this.ObjectName + "'";
            Library.Data.DataSet dsExist = new Data.DataSet(this.Connection);
            dsExist.ExecuteStatement(query);

            if (dsExist.EOF == false){
                columns = this.GetColumnsFromTable();

            } else {
                this.IsTable = false;
                query = "SELECT name FROM sys.Views where name='" + this.ObjectName + "'";
                dsExist = new Data.DataSet(this.Connection);
                dsExist.ExecuteStatement(query);
                if (!dsExist.EOF){
                    columns = this.GetColumnsFromView();
                }
            }

        }

        internal ScriptColumn[] GetColumnsFromTable(){
            string query =
                 @"SELECT c.name 'ColumnName', t.Name 'DataType',c.max_length 'MaxLength',ISNULL(i.is_primary_key, 0) 'IsPrimaryKey',c.is_nullable,c.is_identity "
                + @"FROM sys.columns c "
                + @"INNER JOIN sys.types t ON c.user_type_id = t.user_type_id "
                + @"LEFT OUTER JOIN sys.index_columns ic ON ic.object_id = c.object_id AND ic.column_id = c.column_id "
                + @"LEFT OUTER JOIN sys.indexes i ON ic.object_id = i.object_id AND ic.index_id = i.index_id "
                + @"WHERE c.object_id = OBJECT_ID('" + this.Schema + "." + this.ObjectName + "')";

            Library.Data.DataSet dsColumns = new Data.DataSet(this.Connection);
            dsColumns.ExecuteStatement(query);
            List<ScriptColumn> cols = new List<ScriptColumn>();
            ScriptColumn column = null;

            while (dsColumns.EOF == false){
                column = new ScriptColumn(dsColumns.FieldValue(0), dsColumns.FieldValue(1), int.Parse(dsColumns.FieldValue(2)), bool.Parse(dsColumns.FieldValue(3)), 
                    bool.Parse(dsColumns.FieldValue(4)), bool.Parse(dsColumns.FieldValue(5)));
                cols.Add(column);

                dsColumns.MoveNext();
            }

            return cols.ToArray();
        }

        internal ScriptColumn[] GetColumnsFromView(){
            string query =
            @"SELECT c.COLUMN_NAME, DATA_TYPE,CHARACTER_MAXIMUM_LENGTH 'MaxLength',CONVERT(Bit, 0) as 'IsPrimaryKey',IS_NULLABLE,CONVERT(Bit, 0) as is_identity 
                FROM    INFORMATION_SCHEMA.VIEW_COLUMN_USAGE AS cu
                JOIN    INFORMATION_SCHEMA.COLUMNS AS c
                ON      c.TABLE_SCHEMA  = cu.TABLE_SCHEMA
                AND     c.TABLE_CATALOG = cu.TABLE_CATALOG
                AND     c.TABLE_NAME    = cu.TABLE_NAME
                AND     c.COLUMN_NAME   = cu.COLUMN_NAME
                WHERE   cu.VIEW_NAME    = '" + this.ObjectName + "' ";

            query += @"AND cu.VIEW_SCHEMA = '" + this.Schema + "'";

            Library.Data.DataSet dsColumns = new Data.DataSet(this.Connection);
            dsColumns.ExecuteStatement(query);
            List<ScriptColumn> cols = new List<ScriptColumn>();
            ScriptColumn column = null;
            while (dsColumns.EOF == false){
                column = new ScriptColumn(dsColumns.FieldValue(0), dsColumns.FieldValue(1), int.Parse((dsColumns.FieldValue(2) == String.Empty ? "0" : dsColumns.FieldValue(2))), bool.Parse(dsColumns.FieldValue(3)), (dsColumns.FieldValue(4).Equals("Yes")), bool.Parse(dsColumns.FieldValue(5)));
                cols.Add(column);
                dsColumns.MoveNext();
            }


            return cols.ToArray();
        }

        public string GenerateDeclaration(){
            string declarations = "";

            foreach (ScriptColumn column in this.columns){
                if (string.Compare(column.Name, "ModifiedUser", true) != 0){

                    string field = this.ConvertUpperToUpperLower(column.Name);
                    string variable = field.Substring(0, 1).ToLower() + field.Substring(1);
                    string declare = "";

                    switch (column.dataType){
                    case "int":         declare = "\t\tpublic int " + field + " = 0;"; break;
                    case "bigint":      declare = "\t\tpublic long " + field + " = 0;"; break;
                    case "money":       declare = "\t\tpublic float " + field + " = 0;"; break;
                    case "float":       declare = "\t\tpublic float " + field + " = 0;"; break;
                    case "datetime":    declare = "\t\tpublic DateTime " + field + " = DateTime.Now;"; break;
                    case "date":        declare = "\t\tpublic DateTime " + field + " = DateTime.Now;"; break;
                    case "bit":         declare = "\t\tpublic bool " + field + "  = false;"; break;
                    default:
                        if (variable.Substring(variable.Length - 2) == "YN"){
                            declare = "\t\tpublic string " + variable + @" = ""N"";" + "\n\t\tpublic string " + field + " {get {return this." + variable + ";} set {this." +
                                variable + @" = (value == ""Y"" ? ""Y"" : ""N"");}}";
                        } else {declare = "\t\tpublic string " + field + " = \"\";";}

                        break;
                    }

                    declarations = declarations + declare + "\n";
                }
            }

            return declarations;
        }

        public string GenerateConstructor(){
            string initializes = ""; string initializesPrimary = ""; string primaryVariable = ""; string primaryField = "";

            foreach (ScriptColumn column in this.columns){
                string field = this.ConvertUpperToUpperLower(column.Name);
                string variable = field.Substring(0, 1).ToLower() + field.Substring(1);
                string initialize = "";

                switch (column.dataType.ToLower()){
                case "int":         initialize = "\t\t\tthis." + field + @" = int.Parse(data[""" + field + @"""]);"; break;
                case "bigint":      initialize = "\t\t\tthis." + field + @" = long.Parse(data[""" + field + @"""]);"; break;
                case "money":       initialize = "\t\t\tthis." + field + @" = float.Parse(data[""" + field + @"""]);"; break;
                case "float":       initialize = "\t\t\tthis." + field + @" = float.Parse(data[""" + field + @"""]);"; break;
                case "bit":         initialize = "\t\t\tthis." + field + @" = bool.Parse(data[""" + field + @"""]);"; break;
                case "date":        initialize = "\t\t\tthis." + field + @" = DateTime.Parse(data[""" + field + @"""]);"; break;
                case "datetime":    initialize = "\t\t\tthis." + field + @" = DateTime.Parse(data[""" + field + @"""]);"; break;
                default:            initialize = "\t\t\tthis." + field + @" = data[""" + field + @"""];"; break;
                }

                initializes = initializes + initialize + "\n";
                if (column.InPrimaryKey == true){primaryVariable = variable; initializesPrimary = "int " + primaryVariable; primaryField = field;}
            }

            string temp = "\n\t\tpublic " + this.ClassName + "() : base(){}\n\n" + "\t\tpublic " + this.ClassName + "(string connection, string modifiedUserID, " +
                "int id) : base(connection, modifiedUserID, id, \"" + this.Schema + "." + this.ObjectName + "\"){return;}\n\n" +
                "\t\tpublic " + this.ClassName + "(string connection) : base(connection, \"" + this.Schema + "." + this.ObjectName + "\"){return;}";

            return temp;
        }

        public string GenerateListProcedures(){
            string temp = "";
            string query = @"select procs.name from sys.procedures procs inner join sys.schemas on 
            procs.schema_id = schemas.schema_id where schemas.name = '{0}' 
            and procs.name LIKE '{1}_List{2}'";
            
            Library.Data.DataSet ds = new Data.DataSet(this.Connection);
            ds.ExecuteStatement(string.Format(query, this.Schema, this.ObjectName, this.Extension));

            if (!ds.EOF){
                string sp = string.Empty;
                while (ds.EOF == false){
                    sp = ds.FieldValue(0);
                    string body = "";
                    if (!String.IsNullOrEmpty(sp)){
                        string definition = "\n\t\t[HttpGet][Route(\"" + this.ClassName + "/List" + this.Extension + "\")]\n\t\tpublic List&lt;Dictionary&lt;string, object&gt;&gt; " + ("List" + this.Extension) + "(";
                        query = @"select distinct(PARAMETER_NAME),DATA_TYPE,ORDINAL_POSITION from information_schema.parameters
                        where specific_name='{0}' order by ordinal_position asc";
                        Library.Data.DataSet dsSp = new Data.DataSet(this.Connection);
                        dsSp.ExecuteStatement(string.Format(query, sp));
                        if (!dsSp.EOF){
                            string parameterName = "", dataType = "", bodyParameters = "", paramAux = "";
                            #region parameters
                            while (dsSp.EOF == false){
                                parameterName = dsSp.FieldValue(0);
                                dataType = dsSp.FieldValue(1);
                                paramAux = parameterName.Replace("@", ""); //this.ConvertToUpperLower(parameterName.Replace("@", ""));
                                switch (dataType){
                                case "int": {
                                        definition += "int " + paramAux + ", ";
                                        bodyParameters += string.Format("\t\t\tdata.AddParam(\"{0}\", Library.Data.ParamType.Numeric, {1}.ToString());\n", parameterName, paramAux);
                                    }
                                    break;
                                case "bigint": {
                                        definition += "long " + paramAux + ", ";
                                        bodyParameters += string.Format("\t\t\tdata.AddParam(\"{0}\", Library.Data.ParamType.Numeric, {1}.ToString());\n", parameterName, paramAux);
                                    }
                                    break;
                                case "money": {
                                        definition += "float " + paramAux + ", ";
                                        bodyParameters += string.Format("\t\t\tdata.AddParam(\"{0}\", Library.Data.ParamType.Numeric, {1}.ToString());\n", parameterName, paramAux);
                                    }
                                    break;
                                case "float": {
                                        definition += "float " + paramAux + ", ";
                                        bodyParameters += string.Format("\t\t\tdata.AddParam(\"{0}\", Library.Data.ParamType.Numeric, {1}.ToString());\n", parameterName, paramAux);
                                    }
                                    break;
                                case "datetime": {
                                        definition += "DateTime " + paramAux + ", ";
                                        bodyParameters += string.Format("\t\t\tdata.AddParam(\"{0}\", Library.Data.ParamType.DateTime, {1}.ToString());\n", parameterName, paramAux);
                                    }
                                    break;
                                case "date": {
                                        definition += "DateTime " + paramAux + ", ";
                                        bodyParameters += string.Format("\t\t\tdata.AddParam(\"{0}\", Library.Data.ParamType.DateTime, {1}.ToString());\n", parameterName, paramAux);
                                    }
                                    break;
                                case "bit": {
                                        definition += "bool " + paramAux + ", ";
                                        bodyParameters += string.Format("\t\t\tdata.AddParam(\"{0}\", Library.Data.ParamType.Bool, {1}.ToString());\n", parameterName, paramAux);
                                    }
                                    break;
                                default: {
                                        definition += "string " + paramAux + ", ";
                                        bodyParameters += string.Format("\t\t\tdata.AddParam(\"{0}\", Library.Data.ParamType.Text, {1});\n", parameterName, paramAux);
                                    }
                                    break;
                                }
                                dsSp.MoveNext();
                            }
                            #endregion
                            body = definition.Substring(0, definition.LastIndexOf(',')) + "){\n";
                            body += "\t\t\tLibrary.Data.DataSet data = new Library.Data.DataSet(this.connection);\n\n";
                            body += bodyParameters;
                            body += string.Format("\t\t\tdata.ExecuteProcedure(\"{0}.{1}\")", this.Schema, sp) + ";\n\n";
                            body += "\t\t\treturn data.ToJson();\n";
                            body += "\t\t}\n";
                        }
                    }
                    temp += body;
                    ds.MoveNext();
                }
            }
            return temp;
        }

        public string GenerateMethods(){
            string strRetrieve = "\t\t[HttpGet][Route(\"" + this.ClassName + "/{id}\")]\n\t\tpublic Dictionary&lt;string, object&gt; Retrieve(int id){\n" +
                                 "\t\t\treturn new " + this.ClassName + "(this.connection, \"" + this.ClassName + "Controller" + "\", id).ToJson();\n\t\t}";

            string strSave = "\n\n\t\t[HttpPost][Route(\"" + this.ClassName + "/Save\")]\n\t\tpublic Dictionary&lt;string, object&gt; Save(Dictionary&lt;string, string&gt; my" + this.ObjectName + "){\n" +
                             "\t\t\t" + this.ObjectName + " " + this.ObjectName.ToLower() + " = new " + this.ObjectName + "(this.connection, \"" + this.ClassName + "Controller" + "\", 0);\n\t\t\t" + this.ObjectName.ToLower() + ".Load(my" + this.ObjectName + ");\n\t\t\t" + this.ObjectName.ToLower() + ".Save();\n\n\t\t\treturn " + this.ObjectName.ToLower() + ".ToJson();\n\t\t}";

            string strDelete = "\n\n\t\t[HttpGet][Route(\"" + this.ClassName + "/Delete/{id}\")]\n\t\tpublic Dictionary&lt;string, object&gt; Delete(long id){\n" +
                               "\t\t\t" + this.ObjectName + " " + this.ObjectName.ToLower() + " = new " + this.ObjectName + "(this.connection, \"" + this.ClassName + "Controller" + "\", id);\n\t\t\t" + this.ObjectName.ToLower() + ".Delete();" +
                               "\n\n\t\t\tDictionary&lt;string, string&gt; dict = new Dictionary&lt;string, string&gt;();\n\t\t\tdict.Add(\"status\", \"success\");\n\t\t\treturn dict;\n\t\t}";

            string strList = "\n\n\t\t[HttpGet][Route(\"" + this.ClassName + "/List\")]\n\t\tpublic List&lt;Dictionary&lt;string, object&gt;&gt; List(int Page, int PageSize, string SearchString, string OrderBy, string OrderByDir){\n" +
                             "\t\t\tLibrary.Data.DataSet data = new Library.Data.DataSet(this.connection);\n\n\t\t\t" +
                             "data.AddParam(\"@OrderBy\", Library.Data.ParamType.Text, OrderBy);\n\t\t\t" +
                             "data.AddParam(\"@OrderByDir\", Library.Data.ParamType.Text, OrderByDir);\n\t\t\t" +
                             "data.AddParam(\"@Page\", Library.Data.ParamType.Numeric, Page.ToString());\n\t\t\t" +
                             "data.AddParam(\"@PageSize\", Library.Data.ParamType.Numeric, PageSize.ToString());\n\t\t\t" +
                             "data.AddParam(\"@SearchString\", Library.Data.ParamType.Text, SearchString);\n\t\t\t" +
                             "data.ExecuteProcedure(\"" + this.Schema + "." + this.ObjectName.ToUpper() + "_List\");\n\n\t\t\t" +
                             "return data.ToJson();\n\t\t}";

            string temp = strRetrieve + strSave + strDelete + strList;
            return temp;
        }

        public string GenerateClass(){
            string database = (this.DatabaseName.IndexOf("_") >= 0 ? this.ConvertUpperToUpperLower(this.DatabaseName) : this.DatabaseName.Substring(0, 1).ToUpper() + this.DatabaseName.Substring(1).ToLower());
            string temp = this.GenerateDeclaration() + this.GenerateConstructor();

            temp = "using System;\nusing System.Collections.Generic;\nusing System.Linq;\nusing System.Web;\nusing Library;\n\nnamespace Library.Data" + ("." + database) + ("." + this.ConvertUpperToUpperLower(this.Schema)) +
                " {\n\tpublic class " + this.ClassName + " : Library.Data.Transaction {\n" + temp + "\n\t}\n}";

            return temp;
        }

        public string GenerateController(){
            string temp = "";

            temp = "using System;\nusing System.Collections.Generic;\nusing System.Linq;\nusing System.Net;\nusing System.Net.Http;\nusing System.Web.Http;\nusing Library.Data.Core.Secure;\n\nnamespace VendorApi.Controllers" +
                   " {\n\tpublic class " + this.ClassName + "Controller : ApiController {\n\t\tprivate string connection = System.Configuration.ConfigurationManager.ConnectionStrings[\"core\"].ConnectionString;\n\n" +
                        GenerateMethods() +
                   " \n}";

            return temp;
        }

        public void GenerateProcedures(){
            Library.Data.DataSet data = new Data.DataSet(this.Connection);

            data.AddParam("@TableName", Library.Data.ParamType.Text, this.ObjectName);
            data.AddParam("@SchemaName", Library.Data.ParamType.Text, this.Schema);
            data.ExecuteProcedure("gen.SpDelete");

            data = new Data.DataSet(this.Connection);
            data.AddParam("@TableName", Library.Data.ParamType.Text, this.ObjectName);
            data.AddParam("@SchemaName", Library.Data.ParamType.Text, this.Schema);
            data.ExecuteProcedure("gen.SpRetrieve");

            data = new Data.DataSet(this.Connection);
            data.AddParam("@TableName", Library.Data.ParamType.Text, this.ObjectName);
            data.AddParam("@SchemaName", Library.Data.ParamType.Text, this.Schema);
            data.ExecuteProcedure("gen.SpSave");
        }

        public string GenerateListProcedure() {
            Library.Data.DataSet data = new Data.DataSet(this.Connection);

            data.AddParam("@TableName", Library.Data.ParamType.Text, this.ObjectName);
            data.AddParam("@SchemaName", Library.Data.ParamType.Text, this.Schema);
            data.AddParam("@Extension", Library.Data.ParamType.Text, this.Extension, true);
            data.AddParam("@Params", Library.Data.ParamType.Text, this.Params, true);
            data.AddParam("@Fields", Library.Data.ParamType.Text, this.Fields, true);
            data.ExecuteProcedure("gen.SpList");

            string strList = GenerateListProcedures();
            //string[] parameters = this.Params.Split(',');
            //if(parameters.Length > 0) {

            //}

            //string strList = "\n\n\t\t[HttpGet][Route(\"" + this.ClassName + "/List\")]\n\t\tpublic List&lt;Dictionary&lt;string, object&gt;&gt; List(int Page, int PageSize, string SearchString, string OrderBy, string OrderByDir){\n" +
            //                 "\t\t\tLibrary.Data.DataSet data = new Library.Data.DataSet(this.connection);\n\n\t\t\t" +
            //                 "data.AddParam(\"@OrderBy\", Library.Data.ParamType.Text, OrderBy);\n\t\t\t" +
            //                 "data.AddParam(\"@OrderByDir\", Library.Data.ParamType.Text, OrderByDir);\n\t\t\t" +
            //                 "data.AddParam(\"@Page\", Library.Data.ParamType.Numeric, Page.ToString());\n\t\t\t" +
            //                 "data.AddParam(\"@PageSize\", Library.Data.ParamType.Numeric, PageSize.ToString());\n\t\t\t" +
            //                 "data.AddParam(\"@SearchString\", Library.Data.ParamType.Text, SearchString);\n\t\t\t" +
                             
            //                 "data.ExecuteProcedure(\"" + this.Schema + "." + this.ObjectName.ToUpper() + "_List" + this.Extension + "\");\n\n\t\t\t" +
            //                 "return data.ToJson();\n\t\t}";

            return strList;
        }

        public string ConvertUpperToUpperLower(string textToConvert){
            string endWord = "";

            if (textToConvert.ToLower().IndexOf("_id_") > 0){
                int end = textToConvert.ToLower().IndexOf("_id_");
                endWord = textToConvert.Substring(end + 3);
            }

            string newTextToConvert = (endWord.Length > 0 ? textToConvert.Replace(endWord, "") : textToConvert);
            string[] words = newTextToConvert.Split("_".ToCharArray()[0]);
            string convertedText = "";

            if (words.Length == 1){convertedText = convertedText + words[0].Substring(0, 1).ToUpper() + words[0].Substring(1);} else {
                for (int index = 0; words.Length > index; index++){
                    convertedText = convertedText + words[index].Substring(0, 1).ToUpper() + words[index].Substring(1).ToLower();
                }
            }

            words = convertedText.Split("#".ToCharArray()[0]);
            convertedText = "";

            if (words.Length == 1){convertedText = convertedText + words[0].Substring(0, 1).ToUpper() + words[0].Substring(1);} else {
                for (int index = 0; words.Length > index; index++){
                    convertedText = convertedText + (convertedText.Length > 0 ? "_" : "") + words[index].Substring(0, 1).ToUpper() + words[index].Substring(1);
                }
            }

            convertedText = convertedText + (endWord.Length > 0 ? endWord : "");
            return convertedText;
        }

        public string ConvertToUpperLower(string value){
            string[] words = value.Split(" _#".ToCharArray());
            string result = "";

            for (int index = 0; words.Length > index; index++){
                words[index] = words[index].Substring(0, 1).ToUpper() + words[index].Substring(1).ToLower();
                result = result + words[index];
            }

            return result;
        }
    }
}
