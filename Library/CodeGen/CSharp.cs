using Library.Tools;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;

namespace Library.Codegen {
    public class CSharp {

        public string DatabaseName = "";
        public int MaxLineLength = 108;
        public string User = "";
        public string Password = "";
        public string ObjectName = "";
        public string ClassName = "";
        public string ViewShortName = "";
        public string Schema = "";

        public string Connection { get; set;}
        private ScriptColumn[] columns = new ScriptColumn[0];

        public bool IsTable { get; set;}

        public CSharp(string connection, string objectName){
            SqlConnectionStringBuilder builder = new SqlConnectionStringBuilder(connection);
            this.User = builder.UserID; this.Password = builder.Password;
            this.DatabaseName = builder.InitialCatalog;

            this.Connection = connection;
            this.Schema = "dbo";
            this.IsTable = true;

            if (objectName.Contains(".")){
                var split = objectName.Split('.');
                this.Schema = split[0];
                this.ObjectName = split[1];
            } else {
                this.ObjectName = objectName;
            }

            this.ClassName = (this.ObjectName.IndexOf("_") >= 0 ? this.ConvertUpperToUpperLower(this.ObjectName) : this.ObjectName.Substring(0, 1).ToUpper() + this.ObjectName.Substring(1).ToLower());

            string query = "SELECT name FROM sys.Tables where name='" + this.ObjectName + "'";
            Library.Data.DataSet dsExist = new Data.DataSet(this.Connection);
            dsExist.ExecuteStatement(query);

            if (dsExist.EOF == false){
                columns = this.GetColumnsFromTable();

            } else {
                this.IsTable = false;
                query = "SELECT name FROM sys.Views where name='" + this.ObjectName + "'";
                dsExist = new Data.DataSet(this.Connection);
                dsExist.ExecuteStatement(query);
                if (!dsExist.EOF){
                    columns = this.GetColumnsFromView();
                }
            }

        }

        internal ScriptColumn[] GetColumnsFromTable(){
            string query =
                 @"SELECT c.name 'ColumnName', t.Name 'DataType',c.max_length 'MaxLength',ISNULL(i.is_primary_key, 0) 'IsPrimaryKey',c.is_nullable,c.is_identity "
                + @"FROM sys.columns c "
                + @"INNER JOIN sys.types t ON c.user_type_id = t.user_type_id "
                + @"LEFT OUTER JOIN sys.index_columns ic ON ic.object_id = c.object_id AND ic.column_id = c.column_id "
                + @"LEFT OUTER JOIN sys.indexes i ON ic.object_id = i.object_id AND ic.index_id = i.index_id "
                + @"WHERE c.object_id = OBJECT_ID('" + this.Schema + "." + this.ObjectName + "')";

            Library.Data.DataSet dsColumns = new Data.DataSet(this.Connection);
            dsColumns.ExecuteStatement(query);
            List<ScriptColumn> cols = new List<ScriptColumn>();
            ScriptColumn column = null;

            while (dsColumns.EOF == false){
                column = new ScriptColumn(dsColumns.FieldValue(0), dsColumns.FieldValue(1), int.Parse(dsColumns.FieldValue(2)), bool.Parse(dsColumns.FieldValue(3)), 
                    bool.Parse(dsColumns.FieldValue(4)), bool.Parse(dsColumns.FieldValue(5)));
                cols.Add(column);

                dsColumns.MoveNext();
            }

            return cols.ToArray();
        }

        internal ScriptColumn[] GetColumnsFromView(){
            string query =
            @"SELECT c.COLUMN_NAME, DATA_TYPE,CHARACTER_MAXIMUM_LENGTH 'MaxLength',CONVERT(Bit, 0) as 'IsPrimaryKey',IS_NULLABLE,CONVERT(Bit, 0) as is_identity 
                FROM    INFORMATION_SCHEMA.VIEW_COLUMN_USAGE AS cu
                JOIN    INFORMATION_SCHEMA.COLUMNS AS c
                ON      c.TABLE_SCHEMA  = cu.TABLE_SCHEMA
                AND     c.TABLE_CATALOG = cu.TABLE_CATALOG
                AND     c.TABLE_NAME    = cu.TABLE_NAME
                AND     c.COLUMN_NAME   = cu.COLUMN_NAME
                WHERE   cu.VIEW_NAME    = '" + this.ObjectName + "' ";

            query += @"AND cu.VIEW_SCHEMA = '" + this.Schema + "'";

            Library.Data.DataSet dsColumns = new Data.DataSet(this.Connection);
            dsColumns.ExecuteStatement(query);
            List<ScriptColumn> cols = new List<ScriptColumn>();
            ScriptColumn column = null;
            while (dsColumns.EOF == false){
                column = new ScriptColumn(dsColumns.FieldValue(0), dsColumns.FieldValue(1), int.Parse((dsColumns.FieldValue(2) == String.Empty ? "0" : dsColumns.FieldValue(2))), bool.Parse(dsColumns.FieldValue(3)), (dsColumns.FieldValue(4).Equals("Yes")), bool.Parse(dsColumns.FieldValue(5)));
                cols.Add(column);
                dsColumns.MoveNext();
            }


            return cols.ToArray();
        }

        public string GenerateDeclaration(){
            string declarations = "";

            foreach (ScriptColumn column in this.columns){
                if (string.Compare(column.Name, "ModifiedUser", true) != 0){

                    string field = this.ConvertUpperToUpperLower(column.Name);
                    string variable = field.Substring(0, 1).ToLower() + field.Substring(1);
                    string declare = "";

                    switch (column.dataType){
                    case "int":         declare = "\t\tpublic int " + field + " = 0;"; break;
                    case "bigint":      declare = "\t\tpublic long " + field + " = 0;"; break;
                    case "money":       declare = "\t\tpublic float " + field + " = 0;"; break;
                    case "float":       declare = "\t\tpublic float " + field + " = 0;"; break;
                    case "datetime":    declare = "\t\tpublic DateTime " + field + " = DateTime.Now;"; break;
                    case "date":        declare = "\t\tpublic DateTime " + field + " = DateTime.Now;"; break;
                    case "bit":         declare = "\t\tpublic bool " + field + "  = false;"; break;
                    default:
                        if (variable.Substring(variable.Length - 2) == "YN"){
                            declare = "\t\tpublic string " + variable + @" = ""N"";" + "\n\t\tpublic string " + field + " {get {return this." + variable + ";} set {this." +
                                variable + @" = (value == ""Y"" ? ""Y"" : ""N"");}}";
                        } else {declare = "\t\tpublic string " + field + " = \"\";";}

                        break;
                    }

                    declarations = declarations + declare + "\n";
                }
            }

            return declarations;
        }

        public string GenerateConstructor(){
            string initializes = ""; string initializesPrimary = ""; string primaryVariable = ""; string primaryField = "";

            foreach (ScriptColumn column in this.columns){
                string field = this.ConvertUpperToUpperLower(column.Name);
                string variable = field.Substring(0, 1).ToLower() + field.Substring(1);
                string initialize = "";

                switch (column.dataType.ToLower()){
                case "int":         initialize = "\t\t\tthis." + field + @" = int.Parse(data[""" + field + @"""]);"; break;
                case "bigint":      initialize = "\t\t\tthis." + field + @" = long.Parse(data[""" + field + @"""]);"; break;
                case "money":       initialize = "\t\t\tthis." + field + @" = float.Parse(data[""" + field + @"""]);"; break;
                case "float":       initialize = "\t\t\tthis." + field + @" = float.Parse(data[""" + field + @"""]);"; break;
                case "bit":         initialize = "\t\t\tthis." + field + @" = bool.Parse(data[""" + field + @"""]);"; break;
                case "date":        initialize = "\t\t\tthis." + field + @" = DateTime.Parse(data[""" + field + @"""]);"; break;
                case "datetime":    initialize = "\t\t\tthis." + field + @" = DateTime.Parse(data[""" + field + @"""]);"; break;
                default:            initialize = "\t\t\tthis." + field + @" = data[""" + field + @"""];"; break;
                }

                initializes = initializes + initialize + "\n";
                if (column.InPrimaryKey == true){primaryVariable = variable; initializesPrimary = "int " + primaryVariable; primaryField = field;}
            }

            string temp = "\n\t\tpublic " + this.ClassName + "() : base(){}\n\n" + "\t\tpublic " + this.ClassName + "(string connection, string modifiedUserID, " +
                "int id) : base(connection, modifiedUserID,id, \"" + this.Schema + "." + this.ObjectName + "\"){return;}\n\n" +
                "\t\tpublic " + this.ClassName + "(string connection) : base(connection, \"" + this.Schema + "." + this.ObjectName + "\"){return;}\n" + this.GenerateListProcedures();

            return temp;
        }

        public string GenerateListProcedures(){
            string temp = "";
            string query = @"select procs.name from sys.procedures procs inner join sys.schemas on 
            procs.schema_id = schemas.schema_id where schemas.name = '{0}' 
            and procs.name LIKE '{1}_List%'";
            
            Library.Data.DataSet ds = new Data.DataSet(this.Connection);
            ds.ExecuteStatement(string.Format(query, this.Schema, this.ObjectName));

            if (!ds.EOF){
                string sp = string.Empty;
                while (ds.EOF == false){
                    sp = ds.FieldValue(0);
                    string body = "";
                    if (!String.IsNullOrEmpty(sp)){
                        string definition = "\n\t\tpublic string " + sp.Replace(this.ObjectName + "_", "") + "(";
                        query = @"select distinct(PARAMETER_NAME),DATA_TYPE from information_schema.parameters
                        where specific_name='{0}'";
                        Library.Data.DataSet dsSp = new Data.DataSet(this.Connection);
                        dsSp.ExecuteStatement(string.Format(query, sp));
                        if (!dsSp.EOF){
                            string parameterName = "", dataType = "", bodyParameters = "", paramAux = "";
                            #region parameters
                            while (dsSp.EOF == false){
                                parameterName = dsSp.FieldValue(0);
                                dataType = dsSp.FieldValue(1);
                                paramAux = this.ConvertToUpperLower(parameterName.Replace("@", ""));
                                switch (dataType){
                                case "int": {
                                        definition += "int " + paramAux + ", ";
                                        bodyParameters += string.Format("\t\t\tdata.AddParam(\"{0}\", Library.Data.ParamType.Numeric, {1}.ToString());\n", parameterName, paramAux);
                                    }
                                    break;
                                case "bigint": {
                                        definition += "long " + paramAux + ", ";
                                        bodyParameters += string.Format("\t\t\tdata.AddParam(\"{0}\", Library.Data.ParamType.Numeric, {1}.ToString());\n", parameterName, paramAux);
                                    }
                                    break;
                                case "money": {
                                        definition += "float " + paramAux + ", ";
                                        bodyParameters += string.Format("\t\t\tdata.AddParam(\"{0}\", Library.Data.ParamType.Numeric, {1}.ToString());\n", parameterName, paramAux);
                                    }
                                    break;
                                case "float": {
                                        definition += "float " + paramAux + ", ";
                                        bodyParameters += string.Format("\t\t\tdata.AddParam(\"{0}\", Library.Data.ParamType.Numeric, {1}.ToString());\n", parameterName, paramAux);
                                    }
                                    break;
                                case "datetime": {
                                        definition += "DateTime " + paramAux + ", ";
                                        bodyParameters += string.Format("\t\t\tdata.AddParam(\"{0}\", Library.Data.ParamType.DateTime, {1}.ToString());\n", parameterName, paramAux);
                                    }
                                    break;
                                case "date": {
                                        definition += "DateTime " + paramAux + ", ";
                                        bodyParameters += string.Format("\t\t\tdata.AddParam(\"{0}\", Library.Data.ParamType.DateTime, {1}.ToString());\n", parameterName, paramAux);
                                    }
                                    break;
                                case "bit": {
                                        definition += "bool " + paramAux + ", ";
                                        bodyParameters += string.Format("\t\tdata.AddParam(\"{0}\", Library.Data.ParamType.Bool, {1}.ToString());\n", parameterName, paramAux);
                                    }
                                    break;
                                default: {
                                        definition += "string " + paramAux + ", ";
                                        bodyParameters += string.Format("\t\t\tdata.AddParam(\"{0}\", Library.Data.ParamType.Text, {1});\n", parameterName, paramAux);
                                    }
                                    break;
                                }
                                dsSp.MoveNext();
                            }
                            #endregion
                            body = definition.Substring(0, definition.LastIndexOf(',')) + "){\n";
                            body += "\t\t\tstring result = \"\";\n";
                            body += "\t\t\tLibrary.Data.DataSet data = new Library.Data.DataSet(this._connection);\n";
                            body += bodyParameters;
                            body += string.Format("\t\t\tdata.ExecuteProcedure(\"{0}.{1}\")", this.Schema, sp) + ";\n";
                            body += "\t\t\tresult = data.ToJson();\n";
                            body += "\t\t\treturn result;\n";
                            body += "\t\t}\n";
                        }
                    }
                    temp += body;
                    ds.MoveNext();
                }
            }
            return temp;
        }

        public string GenerateClass(){
            string database = (this.DatabaseName.IndexOf("_") >= 0 ? this.ConvertUpperToUpperLower(this.DatabaseName) : this.DatabaseName.Substring(0, 1).ToUpper() + this.DatabaseName.Substring(1).ToLower()); ;
            string temp = this.GenerateDeclaration() + this.GenerateConstructor();

            temp = "using System;\nusing Library.Tools;\nusing Library.Data;\n\nnamespace Library.Database." + database + (this.IsTable ? ".Table" : ".View") + " {\n\tpublic class " +
                this.ClassName + " : Library.Data.Transaction {\n" + temp + "\n\t}\n}";

            return temp;
        }

        public string GenerateGET(){
            string gets = string.Empty;
            try {
                string query = @"select procs.name from sys.procedures procs inner join sys.schemas on 
                procs.schema_id = schemas.schema_id where schemas.name = '{0}' 
                and procs.name LIKE '{1}_List%'";
                Library.Data.DataSet ds = new Data.DataSet(this.Connection);
                ds.ExecuteStatement(string.Format(query, this.Schema, this.ObjectName));
                if (!ds.EOF){
                    string sp = string.Empty;
                    while (ds.EOF == false){
                        sp = ds.FieldValue(0);
                        string body = "";
                        if (!String.IsNullOrEmpty(sp)){
                            string definition = "\n\t\t[Route(\"Get\")]";
                            definition += "\n\t\tpublic string Get(";

                            query = @"select distinct(PARAMETER_NAME),DATA_TYPE from information_schema.parameters
                            where specific_name='{0}'";
                            Library.Data.DataSet dsSp = new Data.DataSet(this.Connection);
                            dsSp.ExecuteStatement(string.Format(query, sp));
                            if (!dsSp.EOF){
                                string parameterName = "", dataType = "", bodyParameters = "", paramAux = "";
                                #region parameters
                                while (dsSp.EOF == false){
                                    parameterName = dsSp.FieldValue(0);
                                    dataType = dsSp.FieldValue(1);
                                    paramAux = this.ConvertToUpperLower(parameterName.Replace("@", ""));
                                    bodyParameters += paramAux + ", ";
                                    #region switch
                                    switch (dataType){
                                    case "int": {
                                            definition += "int " + paramAux + ", ";
                                        }
                                        break;
                                    case "bigint": {
                                            definition += "long " + paramAux + ", ";
                                        }
                                        break;
                                    case "money": {
                                            definition += "float " + paramAux + ", ";
                                        }
                                        break;
                                    case "float": {
                                            definition += "float " + paramAux + ", ";
                                        }
                                        break;
                                    case "datetime": {
                                            definition += "DateTime " + paramAux + ", ";
                                        }
                                        break;
                                    case "date": {
                                            definition += "DateTime " + paramAux + ", ";
                                        }
                                        break;
                                    case "bit": {
                                            definition += "bool " + paramAux + ", ";
                                            bodyParameters += paramAux + ", ";
                                        }
                                        break;
                                    default: {
                                            definition += "string " + paramAux + ", ";
                                        }
                                        break;
                                    }
                                    #endregion
                                    dsSp.MoveNext();
                                }
                                #endregion

                                bodyParameters = bodyParameters.Substring(0, bodyParameters.LastIndexOf(','));

                                body = definition.Substring(0, definition.LastIndexOf(',')) + "){\n";
                                body += "\t\t\tstring result = \"\";";
                                body += String.Format("\n\t\t\tif (my{0} == null){{ my{0} = new {0}(connection);}}\n", this.ClassName);
                                body += "\t\t\tresult = my" + this.ClassName + "." + sp.Replace(this.ObjectName + "_", "") + "(" + bodyParameters + ");\n";
                                body += "\t\t\treturn result;\n";
                                body += "\t\t}\n";
                            }
                        }
                        gets += body;
                        ds.MoveNext();
                    }
                }
            } catch (Exception){
                return string.Empty;
            }
            return gets;
        }


        public string GenerateController(){

            string database = (this.DatabaseName.IndexOf("_") >= 0 ? this.ConvertUpperToUpperLower(this.DatabaseName) : this.DatabaseName.Substring(0, 1).ToUpper() + this.DatabaseName.Substring(1).ToLower()); ;

            string controllerDef = "using Library.Database.Core.Table;\nusing System;\nusing System.Net;\nusing System.Net.Http;\nusing System.Web.Http;\nusing System.Web.Http.Cors;";
            controllerDef += "\n\nnamespace Library.Owin.Controller{";
            controllerDef += "\n\t[EnableCors(origins: \"*\", headers: \"*\", methods: \"GET,PUT,POST,DELETE,OPTIONS\")]";
            controllerDef += string.Format("\n\t[RoutePrefix(\"{0}\")]", this.ClassName.ToLower());
            controllerDef += "\n\tpublic class " + this.ClassName + "Controller : ApiController {";
            controllerDef += string.Format("\n\t\tprivate string connection = System.Configuration.ConfigurationManager.ConnectionStrings[\"{0}\"].ConnectionString;", this.DatabaseName);
            controllerDef += string.Format("\n\t\tprivate {0} my{1} = null;", this.ClassName, this.ClassName);

            //Get by Default
            controllerDef += "\n\t\t[Route(\"Get\")]";
            controllerDef += "\n\t\tpublic string Get(int id){";
            controllerDef += String.Format("\n\t\t\tif (my{0} == null){{ my{0} = new {0}(connection);}}", this.ClassName);
            controllerDef += String.Format("\n\t\t\treturn my{0}.JsonMe(id);", this.ClassName);
            controllerDef += "\n\t\t}";

            //
            controllerDef += this.GenerateGET();
            //Delete
            controllerDef += "\n\t\t[Route(\"Delete\")]";
            controllerDef += "\n\t\tpublic HttpResponseMessage Delete(int id){";
            controllerDef += "\n\t\t\ttry {";
            controllerDef += string.Format("\n\t\t\t\tif (my{0} == null){{ my{0} = new {0}(connection);}}", this.ClassName);
            controllerDef += string.Format("\n\t\t\t\tmy{0}.Delete(id);", this.ClassName);
            controllerDef += string.Format("\n\t\t\t\treturn Request.CreateResponse<{0}>(HttpStatusCode.OK, my{0});", this.ClassName);
            controllerDef += "\n\t\t\t} catch (Exception){";
            controllerDef += string.Format("\n\t\t\t\treturn Request.CreateResponse<{0}>(HttpStatusCode.InternalServerError, my{0});", this.ClassName);
            controllerDef += "\n\t\t\t}";
            controllerDef += "\n\t\t}";

            //Post
            controllerDef += "\n\t\t[Route(\"Post\")]";
            controllerDef += string.Format("\n\t\tpublic void Post(Library.Database.Core.Table.{0} me){{", this.ClassName);
            controllerDef += String.Format("\n\t\t\tif (my{0} == null){{ my{0} = new {0}(connection);}}", this.ClassName);
            controllerDef += "\n\t\t\tme._connection = connection;";
            controllerDef += String.Format("\n\t\t\tme.DbObjectName = my{0}.DbObjectName;", this.ClassName);
            controllerDef += String.Format("\n\t\t\tme.SchemaName = my{0}.SchemaName;", this.ClassName);
            controllerDef += "\n\t\t\tme.Save();";
            controllerDef += "\n\t\t}";
            controllerDef += "\n\t}";
            controllerDef += "\n}";
            return controllerDef;
        }

        public string AddTabs(string fieldName){return this.AddTabs(fieldName, 5, 8);}

        public string AddTabs(string fieldName, int paramMaxTabs){return this.AddTabs(fieldName, paramMaxTabs, 8);}

        public string AddTabs(string fieldName, int paramMaxTabs, int paramTabLength){
            int maxLength = paramTabLength * paramMaxTabs;
            double tabCount = Convert.ToDouble(maxLength - fieldName.Length) / 8;
            int tabs = Convert.ToInt32(System.Math.Floor(tabCount));
            tabs = (((maxLength - fieldName.Length) % paramTabLength) > 0) ? tabs + 1 : tabs;
            for (int intRow = 1; intRow <= tabs; intRow++){fieldName = fieldName + "\t";}
            if (tabs == 0){fieldName = fieldName + " ";}
            return fieldName;
        }

        public string AddLine(string columns, string newColumn){
            int lastPosition = columns.LastIndexOf("\n");
            string lastLine = (lastPosition > 0 ? columns.Substring(lastPosition + 1) : columns);
            columns = columns + (newColumn.Length + lastLine.Length + 2 > 128 ? "\n\t" : "");
            return columns;
        }

        public string BreakIntoLines(string myString){return this.BreakIntoLines(myString, 1);}

        public string BreakIntoLines(string myString, int tabsBefore){
            string[] array = myString.Split(" ".ToCharArray());
            string[] lines = new string[100];
            string temp = ""; int lineCount = -1; int wordCount = 0; string tabs = "";

            for (int row = 1; row <= tabsBefore; row++){tabs = tabs + "\t";}

            foreach (string word in array){
                if (temp.Length + word.Trim().Length > 120 && word.Trim().Length > 3){lineCount++; lines[lineCount] = temp; temp = ""; wordCount = 0;}
                wordCount++;
                if (wordCount == 1){temp = tabs;}
                temp = temp + (wordCount > 1 ? " " : "") + word.Trim();
            }

            if (wordCount > 0){lineCount++; lines[lineCount] = temp;}
            temp = "";

            for (int row = 1; row <= lines.Length; row++){
                if (Validation.IsNull(lines[row - 1]) == true){break;}
                temp = temp + lines[row - 1] + "\n";
            }

            return temp;
        }

        public string SeparteUpperCaseWords(string textToConvert){
            textToConvert = textToConvert.Replace("_", "");
            string upper = "ABCDEFGHIJKLMNOPQRSTUVWXYZ"; string letter = ""; string result = "";

            for (int index = 0; textToConvert.Length > index; index++){
                if (index == 0){result = textToConvert.Substring(0, 1); continue;}
                letter = textToConvert.Substring(index, 1);
                if (upper.IndexOf(letter) >= 0){result = result + " " + letter;} else { result = result + letter;}
            }

            return result.Trim();
        }

        public string ConvertToUpperLower(string value){
            string[] words = value.Split(" _#".ToCharArray());
            string result = "";

            for (int index = 0; words.Length > index; index++){
                words[index] = words[index].Substring(0, 1).ToUpper() + words[index].Substring(1).ToLower();
                result = result + words[index];
            }

            return result;
        }

        public string ConvertUpperToUpperLower(string textToConvert){
            string endWord = "";

            if (textToConvert.ToLower().IndexOf("_id_") > 0){
                int end = textToConvert.ToLower().IndexOf("_id_");
                endWord = textToConvert.Substring(end + 3);
            }

            string newTextToConvert = (endWord.Length > 0 ? textToConvert.Replace(endWord, "") : textToConvert);
            string[] words = newTextToConvert.Split("_".ToCharArray()[0]);
            string convertedText = "";

            if (words.Length == 1){convertedText = convertedText + words[0].Substring(0, 1).ToUpper() + words[0].Substring(1);} else {
                for (int index = 0; words.Length > index; index++){
                    convertedText = convertedText + words[index].Substring(0, 1).ToUpper() + words[index].Substring(1).ToLower();
                }
            }

            words = convertedText.Split("#".ToCharArray()[0]);
            convertedText = "";

            if (words.Length == 1){convertedText = convertedText + words[0].Substring(0, 1).ToUpper() + words[0].Substring(1);} else {
                for (int index = 0; words.Length > index; index++){
                    convertedText = convertedText + (convertedText.Length > 0 ? "_" : "") + words[index].Substring(0, 1).ToUpper() + words[index].Substring(1);
                }
            }

            convertedText = convertedText + (endWord.Length > 0 ? endWord : "");
            return convertedText;
        }
    }
}
