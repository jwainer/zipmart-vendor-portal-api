using System;
using System.Web.Http;
using System.Collections.Generic;

namespace Library.Data {
    public class Transaction {
        public virtual string connection {get; set;}

        private string modifiedUser {get; set;}

        private string objectName {get; set;}

        private string schemaName {get; set;}

        public long Id {get; set;}

        public Transaction(){}

        public Transaction(string dbConnect, string objectName){this.initialize(dbConnect, "", 0, objectName);}

        public Transaction(string dbConnect, string modifiedUser, long id, string objectName){this.initialize(dbConnect, modifiedUser, id, objectName);}
		
		public Transaction(string dbConnect, long id, string objectName){this.initialize(dbConnect, "", id, objectName);}

		private void initialize(string dbConnect, string modifiedUser, long id, string objectName){
            connection = dbConnect;

            if (objectName.Contains(".")){
                var split = objectName.Split('.');
                this.schemaName = split[0];
                this.objectName = split[1];

            } else {
				this.schemaName = "dbo";
				this.objectName = objectName;
			}

            this.modifiedUser = modifiedUser;
            this.Id = id;
            if (this.Id > 0){this.retrievedata();}
		}

        private void retrievedata(){
            Library.Data.DataSet data = new Library.Data.DataSet(this.connection);
            data.AddParam("@" + this.upperLower(this.objectName + "_ID"), Library.Data.ParamType.Numeric, this.Id.ToString());
            data.ExecuteProcedure(this.schemaName + "." + this.objectName + "_Retrieve");

            if (data.EOF == true){this.Clear(); return;}

            foreach (System.Reflection.FieldInfo field in this.GetType().GetFields()){
                if (data.FieldNameExists(field.Name) > -1){

                    if (String.Compare(field.FieldType.Name, "int32", true) == 0){
                        field.SetValue(this, int.Parse(data[field.Name]));

                    } else if (String.Compare(field.FieldType.Name, "int64", true) == 0 || String.Compare(field.FieldType.Name, "long", true) == 0){
                        field.SetValue(this, long.Parse(data[field.Name]));

                    } else if (String.Compare(field.FieldType.Name, "float", true) == 0 || String.Compare(field.FieldType.Name, "single", true) == 0){
                        field.SetValue(this, float.Parse(data[field.Name]));

                    } else if (String.Compare(field.FieldType.Name, "decimal", true) == 0){
                        field.SetValue(this, decimal.Parse(data[field.Name]));

                    } else if (String.Compare(field.FieldType.Name, "datetime", true) == 0){
                        field.SetValue(this, DateTime.Parse(data[field.Name]));

                    } else if (String.Compare(field.FieldType.Name, "bool", true) == 0 || String.Compare(field.FieldType.Name, "boolean", true) == 0){
                        field.SetValue(this, bool.Parse(data[field.Name]));

                    } else {
                        field.SetValue(this, data[field.Name]);
                    }
                }
            }
        }

        public void Clear(){
            System.Type type = this.GetType();
            foreach (System.Reflection.FieldInfo field in type.GetFields()){field.SetValue(this, null);}
        }

        public void Save(){
            System.Type type = this.GetType();
            Library.Data.DataSet data = new Library.Data.DataSet(connection);

            foreach (System.Reflection.FieldInfo field in type.GetFields()){
                if (field.Name.ToLower() == "id"){continue;}
                if (field.Name.ToLower() == "connection"){continue;}
                if (field.Name.ToLower() == "modifieduser"){continue;}
                if (field.Name.ToLower() == "objectname"){continue;}
				if (field.Name.ToLower() == "schemaname"){continue;}

                Library.Data.ParamType dataType = Library.Data.ParamType.Text;

                if (field.FieldType.Name.ToLower().Contains("int") || field.FieldType.Name.ToLower().Contains("float") || field.FieldType.Name.ToLower().Contains("decimal")){
                    dataType = Library.Data.ParamType.Numeric;

                } else if (field.FieldType.Name.ToLower().Contains("string") || field.FieldType.Name.ToLower().Contains("char")){
                    dataType = Library.Data.ParamType.Text;

                } else if (field.FieldType.Name.ToLower().Contains("date")){
                    dataType = Library.Data.ParamType.DateTime;

                } else if (field.FieldType.Name.ToLower().Contains("bool")){
                    dataType = Library.Data.ParamType.Bool;
                }

                data.AddParam("@" + field.Name, dataType, field.GetValue(this) != null ? field.GetValue(this).ToString() : "");
            }

            data.AddParam("@ModifiedBy", Library.Data.ParamType.Numeric, this.modifiedUser);
            data.ExecuteProcedure(this.schemaName + "." + this.objectName + "_Save");
        }

        public void Load(Dictionary<string, string> dict){
            foreach (System.Reflection.FieldInfo field in this.GetType().GetFields()){
                if (dict.ContainsKey(field.Name) == true){

                    if (String.Compare(field.FieldType.Name, "int32", true) == 0){
                        field.SetValue(this, int.Parse(dict[field.Name]));

                    } else if (String.Compare(field.FieldType.Name, "int64", true) == 0 || String.Compare(field.FieldType.Name, "long", true) == 0){
                        field.SetValue(this, long.Parse(dict[field.Name]));

                    } else if (String.Compare(field.FieldType.Name, "float", true) == 0 || String.Compare(field.FieldType.Name, "single", true) == 0){
                        field.SetValue(this, float.Parse(dict[field.Name]));

                    } else if (String.Compare(field.FieldType.Name, "decimal", true) == 0){
                        field.SetValue(this, decimal.Parse(dict[field.Name]));

                    } else if (String.Compare(field.FieldType.Name, "datetime", true) == 0){
                        field.SetValue(this, DateTime.Parse(dict[field.Name]));

                    } else if (String.Compare(field.FieldType.Name, "bool", true) == 0 || String.Compare(field.FieldType.Name, "boolean", true) == 0){
                        field.SetValue(this, bool.Parse(dict[field.Name]));

                    } else {
                        field.SetValue(this, dict[field.Name]);
                    }
                }
            }
        }

        public void Delete(){
            Library.Data.DataSet data = new Library.Data.DataSet(this.connection);
            data.AddParam("@" + this.upperLower(this.objectName + "_ID"), Library.Data.ParamType.Numeric, this.Id.ToString());
            data.ExecuteProcedure(this.schemaName + "." + this.objectName + "_Delete");

            this.Clear();
        }

        public Dictionary<string, object> ToJson(){
            Dictionary<string, object> row;
            System.Type type = this.GetType();

            row = new Dictionary<string, object>();
            foreach (System.Reflection.FieldInfo field in type.GetFields()){
                if (field.Name.ToLower() == "connection"){continue;}
                if (field.Name.ToLower() == "modifieduser"){continue;}
                if (field.Name.ToLower() == "objectname"){continue;}
				if (field.Name.ToLower() == "schemaname"){continue;}

                row.Add(field.Name, field.GetValue(this));
            }

            return row; 
        }
       
        private string upperLower(string textToConvert){
            string endWord = "";

            if (textToConvert.ToLower().IndexOf("_id_") > 0){
                int end = textToConvert.ToLower().IndexOf("_id_");
                endWord = textToConvert.Substring(end + 3);
            }

            string newTextToConvert = (endWord.Length > 0 ? textToConvert.Replace(endWord, "") : textToConvert);
            string[] words = newTextToConvert.Split("_".ToCharArray()[0]);
            string convertedText = "";

            if (words.Length == 1){convertedText = convertedText + words[0].Substring(0, 1).ToUpper() + words[0].Substring(1);} else {
                for (int index = 0; words.Length > index; index++){
                    convertedText = convertedText + words[index].Substring(0, 1).ToUpper() + words[index].Substring(1).ToLower();
                }
            }

            words = convertedText.Split("#".ToCharArray()[0]);
            convertedText = "";

            if (words.Length == 1){convertedText = convertedText + words[0].Substring(0, 1).ToUpper() + words[0].Substring(1);} else {
                for (int index = 0; words.Length > index; index++){
                    convertedText = convertedText + (convertedText.Length > 0 ? "_" : "") + words[index].Substring(0, 1).ToUpper() + words[index].Substring(1);
                }
            }

            convertedText = convertedText + (endWord.Length > 0 ? endWord : "");
            return convertedText;
        }
    }
}
