using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using Library.Tools;
using System.Collections.Generic;

namespace Library.Data {
	public enum ParamType{Text, Numeric, DateTime, Bool}

	public class Param {
		public Param(string name, string value, ParamType type){this.Name = name; this.Value = value; this.Type = type;}

		public string Name {get; set;}

		public string Value {get; set;}

		public ParamType Type {get; set;}
	}

	public class DataSet {
		private SqlConnection connection = new SqlConnection();
        private SqlDataReader recordset;
		public Param[] Parameters = new Param[0]; 

		public DataSet(){}

		public DataSet(string connection){this.Connection = connection;}

		public string Connection {get {return connection.ConnectionString;} set {connection.ConnectionString = value;}}
		
		private string query = ""; 
		public string Query{get {if (this.query.Length == 0){this.CreateQueryString();}; return this.query;} set {this.query = value;}}

		private string procedure; 
		public string Procedure{get {return procedure;} set {procedure = value;}}

		private int timeout = 90; 
		public int Timeout{get {return this.timeout;} set {this.timeout = value;}}

		public void AddParam(string name, float value){this.AddParam(name, ParamType.Numeric, value.ToString(), false);}
		
		public void AddParam(string name, string value){this.AddParam(name, ParamType.Text, value, false);}
		
		public void AddParam(string name, DateTime value){this.AddParam(name, ParamType.DateTime, value.ToString(), false);} 
		
		public void AddParam(string name, ParamType type, string value){this.AddParam(name, type, value, false);}
		
		public void AddParam(string name, ParamType type, string value, bool nullifblank){
			int paramID = -1; if (Library.Tools.Validation.IsNull(value) == true){value = "";}

			if (type == ParamType.DateTime && Library.Tools.Validation.IsDate(value) == false && value != "NULL"){value = "";}
			if (type == ParamType.Numeric && Library.Tools.Validation.IsNumeric(value) == false){value = "0";}
			if (type == ParamType.Text){value = value.Replace("'", "''");}

			if (nullifblank == true){
				if ((Library.Tools.Validation.IsNull(value)) || (value.Length == 0) || (value == "0")){
					value = "NULL"; type = ParamType.Numeric;
				}
			}
		
			int count = this.Parameters.Length;
			for (int row = 0; row < count; row++){if (Parameters[row].Name.ToLower() == name.ToLower()){paramID = row; break;}}
			
			if (paramID == -1){
				count++; 
				paramID = count - 1;

				Param param = new Param(name, value, type);
				Param[] arrTemp = new Param[Parameters.Length + 1]; 

				Parameters.CopyTo(arrTemp, 0);
				Parameters = new Param[count];

				arrTemp.CopyTo(Parameters, 0);
				Parameters.SetValue(param, count - 1);
				
			} else {this.Parameters[paramID].Value = value;}
		}

		public virtual void ClearParams(){Parameters = new Param[0];}

		public string CreateQueryString(){return this.CreateQueryString("");}

		public string CreateQueryString(string procedure){
			if (procedure.Length > 0){this.Procedure = procedure;}
			procedure = this.Procedure;
		
			string strname = "";
			string query = "EXEC " + procedure;

			for (int row = 0; row < this.Parameters.Length; row++){
				if (row > 0){query = query + ",";}
			
				strname = Parameters[row].Name.Length == 0 ? "": Parameters[row].Name + " = ";
			
				switch (Parameters[row].Type){
					case ParamType.DateTime:	goto case ParamType.Text;
					case ParamType.Text:		query = query + " " + strname + "'" + Parameters[row].Value + "'";	break;
					case ParamType.Numeric:	    query = query + " " + strname + Parameters[row].Value; break;
					case ParamType.Bool:		query = query + " " + strname + (Parameters[row].Value.ToLower() == "true" || Parameters[row].Value == "1" ? "1" : "0"); break;	
				}
			}

			return query;
		}
	
		public virtual void ExecuteProcedure(string procedure){ExecuteProcedure(procedure, true);}
		
		public virtual void ExecuteProcedure(string procedure, bool clearParams){
			SqlCommand command = new SqlCommand();
		
			this.query = this.CreateQueryString(procedure);

			command.Connection = this.connection;
			command.CommandType = CommandType.Text; 
			command.CommandText = query;
			command.CommandTimeout = this.timeout;
			
			try {command.Connection.Open();} catch {command.Connection.Close(); command.Connection.Open();}
			recordset = command.ExecuteReader();
			if (clearParams == true){this.ClearParams();} 
			this.MoveNext();
			System.GC.Collect();
		}

		public virtual void ExecuteStatement(string query){
			SqlCommand command = new SqlCommand();
			this.ClearParams();
			this.query = query;

			if (this.connection.State != System.Data.ConnectionState.Closed){this.connection.Close();}
			this.connection.Open();

			command.Connection = this.connection;
			command.CommandType = CommandType.Text;
			command.CommandText = query;
			command.CommandTimeout = this.timeout;

			recordset = command.ExecuteReader();

			this.MoveNext();
			System.GC.Collect();
		}

		public int RecordCount { 
			get { 
				if (recordset.RecordsAffected > 0){return recordset.RecordsAffected;}

				if (recordset.FieldCount > 0){
					int fieldCount = recordset.FieldCount;
					string fieldName = recordset.GetName(fieldCount - 1);
					if (fieldName == "RecordCount"){return (this.EOF == true ? 0 : int.Parse(recordset["RecordCount"].ToString()));}
				}

				return 0;
			} set {this.RecordCount = value;}
		}

		public virtual void MoveNext(int count){for (int row = 1; count >= row; row++){recordset.Read();}}

		public virtual void MoveNext(){this.eof = !recordset.Read();}

		private bool eof = false; 
		public bool EOF{get {return eof;}}

		public string this[int index]{get {if (this.eof == true){return null;} else {return recordset[index].ToString();}}}
		
		public string this[string name]{get {if (this.eof == true){return null;} else {return recordset[name].ToString();}}}

        public int FieldCount {get {return recordset.FieldCount;}}

        public string FieldName(int id){string name; try {name = recordset.GetName(id);} catch {name = "";} return name;}

        public string FieldValue(int id){return recordset.GetValue(id)==null?"":recordset.GetValue(id).ToString();}

        public int FieldNameExists(string name){
            int index = -1;

            for (int indexField = 0; indexField < this.FieldCount; indexField++){
                if (String.Compare(this.FieldName(indexField), name, true) == 0){
                    index = indexField;
                    break;
                }
            }

            return index;
        }

		public override string ToString(){return this.query;}

        public List<Dictionary<string, object>> ToJson(){
            List<Dictionary<string, object>> rows = new List<Dictionary<string, object>>();
            Dictionary<string, object> row;

            while (this.EOF == false){
                row = new Dictionary<string, object>();

                for (int index = 0; index < this.FieldCount; index++){
                    row.Add(this.FieldName(index), this.FieldValue(index));
                }

                rows.Add(row);
                this.MoveNext();
            }

            return rows; 
        }
	}
}