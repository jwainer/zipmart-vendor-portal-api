using System;

namespace Library.Data {
	public enum DataType {StoredProcedure, Table, View, Function, Trigger, None};

	public class Connection {
		public Connection(){}
		
		public Connection(string connectionName, string serverName, string databaseName, string user, string password, string ssserver, string ssproject, string sspassword, string workingFolder){
			this.ConnectionName = ConnectionName;
			this.ServerName = serverName;
			this.DatabaseName = databaseName;
			this.User = user;
			this.Password = password;
		}

		public Connection(string connectionName, string serverName, string databaseName, string user, string password, string workingFolder){
			this.ConnectionName = ConnectionName;
			this.ServerName = serverName;
			this.DatabaseName = databaseName;
			this.User = user;
			this.Password = password;
			this.SSServer = "";
			this.SSProject = "";
			this.SSPassword = "";
			this.WorkingFolder = workingFolder;
			this.IsSourceSafed = false;
		}

		private string connection = ""; 
		public string ConnectionName{get {return this.connection;} set {this.connection = value;}}

		private string server = ""; 
		public string ServerName{get {return this.server;} set {this.server = value;}}

		private string database = ""; 
		public string DatabaseName{get {return this.database;} set {this.database = value;}}

		private string user = ""; 
		public string User{get {return this.user;} set {this.user = value;}}

		private string password = ""; 
		public string Password{get {return this.password;} set {this.password = value;}}

		private string ssserver = ""; 
		public string SSServer{get {return this.ssserver;} set {this.ssserver = value;}}

		private string ssuser = ""; 
		public string SSUser{get {return this.ssuser;} set {this.ssuser = value;}}

		private string sspassword = ""; 
		public string SSPassword{get {return this.sspassword;} set {this.sspassword = value;}}

		private string ssproject = ""; 
		public string SSProject{get {return this.ssproject;} set {this.ssproject = value;}}

		private string workingFolder = ""; 
		public string WorkingFolder{get {return this.workingFolder;} set {this.workingFolder = value;}}

		private bool isLegacy = false; 
		public bool IsLegacy{get {return this.isLegacy;} set {this.isLegacy = value;}}

		private bool isSourceSafed = false; 
		public bool IsSourceSafed{get {return this.isSourceSafed;} set {this.isSourceSafed = value;}}

		private int sourceSafeSpeed = 0; 
		public int SourceSafeSpeed{get {return this.sourceSafeSpeed;} set {this.sourceSafeSpeed = value;}}
		
		public static string GetPath(Library.Data.DataType type){
			string path = "";
			
			switch (type){
				case DataType.StoredProcedure:	path = @"/Stored Procedures/"; break;
				case DataType.Table:			path = @"/Tables/"; break;
				case DataType.Trigger:			path = @"/Triggers/"; break;
				case DataType.View:				path = @"/Views/"; break;
				case DataType.Function:			path = @"/Functions/"; break;
			}
			
			return path;
		}

		public static Library.Data.DataType GetType(string path){
			Library.Data.DataType type = Library.Data.DataType.None;
			
			switch (path){
				case "Stored Procedures":	type = Library.Data.DataType.StoredProcedure; break;
				case "Views":				type = Library.Data.DataType.View; break;
				case "Tables":				type = Library.Data.DataType.Table; break;
				case "Triggers":			type = Library.Data.DataType.Trigger; break;
				case "Functions":			type = Library.Data.DataType.Function; break;
			}	
			
			return type;
		}	
	}
}