﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Collections;
using DAL;
using System.Xml.Linq;
using BLL.ZetAppSettingsService;
namespace BLL
{
   public class PasswordPolicy
    {
        /// <summary>
        /// This method gets password policy data from 
        /// </summary>
        /// <returns></returns>
       public static   Dictionary<string, string> GetPasswordPolicyData()
        {
            Dictionary<string, string> passwordPolicy = new Dictionary<string, string>();
            using (AppsClient zClient = new AppsClient())
            {
                string token = zClient.GetToken();
                passwordPolicy = zClient.GetAppSettings(new string[] { "PasswordPolicyXML", "PasswordPolicyHTML" }, token);
            }
            return passwordPolicy;
        }

        /// <summary>
        /// This method pulls out Password Polocy XML from database and generates regex to validate password
        /// </summary>
        /// <returns>regex string </returns>
        public static string GetPasswordRegex()
        {
            Dictionary<string, string> passwordPolicy  = GetPasswordPolicyData();
            string passwordPolicyXML = passwordPolicy["PasswordPolicyXML"].ToString();

            if (!string.IsNullOrEmpty(passwordPolicyXML))
            {
                System.Xml.Linq.XDocument xmlDoc = XDocument.Parse(passwordPolicyXML);

                var passwordSetting = (from p in xmlDoc.Descendants("Password")
                                       select new
                                       {
                                           MinLength = int.Parse(p.Element("minLength").Value),
                                           MaxLength = int.Parse(p.Element("maxLength").Value),
                                           NumsLength = int.Parse(p.Element("numsLength").Value),
                                           SpecialLength = int.Parse(p.Element("specialLength").Value),
                                           UpperLength = int.Parse(p.Element("upperLength").Value),
                                           SpecialChars = p.Element("specialChars").Value
                                       }).First();
                StringBuilder sbPasswordRegx = new StringBuilder(string.Empty);
                //min and max
                sbPasswordRegx.Append(@"(?=^.{" + passwordSetting.MinLength + "," + passwordSetting.MaxLength + "}$)");

                //numbers length
                sbPasswordRegx.Append(@"(?=(?:.*?\d){" + passwordSetting.NumsLength + "})");

                //a-z characters
                sbPasswordRegx.Append(@"(?=.*[a-z])");

                //A-Z length
                sbPasswordRegx.Append(@"(?=(?:.*?[A-Z]){" + passwordSetting.UpperLength + "})");

                //special characters length
                sbPasswordRegx.Append(@"(?=(?:.*?[" + passwordSetting.SpecialChars + "]){" + passwordSetting.SpecialLength + "})");

                ////(?!.*\s) - no spaces
                ////[0-9a-zA-Z!@#$%*()_+^&] -- valid characters
                sbPasswordRegx.Append(@"(?!.*\s)[0-9a-zA-Z" + passwordSetting.SpecialChars + "]*$");

                return sbPasswordRegx.ToString();
            }
            return string.Empty;
        }
    }
}
