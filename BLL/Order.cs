﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Configuration;
using DAL;

namespace BLL
{
    public class Order
    {
        #region ' Properties '

        public decimal SubTotal { get; set; }
        public decimal Discount { get; set; }
        public decimal Tax { get; set; }
        public decimal Total { get; set; }
        public string PromoCode { get; set; }
        public bool FreeCourses { get; set; }
        public long ShoppingCartId { get; set; }

        #endregion

        #region ' Methods '

        public Order(long userId, int vendorId, bool loggingIn = false, string promoCode = null)
        {
            this.FreeCourses = false;

            using (ZipMartEntities ze = new ZipMartEntities())
            {
                MpUser mpUser = ze.MpUser.Get(userId);
                MpVendor vendor = ze.MpVendor.Get(vendorId);

                if (mpUser != null && mpUser.MpShoppingCart != null && mpUser.MpShoppingCart.MpShoppingSeat.Where(x => x.VendorId == vendorId).Count() > 0)
                {
                    if (loggingIn && promoCode == null)
                    {
                        mpUser.MpShoppingCart.PromotionCode = null;
                        ze.SaveChanges();
                    }

                    this.PromoCode = mpUser.MpShoppingCart.PromotionCode;

                    decimal taxRate;
                    MpKeyValue kv = ze.MpKeyValue.Where(x => x.Key == "TaxRate").Single();
                    taxRate = decimal.Parse(kv.Value);

                    //this.SubTotal = subTotal;
                    DateTime curDate = DateTime.Now.Date;
                    double effectivePrice = 0;
                    double subTotal = 0;
                    decimal discountRate = 0;

                    bool applyToAll = false;
                    MpPromotion mpPromotion = mpUser.MpShoppingCart.MpPromotion;

                    Dictionary<int, int> shoppingseats = new Dictionary<int, int>();
                    int courseId = 0;
                    int qty = 0;

                    MpShoppingCart shoppingCart = mpUser.MpShoppingCart;
                    if (shoppingCart != null)
                    {
                        this.ShoppingCartId = shoppingCart.CartId;

                        foreach (var sSeat in shoppingCart.MpShoppingSeat.Where(x => x.VendorId == vendorId))
                        {
                            courseId = sSeat.MpCourseInstance.CourseId;
                            if (shoppingseats.TryGetValue(courseId, out qty))
                                shoppingseats[courseId] = qty + 1;
                            else
                                shoppingseats.Add(courseId, 1);
                        }
                    }

                    if (mpPromotion != null && mpPromotion.IsValid(vendorId))
                    {
                        //Get the promo code discount rate. this is calculating discount for entering a promo code
                        if (mpPromotion.Discount <= 1)
                        {
                            promoCode = mpPromotion.PromotionCode;
                            discountRate = (decimal)mpPromotion.Discount;
                            // If a valid promo code has no courses realated to it than apply it to all seats in the shopping cart.
                            if (mpPromotion.MpCourse.Count() == 0)
                                applyToAll = true;
                        }
                        subTotal = 0;
                        foreach (var sSeat in shoppingseats)
                        {
                            MpPromotion courseInstPromo = null;
                            MpCourse course = ze.MpCourse.Get(sSeat.Key);
                            effectivePrice = (double)CoursePricingClass.GetSubscribedCoursePrice(vendorId, sSeat.Key, null, userId, sSeat.Value, course.PublisherId ?? 0, vendor.VendorTypeId);            // Subscriber Price
                            subTotal += effectivePrice * sSeat.Value;

                            if (course.MpPromotion != null)
                                courseInstPromo = course.MpPromotion.Where(x => x.PromotionCode == promoCode && (x.StartDate == null || x.StartDate <= curDate) & (x.EndDate == null || x.EndDate >= curDate)).SingleOrDefault();

                            if (applyToAll)
                            {
                                if (course.MpPublisher == null || course.MpPublisher.AcceptAllCoursePromo == true)
                                    this.Discount += (decimal)(effectivePrice * sSeat.Value) * discountRate;                       // Promo discount apply to all
                            }
                            else if (courseInstPromo != null && courseInstPromo.Discount > 0 && courseInstPromo.Discount <= 1)
                            {
                                this.Discount += (decimal)(effectivePrice * sSeat.Value) * (decimal)courseInstPromo.Discount;      // Promo Discount apply per course
                            }
                        }
                    }
                    else
                    {
                        // No Promo code, Course Automatic Discount by Vendor and Subscription Discount
                        subTotal = 0;
                        foreach (var sSeat in shoppingseats)
                        {
                            MpCourse course = ze.MpCourse.Get(sSeat.Key);
                            effectivePrice = (double)CoursePricingClass.GetSubscribedCoursePrice(vendorId, sSeat.Key, null, userId, 1, course.PublisherId ?? 0, vendor.VendorTypeId);   // Subscriber Price
                            subTotal += effectivePrice * sSeat.Value;

                            if (vendor != null && vendor.MpVendorDiscount != null)
                            {
                                this.Discount += (decimal)(effectivePrice * sSeat.Value) * (decimal)vendor.MpVendorDiscount.Discount; // Vendor Discount
                            }
                        }
                    }
                    this.SubTotal = (decimal)Math.Round(subTotal, 2);
                    this.Tax = (this.SubTotal - this.Discount) * taxRate;
                    this.Total = this.SubTotal - this.Discount + this.Tax;
                    if (shoppingseats.Count() > 0 && this.Total == 0)
                        this.FreeCourses = true;
                    else
                        this.FreeCourses = false;
                }
            }
        }

        #endregion
    }
}
