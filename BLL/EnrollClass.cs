﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DAL;
namespace BLL
{
    public class EnrollClass
    {
        public int CourseId { get; set; }
        public int VendorId {get;set;}
        public decimal CoursePrice {get;set;}
        public MpUser MPUsr { get; set; }

        public void FreeCoursesEnrollment(List<CourseInCart> coursesInCart, string promoCode, ZipMartEntities ze)
        {
            MpCourseInstance ci = null;
            DateTime currdate = DateTime.Now;

            foreach (var cic in coursesInCart)
            {
                ci = ze.MpCourseInstance.Get(cic.CourseInstanceId);
                MpCourse crs = ci.MpCourse;
                if (crs.IsBundle == true && crs.MpCourseBundle.Count() > 0)
                {
                    this.CourseId = crs.CourseId; 
                    this.EnrollStudentToClass(true, promoCode);
                }
                else
                {
                    if (!ze.MpCourseSeat.Where(x => x.UserId == this.MPUsr.UserId && x.CourseInstanceId == cic.CourseInstanceId).Any()) // Enroll ONLY If the user never took this course
                    {
                        ci = ze.MpCourseInstance.Get(cic.CourseInstanceId);
                        this.CourseId = ci.MpCourse.CourseId;
                        this.CoursePrice = ci.Price;;
                        this.EnrollStudentToClass(true, promoCode);
                    }
                }
            }
        }

        public long EnrollStudentToClass(bool freeClass,  string promoCode)
        {
            using (ZipMartEntities ze = new ZipMartEntities())
            {
                DateTime currDate = DateTime.Now;
                MpCourse course = ze.MpCourse.Get(this.CourseId);
                if (course == null)
                    return 0;
                else
                {
                    MpCourseInstance courseInst = course.MpCourseInstance.FirstOrDefault(x => (x.EndDate == null | x.EndDate > DateTime.Now) & x.Active == true);
                    if (course.IsBundle == true)
                    {
                        MpCourseBundle[] coursesInBundle = course.MpCourseBundle.ToArray();
                        List<MpCourseInstance> courseInstances = new List<MpCourseInstance>(); 
                        foreach (var crs in coursesInBundle)
                        {
                            MpCourseInstance ci = crs.MpCourse1.MpCourseInstance.FirstOrDefault(x => (x.EndDate == null | x.EndDate > DateTime.Now) & x.Active == true);
                            if (ci != null)
                            {
                                courseInstances.Add(ci);
                            }
                        }
                        this.EnrollMultSeats(freeClass, ze, currDate, courseInstances.ToArray(), promoCode);
                        if (courseInst != null)
                            this.EmptyShoppingCart(ze, courseInst.CourseInstanceId);
                        return 0;
                    }
                    else
                    {
                        MpCourseInstance courseInstance = course.MpCourseInstance.FirstOrDefault(x => (x.EndDate == null | x.EndDate > DateTime.Now) & x.Active == true);
                        if (courseInstance == null)
                            return 0;
                        else if (freeClass)
                        {
                            long crseSeatId = this.EnrollOneSeat(freeClass, ze, currDate, courseInstance, promoCode);
                            if (courseInst != null)
                                this.EmptyShoppingCart(ze, courseInst.CourseInstanceId);
                            return crseSeatId;
                        }
                        else
                            return 0;
                    }
                }
            }

        }

        private long EnrollOneSeat(bool freeClass, ZipMartEntities ze, DateTime currDate, MpCourseInstance courseInstance, string promoCode)
        {
            MpCourseSeat existingCourseseat = ze.MpCourseSeat.Where(x => x.UserId == this.MPUsr.UserId && x.CourseInstanceId == courseInstance.CourseInstanceId && !x.Transferable).FirstOrDefault();
            MpPromotion promo = ze.MpPromotion.Get(promoCode, this.VendorId);

            if (existingCourseseat == null)
            {
                MpOrder order = new MpOrder() { UserId = this.MPUsr.UserId, RCD = currDate, Completed = currDate };
                order.MpOrderSeat.Add(new MpOrderSeat()
                {
                    CourseInstanceId = courseInstance.CourseInstanceId
                    ,
                    VendorId = this.VendorId
                    ,
                    Price = this.CoursePrice
                    ,
                    PromoApplied = (promo != null)
                    ,
                    MpCourseSeat = new List<MpCourseSeat>() { new MpCourseSeat() { UserId = this.MPUsr.UserId, CourseInstanceId = courseInstance.CourseInstanceId, Transferable = false } }
                });

                if (!string.IsNullOrEmpty(promoCode))
                {
                    if (promo != null)
                        order.MpOrderPromotion = new MpOrderPromotion() { Discount = promo.Discount, EndDate = promo.EndDate, PromotionCode = promo.PromotionCode, StartDate = promo.StartDate }; 
                }

                ze.MpOrder.Add(order);

                ze.SaveChanges();

                return order.MpOrderSeat.First().MpCourseSeat.First().CourseSeatId;
            }
            else
                return existingCourseseat.CourseSeatId;
        }

        private void EnrollMultSeats(bool freeClass, ZipMartEntities ze, DateTime currDate, MpCourseInstance[] cids, string promoCode)
        {
            MpPromotion promo = ze.MpPromotion.Get(promoCode, this.VendorId);

            if (cids.Length > 0)
            {
                MpOrder order = new MpOrder() { UserId = this.MPUsr.UserId, RCD = currDate, Completed = currDate };

                foreach (MpCourseInstance ci in cids)
                {
                    MpCourseSeat existingCourseseat = ze.MpCourseSeat.Where(x => x.UserId == this.MPUsr.UserId && x.CourseInstanceId == ci.CourseInstanceId && !x.Transferable).FirstOrDefault();
                    if (existingCourseseat == null)
                    {
                        order.MpOrderSeat.Add(new MpOrderSeat()
                        {
                            CourseInstanceId = ci.CourseInstanceId
                            ,
                            VendorId = this.VendorId
                            ,
                            Price = ci.Price
                            ,
                            PromoApplied = (promo != null)
                            ,
                            MpCourseSeat = new List<MpCourseSeat>() { new MpCourseSeat() { UserId = this.MPUsr.UserId, CourseInstanceId = ci.CourseInstanceId, Transferable = false } }
                        });
                    }
                }

                if (!string.IsNullOrEmpty(promoCode))
                {
                    if (promo != null)
                        order.MpOrderPromotion = new MpOrderPromotion() { Discount = promo.Discount, EndDate = promo.EndDate, PromotionCode = promo.PromotionCode, StartDate = promo.StartDate };
                }

                ze.MpOrder.Add(order);
                ze.SaveChanges();
            }
        }

        private void EmptyShoppingCart(ZipMartEntities ze, long courseInstanceId)
        {
            if (this.MPUsr.MpShoppingCart != null)
            {
                ze.uspDeleteShoppingSeats(MPUsr.MpShoppingCart.CartId, courseInstanceId, this.VendorId);
                this.MPUsr.MpShoppingCart.PromotionCode = null;
                ze.SaveChanges();
            }
        }

    }
}
