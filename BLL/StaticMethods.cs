﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DAL;

namespace BLL
{
    public static class StaticMethods
    {
        public static void StoreCustomerContactInfo(string fname, string lname, string email, string phoneNbr)
        {
            using (ZipMartEntities ze = new ZipMartEntities())
            {
                CustomerContactInfo contactInfo = ze.CustomerContactInfo.SingleOrDefault(x => x.CustomerEmailAddress == email);
                if (contactInfo == null)
                {
                    contactInfo = new CustomerContactInfo() { CustomerEmailAddress = email, CustomerName = string.Format("{0} {1}", fname, lname), CustomerPhone = phoneNbr, RCD = DateTime.Now };
                    ze.CustomerContactInfo.Add(contactInfo);
                }
                else if (!string.IsNullOrEmpty(phoneNbr) && phoneNbr.Length >= 10)
                {
                    contactInfo.CustomerPhone = phoneNbr;
                }
                ze.SaveChanges();
            }
        }

        public static Exception ExceptionMessage(Exception ex)
        {
            if (ex.InnerException != null)
                return ExceptionMessage(ex.InnerException);
            else
                return ex;
        }

        public static bool CreateOrderAndSeats(long userId, int vendorId, string sourceId, decimal paidAmount, decimal tax, decimal discount, decimal subTotal, long? invoiceId, string extraInfo, out MessageCodes rtnMessage)
        {

            using (ZipMartEntities ze = new ZipMartEntities())
            {
                MpVendor vendor = ze.MpVendor.Get(vendorId);
                MpUser mpUser = ze.MpUser.Get(userId);
                MpOrder order = mpUser.MpShoppingCart.ToMpOrder(vendor.VendorId, userId, vendor.VendorTypeId);

                if (order.MpOrderSeat.Count() == 0)
                {
                    rtnMessage = MessageCodes.Message_1060;
                    return true;
                }

                order.UserId = userId;
                order.RCD = DateTime.Now;
                order.InvoiceId = invoiceId;

                order.MpOrderPayment.Add(new MpOrderPayment() { SubTotal = subTotal, Discount = discount, Amount = paidAmount, Tax = tax, PaymentTypeId = 1, ExtraInfo = extraInfo });

                if (mpUser.MpShoppingCart.MpPromotion != null)
                {
                    MpOrderPromotion op = new MpOrderPromotion();
                    MpPromotion mpPromotion = mpUser.MpShoppingCart.MpPromotion;
                    op.PromotionCode = mpPromotion.PromotionCode;
                    op.StartDate = mpPromotion.StartDate;
                    op.EndDate = mpPromotion.EndDate;
                    op.Discount = mpPromotion.Discount;
                    order.MpOrderPromotion = op;
                }

               order.MpOrderQuickCheckout = new MpOrderQuickCheckout() { TransactionDate = order.RCD };

               mpUser.MpOrder.Add(order);


                if (ze.SaveChanges() > 0)
                {
                    List<int> courseIds = new List<int>();
                    List<decimal> coursePrices = new List<decimal>();

                    foreach (MpOrderSeat os in order.MpOrderSeat)
                    {
                        if (os.MpCourseInstance.MpCourse.IsBundle == true)
                        {
                            var courseBundles = ze.MpCourseBundle.Where(x => x.CourseId == os.MpCourseInstance.CourseId).ToList();

                            foreach (MpCourseBundle c in courseBundles)
                            {
                                MpCourseSeat courseSeat = new MpCourseSeat();
                                courseSeat.UserId = userId;
                                courseSeat.CourseInstanceId = ze.MpCourseInstance.FirstOrDefault(i => i.CourseId == c.CourseId_Child).CourseInstanceId; //"ID OF HTE COURSE THAT IS PART OF BUNDLE";
                                courseSeat.Transferable = true;
                                courseSeat.OrderSeatId = os.OrderSeatId;
                                courseSeat.ReachedMaxAttempt = false;
                                ze.MpCourseSeat.Add(courseSeat);

                                MpCourseVendor courseVendor = ze.MpCourseVendor.Where(x => x.CourseId == courseSeat.MpCourseInstance.MpCourse.CourseId && x.VendorId == 14).FirstOrDefault();
                                if (courseVendor != null)
                                {
                                    courseIds.Add(courseSeat.MpCourseInstance.MpCourse.CourseId);
                                    coursePrices.Add(courseSeat.MpCourseInstance.Price);
                                }
                            }
                        }
                        else
                        {
                            MpCourseSeat courseSeat = new MpCourseSeat();
                            courseSeat.UserId = userId;
                            courseSeat.CourseInstanceId = os.CourseInstanceId;
                            courseSeat.Transferable = true;
                            courseSeat.OrderSeatId = os.OrderSeatId;
                            courseSeat.ReachedMaxAttempt = false;
                            ze.MpCourseSeat.Add(courseSeat);

                            if (courseSeat.MpCourseInstance.MpCourse.FormatId == 4)
                                courseSeat.MpCourseInstance.Active = false;

                            MpCourseVendor courseVendor = ze.MpCourseVendor.Where(x => x.CourseId == courseSeat.MpCourseInstance.MpCourse.CourseId && x.VendorId == 14).FirstOrDefault();
                            if (courseVendor != null)
                            {
                                courseIds.Add(courseSeat.MpCourseInstance.MpCourse.CourseId);
                                coursePrices.Add(courseSeat.MpCourseInstance.Price);
                            }
                        }
                    }

                    var cartId = mpUser.MpShoppingCart.CartId;
                    var shoppingSeat = ze.MpShoppingSeat.Where(s => s.CartId == cartId && s.VendorId == vendorId).ToList();
                    ze.MpShoppingSeat.RemoveRange(shoppingSeat);
                    order.Completed = DateTime.Now;
                    ze.SaveChanges();

                    mpUser.MpShoppingCart.PromotionCode = null;
                    ze.SaveChanges();

                    //to make sure remove cart from MpShoppingCart table if no seat left in cart
                    shoppingSeat = ze.MpShoppingSeat.Where(s => s.CartId == cartId).ToList();
                    if (shoppingSeat == null || shoppingSeat.Count == 0)
                    {
                        ze.MpShoppingCart.Remove(mpUser.MpShoppingCart);
                    }

                    // send the verification email
                    using (ZET_EMAILEntities zEntity = new ZET_EMAILEntities())
                    {
                        EmailToSend email = new EmailToSend();
                        if (vendor != null && vendor.VendorTypeId == 4)
                        {
                            email.EmailSubject = new EmailSubject();
                            email.EmailSubject.Subject = string.Format("{0} Order Confirmation", vendor.Name);
                        }
                        email.EmailTypeId = 4;
                        email.Parameter = order.OrderId.ToString() + "|" + vendorId.ToString() + "|" + sourceId + "|";
                        email.Priority = 1;
                        email.CreatedDate = DateTime.Now;

                        using (BLL.ZetGatewayService.GatewayClient gClient = new BLL.ZetGatewayService.GatewayClient())
                        {
                            EmailAttachment attach = new EmailAttachment();
                            attach.Attachment = gClient.GenerateReceipt((long)order.OrderId, "ZipMart Receipt", BLL.ZetGatewayService.RenderingFormat.PDF, gClient.GetToken());
                            if (vendor != null && vendor.VendorTypeId == 4)
                                attach.FileName = "Receipt (" + order.OrderId.ToString() + ").pdf";
                            else
                                attach.FileName = "ZipMart Receipt (" + order.OrderId.ToString() + ").pdf";
                            email.EmailAttachment.Add(attach);
                        }
                        zEntity.EmailToSend.Add(email);
                        if (zEntity.SaveChanges() > 0)
                        {
                            if ((bool)zEntity.usp_SendEmail(email.EmailId).First())
                            {
                                rtnMessage = MessageCodes.Message_1024;
                                return true;
                            }
                            else
                            {
                                rtnMessage = MessageCodes.Message_1025;
                                return false;
                            }
                        }
                        else
                        {
                            rtnMessage = MessageCodes.Message_1025;
                            return false;
                        }
                    }
                }
                else
                {
                    rtnMessage = MessageCodes.Message_1026;
                    return false;
                }
            }
        }

    }
}
