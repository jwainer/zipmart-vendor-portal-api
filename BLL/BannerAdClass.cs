﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BLL
{
    public class BannerAdClass
    {
        public long RecId { get; set; }
        public string BannerName { get; set; }
        public string HTMLContents { get; set; }
        public long ImageId { get; set; }
        public bool ShowInMainCategory { get; set; }
        public byte BottomPadding { get; set; }
    }
}
