﻿using System;
using System.Collections.Generic;
using System.Linq;
using DAL;

namespace BLL
{
    public class AssociatedCourse
    {
        public int CourseId { get; set; }
        public string CourseName { get; set; }
        public long CourseImageId { get; set; }
        public decimal OldPrice { get; set; }
        public decimal CurrPrice { get; set; }
        public double Hours { get; set; }
        public string PublisherName { get; set; }
        public string Stars { get; set; }
    }

    public class CourseInCart : AssociatedCourse
    {
        public int Qty { get; set; }
        public long CourseInstanceId { get; set; }
    }

    public class Course
    {
        public string Nm { get; set; }
        public int Id { get; set; }
        public long ImgId { get; set; }
        public int? Seq { get; set; }
        public decimal? Pr { get; set; }
        public double? Hr { get; set; }
        public string PNm { get; set; }
        public string Ctgs { get; set; }
        public string Fmt { get; set; }
        public string Descr { get; set; }
    }

    public class CourseDetails
    {
        public string CourseName { get; set; }
        public long CourseImageId { get; set; }
        public string CourseDescription { get; set; }
    }

    public class CourseExtraInfo
    {
        public usp_GetCourses3_Result CourseRec { get; set; }
        public string CourseCategories { get; set; }
        public string FormattedPrice { get; set; }
        public string FormattedHour { get; set; }
        public int PublisherId { get; set; }
        public string PublisherName { get; set; }
        public bool IsBundle { get; set; }
        public string Discount { get; set; }
    }

    public class CourseBundleWithRating
    {
        public usp_GetCoursesBundle_Result Course { get; set; }
        public string CourseDescription { get; set; }
        public bool DescriptionTruncated { get; set; }
        public string StarRating { get; set; }
        public decimal Price { get; set; }
    }

    public class CourseMdl
    {
        public string Name { get; set; }
        public int CourseId { get; set; }
        public bool Selected { get; set; }
        public long ImageId { get; set; }
        public int? DispSeq { get; set; }
    }

    public class CoursePromotion
    {
        public int CourseId { get; set; }
        public string OrigPromoCode { get; set; }
        public string PromotionCode { get; set; }
        public double Discount { get; set; }
        public DateTime? StartDate { get; set; }
        public DateTime? EndDate { get; set; }
        public string Description { get; set; }
    }

    public class UpdtCourseCat
    {
        public int CourseId { get; set; }
        public CourseCategory[] Categories { get; set; }
    }

    public class UpdtCatCourse
    {
        public int CategoryId { get; set; }
        public int[] CourseIds { get; set; }
    }

    public class Category
    {
        public int CategoryId { get; set; }
        public string CategoryName { get; set; }
        public int? ParentId { get; set; }
    }

    public class CourseCategory
    {
        public int CategoryId { get; set; }
        public bool Selected { get; set; }
        public string CategoryName { get; set; }
        public int? ParentId { get; set; }
    }

    public class UpdtCourseVend
    {
        public int CourseId { get; set; }
        public CourseVendor[] Vendors { get; set; }
    }

    public class CourseVendor
    {
        public int VendorId { get; set; }
        public bool Selected { get; set; }
        public string VendorName { get; set; }

        public MpVendor[] GetVendorList(int courseId)
        {
            using (ZipMartEntities ze = new ZipMartEntities())
            {
                int[] vendorCourses = ze.MpCourseVendor.Where(x => x.CourseId == courseId).Select(y => y.VendorId).ToArray();

                return ze.MpVendor.Where(x => vendorCourses.Contains(x.VendorId)).ToArray();
            }
        }
    }

    public class SequencedCourse
    {
        public int CourseId { get; set; }
        public int SeqId { get; set; }
    }

    public class VendorSequencedCourses
    {
        public int VendorId { get; set; }
        public SequencedCourse[] Courses { get; set; }
    }

    public class CourseImageMap
    {
        public int ImageId { get; set; }
        public int[] CourseIds { get; set; }
    }

    public class CoursePricing
    {
        public long CourseInstanceId { get; set; }
        public long CoursePricingId { get; set; }
        public long CourseId { get; set; }
        public int StartQuantity { get; set; }
        public int EndQuantity { get; set; }
        public decimal Price { get; set; }
    }

}
