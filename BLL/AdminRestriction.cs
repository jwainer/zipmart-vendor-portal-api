﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.Mvc;

namespace BLL
{
    public class AdminRestriction : ActionFilterAttribute
    {
        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            // If the request has arrived via HTTP...
            if (filterContext.HttpContext.Session["AdminPrivilege"] == "franchise")
            {
                filterContext.Result = new RedirectResult("http://www.google.com"); 
                filterContext.Result.ExecuteResult(filterContext);
            }
            base.OnActionExecuting(filterContext);
        }
    }
}
