﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BLL
{
    public class SpecialPaging
    {
        public int FirstPage { get; set; }
        public int FromPage { get; set; }
        public int CurrentPage { get; set; }
        public int ToPage { get; set; }
        public int MaxPage { get; set; }
        public bool ShowDotsOnLeft { get; set; }
        public bool ShowDotsOnRight { get; set; }

        public SpecialPaging()
        {
            this.FirstPage = 1;
        }

        public SpecialPaging(int currentPage, int maxPage)
            : this()
        {
            this.SetCurrentPage(currentPage, maxPage);
        }

        public void SetCurrentPage(int currentPage, int maxPage)
        {
            this.FromPage = this.FirstPage;
            this.ToPage = maxPage;
            this.CurrentPage = currentPage;
            this.MaxPage = maxPage;

            if (this.CurrentPage > this.MaxPage - 4)
                this.FromPage = this.MaxPage - 4;
            else if (this.CurrentPage - 4 > 0)
                this.FromPage = this.CurrentPage - 2;

            if (this.CurrentPage < 5 & this.MaxPage > 5)
                this.ToPage = 5;
            else if (currentPage + 3 < maxPage)
                this.ToPage = currentPage + 2;

            this.ShowDotsOnLeft = (this.FromPage > this.FirstPage);
            this.ShowDotsOnRight = (this.ToPage < this.MaxPage);
        }
    }
}
