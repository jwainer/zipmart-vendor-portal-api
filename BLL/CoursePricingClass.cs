﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;
using DAL;

namespace BLL
{
    public static class CoursePricingClass
    {
        public static decimal GetOriginalCoursePrice(int vendorId, int courseId, long? courseInstanceId, int quantity, int vendorTypeId)
        {
            if (vendorTypeId == 5)
                return 0;

            using (ZipMartEntities ze = new ZipMartEntities())
            {
                string query = string.Format("select price = dbo.GetCoursePricing3({0}, {1}, {2}, {3})", vendorId, courseId, (courseInstanceId == null ? "NULL" : courseInstanceId.ToString()), quantity);
                decimal? price = ze.Database.SqlQuery<decimal?>(query).FirstOrDefault();
                price = price ?? 0;
                return (decimal)price;
            }
        }


        public static decimal GetSubscribedCoursePrice(int vendorId, int courseId, long? courseInstanceId, long userId, int quantity, int publisherId, int vendorTypeId )
        {
            if (vendorTypeId == 5)
                return 0;

            using (ZipMartEntities ze = new ZipMartEntities())
            {
                double discount = 0;
                long subscriberId = 0;
                double userDiscount = Discount(ze, vendorId, userId, out subscriberId);

                MpPublisher publisher = ze.MpPublisher.SingleOrDefault(x => x.PublisherId == publisherId);
                if (publisher != null && publisher.AcceptSubscription == true && subscriberId > 0)
                    discount = 1.0;
                else if (subscriberId > 0)
                    discount = userDiscount;
                else
                    discount = 0.0;

                string query = string.Format("select price = dbo.GetCoursePricing3({0}, {1}, {2}, {3})", vendorId, courseId, (courseInstanceId == null ? "NULL" : courseInstanceId.ToString()), quantity);
                decimal? price = ze.Database.SqlQuery<decimal?>(query).FirstOrDefault();
                price = price ?? 0;

                if (price > 0 && discount > 0)
                    price = (decimal)((double)price - discount * (double)price);

                return (decimal)Math.Round((double)price,2);
            }
        }


        public static double Discount(ZipMartEntities ze, int vendorId, long userId, out long subscriberId)
        {
            subscriberId = 0;
            double discount = 0;
            if (userId > 0)
            {
                MpSubscriber subscriber = ze.MpSubscriber.SingleOrDefault(x => x.UserId == userId && (x.VendorId == vendorId && x.SubscriptionId == null || x.SubscriptionId != null && x.StripeToken != null));
                if (subscriber != null && subscriber.SubscriptionId == null)
                    discount = 1.0;                                 // Franchise Subscription Model
                else if (subscriber != null && subscriber.SubscriptionId != null)
                {
                    discount = subscriber.MpSubscription.Discount;  // Public Subscription Model matching Vendor
                }
                else
                    discount = 0;

                if (subscriber != null)
                    subscriberId = subscriber.SubscriberId;

            }

            return discount;
        }

    }
}
