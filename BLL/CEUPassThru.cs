﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BLL
{
    public class CEUPassThru
    {
        public string[] Username { get; set; }
        public string[] FirstName { get; set; }
        public string[] LastName { get; set; }
        public string[] RCD { get; set; }
        public int[] CourseIds { get; set; }
        public decimal[] Prices { get; set; }
    }
}
