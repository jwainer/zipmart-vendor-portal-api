﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DAL;

namespace BLL
{
    public class AdRotatorClass
    {
        public string AdRotatorName { get; set; }
        public long AdRecId { get; set; }
        public bool Selected { get; set; }
    }
}
