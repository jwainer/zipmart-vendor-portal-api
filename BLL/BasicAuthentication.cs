﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using System.Web.Mvc.Filters;
using System.Web.Routing;
using BLL.ZetUserService;

namespace BLL
{
    public class BasicAuthentication : ActionFilterAttribute, IAuthenticationFilter
    {
        #region ' Implementation Methods '

        public void OnAuthentication(AuthenticationContext context)
        {
            var user = context.HttpContext.User;
            if (user == null || !user.Identity.IsAuthenticated)
            {
                context.Result = new HttpUnauthorizedResult();
            }
        }

        public void OnAuthenticationChallenge(AuthenticationChallengeContext context)
        {
            var user = context.HttpContext.User;
            //if (user == null || !user.Identity.IsAuthenticated)
            //{
            //    context.Result = new HttpUnauthorizedResult();
            //}
        }

        #endregion

        #region ' Methods '

        /// <summary>
        /// Authenticates the user against the current user system.
        /// </summary>
        /// <param name="username">The username of the user.</param>
        /// <param name="password">The password of the user.</param>
        public static ZetUser Authenticate(string username, string password, int applicationId = 1)
        {
            if (string.IsNullOrEmpty(username))
                return null;

            if (string.IsNullOrEmpty(password))
                return null;

            //Authenticate against user system.
            using (ZetUserService.UserClient zClient = new ZetUserService.UserClient())
            {
                string token = zClient.GetToken();
                ZetUserService.ZetUser zUser = zClient.Authenticate(username, password, applicationId, token);
                if (zUser != null)
                {
                    BasicAuthentication.SetAuthenticationTicket(zUser, 1, applicationId);
                    return zUser;
                }
            }
            return null;
        }

        /// <summary>
        /// Authenticates an auth ticket against the current user system.
        /// </summary>
        /// <param name="ticketId">The auth ticket ID.</param>
        public static ZetUser Authenticate(string ticketId)
        {
            if (string.IsNullOrEmpty(ticketId))
                return null;

            using (UserClient uClient = new UserClient())
            {
                ZetUser zUser = uClient.AuthenticateTicket(ticketId, uClient.GetToken());
                if (zUser != null)
                {
                    BasicAuthentication.SetAuthenticationTicket(zUser, 2, 1);
                    return zUser;
                }
            }
            return null;
        }

        /// <summary>
        /// Signs out the current logged user.
        /// </summary>
        public static void SignOut()
        {
            System.Web.Security.FormsAuthentication.SignOut();
        }

        private static void SetAuthenticationTicket(ZetUser zUser, int version, int appId)
        {
            System.Web.Security.FormsAuthentication.Initialize();
            //The AddMinutes determines how long the user will be logged in after leaving
            //the site if he doesn't log off.
            StringBuilder sb = new StringBuilder();
            sb.AppendFormat("fn={0};ln={1};uid={2};rpc={3};un={4};appid={5}", zUser.FirstName, zUser.LastName, zUser.UserId.ToString(), zUser.RPC.ToString(), zUser.Username, appId);
            DateTime currDate = DateTime.Now;
            System.Web.Security.FormsAuthenticationTicket fat = new System.Web.Security.FormsAuthenticationTicket(version, zUser.Username, currDate, currDate.AddMinutes(30), false, sb.ToString());
            System.Web.HttpContext.Current.Response.Cookies.Add(new System.Web.HttpCookie(
                System.Web.Security.FormsAuthentication.FormsCookieName, System.Web.Security.FormsAuthentication.Encrypt(fat)));
        }

        #endregion
    }
}
