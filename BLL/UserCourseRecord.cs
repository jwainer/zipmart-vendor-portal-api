﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BLL
{
    public class UserCourseRecord
    {
        public int CourseId { get; set; }
        public string Name { get; set; }
        public DateTime? StartDate { get; set; }
        public string StartDateDisp { get; set; }
        public DateTime? StatusChangeDate { get; set; }     // Status change date to track Redeem, in progress and complete 
        public string StatusChangeDateDisp { get; set; }    // Status change date display 
        public long CourseInsId { get; set; }
        public long CourseSeatId { get; set; }
        public int VendorId { get; set; }
        public string VendorName { get; set; }
        public byte Format { get; set; }
        public long? ZlearnId { get; set; }
        public CourseStatus Status { get; set; }
        public string TransDate { get; set; }
        public string Email { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public long CertificateId { get; set; }
        public string AssignOnly { get; set; }
        public Guid TransferId { get; set; }
        public string[] VendorRestrictedIps { get; set; }
        public long TransfereeUserId { get; set; }
        public string PublisherName { get; set; }
        public double? UserCourseRating { get; set; }
        public string UserComments { get; set; }
        public DateTime? EnrollDate { get; set; }
        public DateTime? Completed { get; set; }
        public bool? Transferrable { get; set; }
        public bool? ReachedmaxAttemp { get; set; }
        public string CourseFormat { get; set; }
        public string AllowedIp { get; set; }
    }
}
