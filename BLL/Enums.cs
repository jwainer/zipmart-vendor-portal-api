﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BLL
{
    [Flags()]
    public enum ReturnType
    {
        NoChanges = 0,
        Success = 1,
        DeleteCookie = 2
    }
    
    public enum Section
    {
        None = 0,
        Password = 1,
        Contact = 2,
        Billing = 3
    }

    public enum AdRotatorType
    {
        Category = 0,
        Vendor = 1
    }

    /// <summary>
    /// User data in the Auth cooki.
    /// </summary>
    public enum UserDataKeys
    {
        /// <summary>
        /// First name.
        /// </summary>
        FN = 1,
        /// <summary>
        /// Last name.
        /// </summary>
        LN = 2,
        /// <summary>
        /// User ID.
        /// </summary>
        UID = 3,
        /// <summary>
        /// RPC
        /// </summary>
        RPC = 4,
        /// <summary>
        /// User name
        /// </summary>
        UN = 5,
        /// <summary>
        /// Appplication Id
        /// </summary>
        APPID = 6

    }

    public enum LoginType
    {
        /// <summary>
        /// Normal Login, entering username and password
        /// </summary>
        [Description("Login")]
        NormalLogin = 0,
        /// <summary>
        /// Create New Account, entering username only
        /// </summary>
        [Description("Create New Account")]
        CreateNewAccount = 1,
        /// <summary>
        /// Forgot Password, entering username and captcha
        /// </summary>
        [Description("Forgot Password")]
        ForgotPassword = 2
    }

    public enum CourseStatus
    {
        /// <summary>
        /// Specifies that course is available for taking or assigning.
        /// </summary>
        Open = 0,
        /// <summary>
        /// Specifies that course is pending for redemption
        /// </summary>
        [Description("Pending Redemption")]
        PendingRedemption = 1,
        /// <summary>
        /// Specifies classroom course has been registrated
        /// </summary>
        Registered = 2,
        /// <summary>
        /// Specifies if the course is in progress
        /// </summary>
        [Description("In Progress")]
        InProgress = 3,
        /// <summary>
        /// Specifies the course completion
        /// </summary>
        Completed = 4,
        /// <summary>
        /// Specifies if the course has been expired
        /// </summary>
        Expired = 5,
        /// <summary>
        /// Specifies if the course has been Redeemed
        /// </summary>
        [Description("Non-Transferrable")]
        Redeemed = 6,
        /// <summary>
        /// specifies if student Reached Maximum Attempts
         [Description("Reached Maximum Attempts")]
        /// </summary>
        ReachedMaximum = 7
    }

    public enum ErrorCodes
    {
        /// <summary>
        /// Failed to send verification email.
        /// </summary>
        [Description("Failed to send verification email.")]
        Error_1 = 1,
        /// <summary>
        /// Failed to create verification code.
        /// </summary>
        [Description("Failed to create verification code.")]
        Error_2 = 2,
        /// <summary>
        /// Failed to create zLearn user.
        /// </summary>
        [Description("Failed to create zLearn user.")]
        Error_3 = 3,
        /// <summary>
        /// Could not find specified zLearn course.
        /// </summary>
        [Description("Could not find specified zLearn course.")]
        Error_4 = 4,
        /// <summary>
        /// This course is not mapped with Zip Elearning System.
        /// </summary>
        [Description("This course is not mapped with Zip Elearning System.")]
        Error_5 = 5,
        /// <summary>
        /// Failed to send reset-password email. Please contact our support department
        /// </summary>
        [Description("Failed to send reset-password email. Please contact our support department")]
        Error_6 = 6,
        /// <summary>
        /// Failed to send email to redeem the course. Please contact our support department.
        /// </summary>
        [Description("Failed to send email to redeem the course. Please contact our support department")]
        Error_7 = 7,
        /// <summary>
        /// Failed to registration send email. Please contact our support department.
        /// </summary>
        [Description("Failed to send registration email. Please contact our support department.")]
        Error_8 = 8,
        /// <summary>
        /// User assigned a course but system failed to send email to Redeem the course.
        /// </summary>
        [Description("User assigned a course but system failed to send email to Redeem the course.")]
        Error_9 = 9,
         /// <summary>
        /// User was trying to register for a classroom course but after registration system failed to send registration email.
        /// </summary>
        [Description("User was trying to register for a classroom course but after registration system failed to send registration email.")]
        Error_10 = 10,
         /// <summary>
        /// Error in removing item.
        /// </summary>
        [Description("Error in removing item from wishlist.")]
        Error_11 = 11,
        /// <summary>
        /// Expired / Invalid Promo Code.
        /// </summary>
        [Description("Expired / Invalid Promo Code.")]
        Error_12 = 12,
        /// <summary>
        /// Failed to transfer the seat.
        /// </summary>
        [Description("Failed to transfer the seat.")]
        Error_13 = 13,
        /// <summary>
        /// Error Adding course to wishlist.
        /// </summary>
        [Description("Error Adding course to wishlist.")]
        Error_14 = 14,
        /// <summary>
        /// Course reclaimed but system failed to send email to notify the assigned person.
        /// </summary>
        [Description("Course reclaimed but system failed to send email to notify the assigned person.")]
        Error_15 = 15,
        /// <summary>
        /// Failed to generate report
        /// </summary>
        [Description("Failed to generate report")]
        Error_16 = 16
    }

    public enum MessageCodes
    {
        /// <summary>
        /// No message (default).
        /// </summary>
        None = 1000,
        /// <summary>
        /// Sorry you can't redeem this course because you are already Registered/Taking/Completed the same course.
        /// </summary>
        [Description("Sorry you can't redeem this course because you are already Registered/Taking/Completed the same course.")]
        Message_1001 = 1001,
        /// <summary>
        /// Your account has been created. Please check your email and click on the "Activate account" link that we are sending you to log in and proceed with you transaction.
        /// </summary>
        [Description("Your account has been created. Please check your email and click on the \"Activate account\" button in the email.")]
        Message_1002 = 1002,
        /// <summary>
        /// Currently you do not have any course in your list.
        /// </summary>
        [Description("Currently you do not have any course in your list.")]
        Message_1003 = 1003,
        /// <summary>
        /// Please enter valid email address.
        /// </summary>
        [Description("Please enter valid email address.")]
        Message_1004 = 1004,
        /// <summary>
        /// You already assigned this course to.
        /// </summary>
        [Description("You already assigned this course to same person ")]
        Message_1005 = 1005,
        /// <summary>
        ///You already owned this course.Please enter other email address to assign it.
        /// </summary>
        [Description("You already owned this course.Please enter other email address to assign it.")]
        Message_1006 = 1006,
         /// <summary>
        /// Please enter numaric extension".
        /// </summary>
        [Description("Please enter numaric extension.")]
        Message_1007 = 1007,
        /// <summary>
        /// Please enter correct old password.
        /// </summary>
        [Description("Please enter correct old password.")]
        Message_1008 = 1008,
        /// <summary>
        /// "New Password should not be same as old password.
        /// </summary>
        [Description("New Password should not be same as old password.")]
        Message_1009 = 1009,
         /// <summary>
        /// Please enter valid zip code.
        /// </summary>
        [Description("Please enter valid zip code.")]
        Message_1010 = 1010,
        /// <summary>
        /// Information updated successfully.
        /// </summary>
        [Description("Information updated successfully.")]
        Message_1011 = 1011,
        /// <summary>
        /// Currently you do not have any transactions.
        /// </summary>
        [Description("Currently you do not have any transactions.")]
        Message_1012 = 1012,
        /// <summary>
        /// Currently you do not have any course in your wishlist.
        /// </summary>
        [Description("Currently you do not have any course in your wishlist.")]
        Message_1013 = 1013,
        /// <summary>
        ///  Email has been sent to Redeem course.
        /// </summary>
        [Description(" Email has been sent to Redeem course.")]
        Message_1014 = 1014,
        /// <summary>
        ///  Sorry you can't redeem this course because course has already started.
        /// </summary>
        [Description("Sorry you can't redeem this course because course has already started.")]
        Message_1015 = 1015,
        /// <summary>
        /// Sorry you can't redeem this course because it is assigned to someone else.
        /// </summary>
        [Description("Sorry you can't redeem this course because it is assigned to someone else.")]
        Message_1016 = 1016,
        /// <summary>
        /// Your account has been created but has not been verified. Please check your email and click on the Activation button to verify your email address. Please contact us if you still have problems logging in.
        /// </summary>
        [Description("Your account has been created but has not been verified. Please check your email and click on the Activation button to verify your email address. Please contact us if you still have problems logging in.")]
        Message_1017 = 1017,
        /// <summary>
        /// Thank you! Your email address has been verified. Please log in now.
        /// </summary>
        [Description("Thank you! Your email address has been verified. Please log in now.")]
        Message_1018 = 1018,
        /// <summary>
        /// Your password has been changed successfully. Please log in with your new password.
        /// </summary>
        [Description("Your password has been changed successfully. Please log in with your new password.")]
        Message_1019 = 1019,
        /// <summary>
        /// Password Session has expired. Please reset your password again.
        /// </summary>
        [Description("Password Session has expired. Please reset your password again.")]
        Message_1020 = 1020,
        /// <summary>
        /// Activation period has expired or account has been activated
        /// </summary>
        [Description("Activation period has expired or account has been activated.")]
        Message_1021 = 1021,
        /// <summary>
        /// Missing / Incorrect Parameters.
        /// </summary>
        [Description("Missing / Incorrect Parameters.")]
        Message_1022 = 1022,
        /// <summary>
        /// Credit card declined.
        /// </summary>
        [Description("Credit card declined.")]
        Message_1023 = 1023,
        /// <summary>
        /// Thank you. Your order has been completed and a confirmation has been emailed to you.
        /// </summary>
        [Description("Thank you. Your order has been completed and a confirmation has been emailed to you.")]
        Message_1024 = 1024,
        /// <summary>
        /// Your order has been received, however the system failed to send receipt email. Please contact our Customer Service to ensure proper handling.
        /// </summary>
        [Description("Your order has been received, however the system failed to send receipt email. Please contact our Customer Service to ensure proper handling.")]
        Message_1025 = 1025,
        /// <summary>
        /// Unable to process your order. Please try again or contact our Customer Service.
        /// </summary>
        [Description("Unable to process your order. Please try again or contact our Customer Service.")]
        Message_1026 = 1026,
          /// <summary>
        ///This course is already redeemed.
        /// </summary>
        [Description("This course is already redeemed.")]
        Message_1027 = 1027,
           /// <summary>
        ///Invalid Redeem Code.
        /// </summary>
        [Description("Invalid Redeem Code.")]
        Message_1028 = 1028,
        /// <summary>
        ///Please check your mail for registration detail.
        /// </summary>
        [Description("Please check your mail for registration detail.")]
        Message_1029 = 1029,
        /// <summary>
        ///Phone Number already exists.
        /// </summary>
        [Description("Duplicate phone numbers.")]
        Message_1030 = 1030,
         /// <summary>
        ///Email Address already exists.
        /// </summary>
        [Description("Email Address already exists.")]
        Message_1031 = 1031,
         /// <summary>
        ///Item removed successfully.
        /// </summary>
        [Description("Item removed successfully.")]
        Message_1032 = 1032,
        /// <summary>
        /// Failed to send reset-password email. Please contact our support department.
        /// </summary>
        [Description("Failed to send reset-password email. Please contact our support department.")]
        Message_1033 = 1033,
        /// <summary>
        /// Unable to find user account associated with this email.
        /// </summary>
        [Description("Unable to find user account associated with this email.")]
        Message_1034 = 1034,
         /// <summary>
        /// Different phone numbers can't have same phone type.
        /// </summary>
        [Description("Different phone numbers can't have same phone type.")]
        Message_1035 = 1035,
        /// <summary>
        /// Sorry you can't redeem this course because you assigned this.
        /// </summary>
        [Description("Sorry you can't redeem this course because you assigned this.")]
        Message_1036 = 1036,
        /// <summary>
        /// No records found for the filter that you specified.
        /// </summary>
        [Description("No records match your filter criteria")]
        Message_1037 = 1037,
        /// <summary>
        /// Password must be at least 8 characters long containing all alpha numeric, literals and !@#$%^&* characters only.
        /// </summary>
        [Description("Password must be at least 8 characters long containing all alpha numeric, literals and !@#$%^&* characters only.")]
        Message_1038 = 1038,
        /// <summary>
        /// Phone number is required.
        /// </summary>
        [Description("Phone number is required.")]
        Message_1039 =1039,
          /// <summary>
        /// Can't take the course because it doesn't belongs to you.
        /// </summary>
        [Description("Can't take the course because it doesn't belongs to you.")]
        Message_1040 =1040,
        /// <summary>
        /// Credit card has expired.
        /// </summary>
        [Description("Credit card has expired.")]
        Message_1041 = 1041,
        /// <summary>
        /// Course has been added to wish list.
        /// </summary>
        [Description("Course has been added to wish list.")]
        Message_1042 = 1042,
        /// <summary>
        /// Course is already in the wish list.
        /// </summary>
        [Description("Course is already in the wish list.")]
        Message_1043 = 1043,
        /// <summary>
        /// No matching account was found for this identifier entered. Please ensure that you you have typed your Email and Password correctly.
        /// </summary>
        [Description("No matching account was found for this identifier entered. Please ensure that you you have typed your Email and Password correctly.")]
        Message_1044 = 1044,
        /// <summary>
        /// Invalid Credit Card Number.
        /// </summary>
        [Description("Invalid Credit Card Number.")]
        Message_1045 = 1045,
      /// <summary>
        /// Password does not meet policy.
        /// </summary>
        [Description("Password does not meet policy.")]
        Message_1046 = 1046,
        /// <summary>
        /// Invalid Association Session Id.
        /// </summary>
        [Description("Invalid Association Session Id")]
        Message_1047 = 1047,
        /// <summary>
        /// Missing Association Session Id.
        /// </summary>
        [Description("Missing Association Session Id")]
        Message_1048 = 1048,
        /// <summary>
        ///  Email has been sent to Redeem course.
        /// </summary>
        [Description(" Course reclaimed and email has been sent to notify the assigned person.")]
        Message_1049 = 1049,
        /// <summary>
        /// Can't assign the course because it doesn't belongs to you.
        /// </summary>
        [Description("Can't assign the course because it doesn't belongs to you.")]
        Message_1050 = 1050,
        /// <summary>
        /// You can't reclaim the course because it's redeemed.
        /// </summary>
        [Description("You can't reclaim the course because it's redeemed.")]
        Message_1051 = 1051,
        /// <summary>
        /// Your password will be reset.
        /// </summary>
        [Description("Your password will be reset.")]
        Message_1052 = 1052,
        /// <summary>
        /// No records to show for this account.
        /// </summary>
        [Description("No records to show for this account.")]
        Message_1053 = 1053,
        /// <summary>
        /// Please enter Answer.
        /// </summary>
        [Description("Please enter Answer.")]
        Message_1054 = 1054,
        /// <summary>
        /// Unable to show some of the courses because you are outside the designated area.
        /// </summary>
        [Description("Unable to show some of the courses because you are outside the designated area.")]
        Message_1055 = 1055,
        /// <summary>
        /// Please verify that you are over 13 and agree with our EULA by checking at the bottom of this agreement.
        /// </summary>
        [Description("Please verify that you are over 13 and agree with our EULA by checking at the bottom of this agreement.")]
        Message_1056 = 1056,
        /// <summary>
        /// Please verify that you are over 13 years old by checking at the bottom of this agreement.
        /// </summary>
        [Description("Please verify that you are over 13 years old by checking at the bottom of this agreement.")]
        Message_1057 = 1057,
        /// <summary>
        /// Please verify that you agree with this EULA by checking at the bottom of this agreement.
        /// </summary>
        [Description("Please verify that you agree with this EULA by checking at the bottom of this agreement.")]
        Message_1058 = 1058,
        /// <summary>
        /// Please enter your phone number with the area code.
        /// </summary>
        [Description("Please enter your phone number with the area code.")]
        Message_1059 = 1059,
        /// <summary>
        /// Unexpected error occurred. Please contact us ASAP.
        /// </summary>
        [Description("Unexpected error occurred. Please contact us ASAP.")]
        Message_1060 = 1060


    }
}
