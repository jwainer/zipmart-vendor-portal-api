﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Amazon.SimpleEmail;
using Amazon.SimpleEmail.Model;
using System.Net.Mail;
using System.Net.Mime;
using System.IO;

namespace BLL
{
    public static class AGMail
    {
        #region ' Members '

        /// <summary>
        /// ZipEdTech Amazon Access Key ID.
        /// </summary>
        private const string ACCESS_KEY_ID = "AKIAIIP4A3NQ7H3PAGZQ";
        /// <summary>
        /// ZipEdTech Amazon Secret Access Key.
        /// </summary>
        private const string SECRET_ACCESS_KEY = "9B1vSNfnX7ZHs2vCB0fvLuqY8iTH0cWIHa9aqgaF";

        #endregion

        #region ' Methods '

        /// <summary>
        /// Sends an email using Amazon's web service.
        /// </summary>
        /// <param name="message">The email message object to send.</param>
        public static void SendEmail(MailMessage message)
        {
            RawMessage rawMsg = AGMail.GetRawMessage(message);
            if (rawMsg != null)
            {
                AmazonSimpleEmailServiceClient client = new AmazonSimpleEmailServiceClient(ACCESS_KEY_ID, SECRET_ACCESS_KEY, Amazon.RegionEndpoint.USWest2);
                SendRawEmailRequest request = new SendRawEmailRequest();
                request.RawMessage = rawMsg;
                SendRawEmailResponse rawEmailResponse = client.SendRawEmail(request);
            }
        }

        /// <summary>
        /// Gets the formated address line including the display name and email address.
        /// </summary>
        /// <param name="address">The email address.</param>
        /// <param name="name">The display name.</param>
        /// <returns></returns>
        private static string GetAddressLine(MailAddress address, string name)
        {
            string addressLine = string.Empty;
            if (!string.IsNullOrEmpty(address.DisplayName))
                addressLine = name + ":  \"" + address.DisplayName + "\"" + " <" + address.Address + ">";
            else
                addressLine = name + ":  " + address.Address;

            return addressLine;
        }

        /// <summary>
        /// Gets the transfer encoding of the content.
        /// </summary>
        /// <param name="encoding">The enum TransferEncoding object.</param>
        private static string GetTransferEncoding(TransferEncoding encoding)
        {
            if (encoding == TransferEncoding.Base64)
                return "base64";
            else if (encoding == TransferEncoding.QuotedPrintable)
                return "quoted-printable";
            else if (encoding == TransferEncoding.SevenBit)
                return "7bit";
            else
                return "quoted-printable";
        }

        /// <summary>
        /// Gets the raw message.
        /// </summary>
        /// <param name="message">The message object from which to compose the raw message.</param>
        private static RawMessage GetRawMessage(MailMessage message)
        {
            RawMessage rawMst = null;
            using (MailMessage msg = message)
            {
                using (MemoryStream ms = new MemoryStream())
                {
                    rawMst = new RawMessage();
                    rawMst.Data = ms;
                    StringBuilder sb = new StringBuilder();
                    // Write From address.
                    sb.AppendLine(AGMail.GetAddressLine(msg.From, "From"));
                    // Write return path.
                    sb.AppendLine(AGMail.GetAddressLine(msg.From, "Return-Path"));
                    // Write To address.
                    foreach (MailAddress address in msg.To)
                    {
                        sb.AppendLine(AGMail.GetAddressLine(address, "To"));
                    }
                    // Write Reply-To address.
                    if (msg.ReplyToList.Count > 0)
                        sb.AppendLine(AGMail.GetAddressLine(msg.ReplyToList[0], "Reply-To"));
                    else
                        sb.AppendLine(AGMail.GetAddressLine(msg.From, "Reply-To"));

                    // Write CC addresses
                    foreach (MailAddress address in msg.CC)
                    {
                        sb.AppendLine(AGMail.GetAddressLine(address, "CC"));
                    }
                    // Write BCC addresses
                    foreach (MailAddress address in msg.Bcc)
                    {
                        sb.AppendLine(AGMail.GetAddressLine(address, "BCC"));
                    }
                    // Write the Subject.
                    sb.AppendLine("Subject:  " + msg.Subject);
                    sb.AppendLine("MIME-Version: 1.0");
                    if (msg.Priority == MailPriority.Low)
                    {
                        sb.AppendLine("X-Priority: 5 (Lowest)");
                        sb.AppendLine("X-MSMail-Priority: Low");
                    }
                    else if (msg.Priority == MailPriority.High)
                    {
                        sb.AppendLine("X-Priority: 1 (Highest)");
                        sb.AppendLine("X-MSMail-Priority: High");
                    }
                    sb.AppendLine("Content-Type: multipart/mixed; boundary=\"==mixed-content==\"");
                    sb.AppendLine();
                    // Write the attachments.
                    foreach (System.Net.Mail.Attachment a in msg.Attachments)
                    {
                        using (System.Net.Mail.Attachment attach = a)
                        {
                            using (Stream s = attach.ContentStream)
                            {
                                string contentType = attach.ContentType.MediaType;
                                byte[] fileBytes = new byte[attach.ContentStream.Length];
                                sb.AppendLine("--==mixed-content==");
                                sb.AppendLine("Content-Type: " + contentType + "; name=\"" + attach.ContentType.Name + "\"");
                                sb.AppendLine("Content-Description: " + attach.ContentType.Name);
                                sb.AppendLine("Content-Disposition: " + attach.ContentDisposition.DispositionType + "; filename=\""
                                    + attach.ContentDisposition.FileName + "\"; size=" + fileBytes.Length.ToString());

                                sb.AppendLine("Content-Transfer-Encoding: " + AGMail.GetTransferEncoding(a.TransferEncoding));
                                sb.AppendLine();
                                attach.ContentStream.Position = 0;
                                attach.ContentStream.Read(fileBytes, 0, fileBytes.Length);
                                sb.AppendLine(Convert.ToBase64String(fileBytes));
                                sb.AppendLine();
                            }
                        }
                    }

                    // Write the body.
                    if (!string.IsNullOrEmpty(msg.Body))
                    {
                        // Write a new boundary if needed.
                        sb.AppendLine("--==mixed-content==");
                        sb.AppendLine("Content-Type: multipart/alternative; boundary=\"==alternative-content==\"");
                        sb.AppendLine();

                        sb.AppendLine("--==alternative-content==");
                        if (msg.IsBodyHtml)
                            sb.AppendLine("Content-Type: text/html; charset=" + msg.BodyEncoding.HeaderName);
                        else
                            sb.AppendLine("Content-Type: text/plain; charset=" + msg.BodyEncoding.HeaderName);

                        sb.AppendLine("Content-Transfer-Encoding: 7bit");
                        sb.AppendLine();
                        sb.AppendLine(msg.Body);
                        sb.AppendLine();
                    }
                    else
                    {
                        // Write the alternate views.
                        if (msg.AlternateViews != null && msg.AlternateViews.Count > 0)
                        {
                            // Write a new boundary if needed.
                            sb.AppendLine("--==mixed-content==");
                            sb.AppendLine("Content-Type: multipart/alternative; boundary=\"==alternative-content==\"");
                            sb.AppendLine();
                            foreach (AlternateView aView in msg.AlternateViews)
                            {
                                using (AlternateView av = aView)
                                {
                                    string contentType = av.ContentType.MediaType;
                                    string content = string.Empty;
                                    using (Stream s = av.ContentStream)
                                    {
                                        using (StreamReader sr = new StreamReader(s))
                                        {
                                            content = sr.ReadToEnd();
                                        }
                                        sb.AppendLine("--==alternative-content==");
                                        sb.AppendLine("Content-Type: " + contentType);
                                        sb.AppendLine("Content-Transfer-Encoding: " + AGMail.GetTransferEncoding(av.TransferEncoding));
                                        sb.AppendLine();
                                        sb.AppendLine(content);
                                        sb.AppendLine();
                                    }
                                }
                            }
                        }
                    }
                    byte[] bytes = Encoding.ASCII.GetBytes(sb.ToString());
                    ms.Write(bytes, 0, bytes.Length);
                }
            }
            return rawMst;
        }

        /// <summary>
        /// Verifies an email address with amazon.
        /// </summary>
        /// <param name="emailAddress">The email address to verify.</param>
        public static Boolean VerifyEmailAddress(String emailAddress)
        {
            List<String> verifiedEmailAddresses = AGMail.GetVerifiedEmailAddresses();
            if (!verifiedEmailAddresses.Contains(emailAddress))
            {
                AmazonSimpleEmailServiceClient client = new AmazonSimpleEmailServiceClient(ACCESS_KEY_ID, SECRET_ACCESS_KEY, Amazon.RegionEndpoint.USWest2);
                VerifyEmailAddressRequest vEmailAddress = new VerifyEmailAddressRequest();
                vEmailAddress.EmailAddress = emailAddress;
                try
                {
                    VerifyEmailAddressResponse response = client.VerifyEmailAddress(vEmailAddress);
                    object obj = response.HttpStatusCode;
                    return true;
                }
                catch
                {
                    return false;
                }
            }
            return true;
        }

        /// <summary>
        /// Gets a list of all verified (by amazon) email addresses.
        /// </summary>
        private static List<String> GetVerifiedEmailAddresses()
        {
            AmazonSimpleEmailServiceClient client = new AmazonSimpleEmailServiceClient(ACCESS_KEY_ID, SECRET_ACCESS_KEY, Amazon.RegionEndpoint.USWest2);
            try
            {
                return client.ListVerifiedEmailAddresses().VerifiedEmailAddresses;
            }
            catch
            {
                return new List<String>();
            }
        }

        #endregion
    }
}
