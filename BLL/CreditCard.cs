﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;

namespace BLL
{
    public class CreditCard
    {
        #region ' Property '

        [MaxLength(25, ErrorMessage = "Max length is 25 characters")]
        [Required(ErrorMessage = "Cardholder First Name is required")]
        public string CardFirstName { get; set; }
        [MaxLength(25, ErrorMessage = "Max length is 25 characters")]
        [Required(ErrorMessage = "Cardholder Last Name is required")]
        public string CardLastName { get; set; }
        [MinLength(15, ErrorMessage = "Please enter correct credit card number")]
        [MaxLength(20, ErrorMessage = "Max length is 20 characters")]
        [Required(ErrorMessage = "Credit Card number is required")]
        public string CardNumber { get; set; }
        [Required(ErrorMessage = "Expiration Month is required")]
        public int ExpirationMonth { get; set; }
        [Required(ErrorMessage = "Expiration Year is required")]
        [Range(2014, 3000, ErrorMessage = "Invalid Year")]
        public short ExpirationYear { get; set; }
        [StringLength(4, MinimumLength = 3, ErrorMessage = "Please enter 3-digit number in the back of your credit card (4-digit for Amex)")]
        [Required(ErrorMessage = "Credit Card CCV (In the back of the card) is required")]
        public string CCV { get; set; }
        public string CardType { get; set; }
        public string TransactionId { get; set; }
        public long UserId { get; set; }
        public int VendorId { get; set; }
        [MaxLength(50, ErrorMessage = "Max length is 50 characters")]
        [Required(ErrorMessage = "Address Line 1 is required")]
        public string Address1 { get; set; }
        public string Address2 { get; set; }
        [MaxLength(50, ErrorMessage = "Max length is 50 characters")]
        [Required(ErrorMessage = "City is required")]
        public string City { get; set; }
        [MaxLength(20, ErrorMessage = "Max length is 20 characters")]
        [Required(ErrorMessage = "State is required")]
        public string State { get; set; }
        [MaxLength(10, ErrorMessage = "Max length is 10 characters")]
        [Required(ErrorMessage = "ZipCode is required")]
        public string Zip { get; set; }
        [Required(ErrorMessage = "Country is required")]
        public string CountryId { get; set; }
        public string Email { get; set; }

        #endregion

        #region ' methods '

        public CreditCard(long userId)
        {
            this.State = "CA";
            this.CountryId = "US";
            this.UserId = userId;
        }

        /// <summary>
        /// Checks the Month and the Year and evaluate whether or not the credit card has expired
        /// </summary>
        /// <returns></returns>
        public bool HasExpired()
        {

            DateTime currDate = DateTime.Now;
            if (this.ExpirationYear > currDate.Year)
                return false;

            if (this.ExpirationYear == currDate.Year && this.ExpirationMonth >= currDate.Month)
                return false;

            return true;
        }
        /// <summary>
        /// Validate credit card number with LUHN algorighm
        /// </summary>
        /// <param name="creditCardNumber"></param>
        /// <returns></returns>
        public bool ValidCreditCardNumber()
        {
            //// check whether input string is null or empty
            if (string.IsNullOrEmpty(this.CardNumber))
            {
                return false;
            }

            //// 1.	Starting with the check digit double the value of every other digit 
            //// 2.	If doubling of a number results in a two digits number, add up
            ///   the digits to get a single digit number. This will results in eight single digit numbers                    
            //// 3. Get the sum of the digits
            int sumOfDigits = this.CardNumber.Where((e) => e >= '0' && e <= '9')
                            .Reverse()
                            .Select((e, i) => ((int)e - 48) * (i % 2 == 0 ? 1 : 2))
                            .Sum((e) => e / 10 + e % 10);


            //// If the final sum is divisible by 10, then the credit card number
            //   is valid. If it is not divisible by 10, the number is invalid.            
            return sumOfDigits % 10 == 0;
        }


        public Order GetTotalAmount(long userId, int vendorId)
        {
            return new Order(this.UserId, this.VendorId);
        }

          #endregion
    }
}
