﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Security.Cryptography;
using DAL;
using System.Web;
using System.Web.Mvc;
using System.Data.Entity.Core.Objects;
using System.Data.Entity.Infrastructure;
using System.Text.RegularExpressions;
using System.Collections.Specialized;
using BLL.ZetZlearnService;
using System.Web.Security;
using System.Web.Configuration;
using System.Reflection;
using System.ComponentModel;
using System.Web.Script.Serialization;

namespace BLL
{
    public static class Extensions
    {
        #region ' Static and Void'

        public static List<CourseInCart> GetShoppingCartContents(int BrandId, long? cartId, ZipMartEntities ze, MpPromotion mpPromotion, long userId, int vendorTypeId)
        {
            List<CourseInCart> coursesInCart = new List<CourseInCart>();
            decimal oldPrice;
            decimal curPrice;
            Dictionary<long, int> courseSeats = new Dictionary<long, int>();
            if (cartId > 0)
            {
                MpShoppingCart sc = ze.MpShoppingCart.Get((long)cartId);
                if (mpPromotion != null)
                {
                    sc.MpPromotion = mpPromotion;
                    ze.SaveChanges();
                }
                int ct = 0;
                if (sc != null && sc.MpShoppingSeat.Count() > 0)
                {
                    foreach (var r in sc.MpShoppingSeat.Where(x => x.VendorId == BrandId))
                    {
                        int seatCount = sc.MpShoppingSeat.Where(x => x.CourseInstanceId == r.CourseInstanceId).Count();

                        MpCourse c = r.MpCourseInstance.MpCourse;

                        if (seatCount == 1)
                        {
                            oldPrice = (c.MpCourseOldPrice != null) ? c.MpCourseOldPrice.OldPrice : 0;
                            curPrice = CoursePricingClass.GetSubscribedCoursePrice(BrandId, c.CourseId, r.CourseInstanceId, userId, seatCount, c.PublisherId ?? 0, vendorTypeId);
                            if (mpPromotion != null && mpPromotion.Discount > 0)
                            {
                                curPrice = mpPromotion.PriceCheck(ze, BrandId, c.CourseId, (double)curPrice);
                            }
                            coursesInCart.Add(new CourseInCart() { CourseInstanceId = r.CourseInstanceId, Qty = 1, OldPrice = oldPrice, CourseId = c.CourseId, CourseName = c.Name, CurrPrice = curPrice, CourseImageId = c.MpImage.ImageId, Hours = c.Hours == null ? 0 : (double)c.Hours, PublisherName = c.MpPublisher.Name, Stars = c.MpCourseRating.GetStars() });
                        }
                        else
                        {
                            if (courseSeats.TryGetValue(r.CourseInstanceId, out ct))
                                courseSeats[r.CourseInstanceId] = ct + 1;
                            else
                                courseSeats.Add(r.CourseInstanceId, 1);
                        }
                    }

                    foreach (var r in courseSeats)
                    {
                        MpCourseInstance ci = ze.MpCourseInstance.Single(x => x.CourseInstanceId == r.Key);
                        MpCourse c = ci.MpCourse;
                        oldPrice = (c.MpCourseOldPrice != null) ? c.MpCourseOldPrice.OldPrice : 0;
                        curPrice = CoursePricingClass.GetSubscribedCoursePrice(BrandId, c.CourseId, ci.CourseInstanceId, userId, 1, c.PublisherId ?? 0, vendorTypeId);

                        coursesInCart.Add(new CourseInCart() { CourseInstanceId = r.Key, Qty = r.Value, OldPrice = oldPrice, CourseId = c.CourseId, CourseName = c.Name, CurrPrice = curPrice, CourseImageId = c.MpImage.ImageId, Hours = c.Hours == null ? 0 : (double)c.Hours, PublisherName = c.MpPublisher.Name, Stars = c.MpCourseRating.GetStars() });
                    }
                }
            }
            return coursesInCart;
        }

        public static bool IsSoldout(byte formatId, int courseId, long courseInstanceId)
        {
            if (formatId != 4) // If not a session course don't bother checking anything else, they will always be available
                return false;

            DateTime expireTime = DateTime.Now.AddMinutes(-25);
            List<long> shoppingSeatIdToDelete = new List<long>();
            using (DAL.ZipMartEntities ze = new DAL.ZipMartEntities())
            {
                MpCourseInstance courseInstance = ze.MpCourseInstance.SingleOrDefault(x => x.CourseInstanceId == courseInstanceId);
                if (courseInstance != null && courseInstance.IsSessionSold == true)
                {
                    MpOrderSeat orderSeat = ze.MpOrderSeat.Where(x => x.CourseInstanceId == courseInstanceId).FirstOrDefault(); // Check if this instance has been purchased and paid for
                    if (orderSeat != null)
                        return true;  // The course has been paid for, therefore is truly sold - leave everything alone and show "Sold out"

                    MpShoppingSeat[] ss = ze.MpShoppingSeat.Where(x => x.CourseInstanceId == courseInstanceId).ToArray();
                    foreach (MpShoppingSeat s in ss) // for each shopping seat record that has this particular session, check the datetime stamp
                    {
                        if (s.RCD < expireTime)
                            shoppingSeatIdToDelete.Add(s.ShoppingSeatId); // Mark this record for deletion because it exceeded 25 minutes
                        else if (s.RCD == null)
                            shoppingSeatIdToDelete.Add(s.ShoppingSeatId); // Mark this record for deletion because it has not timestamp (maybe old entries)
                        else
                            return true; // *) The course is in someone's shopping cart and was put there less than 25 minutes ago, so it is not available at the moment - leave everything and show "Sold out"
                    }

                    if (shoppingSeatIdToDelete.Count() == ss.Count()) // If all records in the shopping cart are time stamped > 25 minutes ago, remove them all and mark the course as available
                    {
                        courseInstance.IsSessionSold = null;
                        ze.MpShoppingSeat.RemoveRange(ss);
                        ze.SaveChanges();
                        return false; // This course is marked available
                    }
                    else if (shoppingSeatIdToDelete.Count() > 0) // If some of the shopping cart records are time stamped 25 minutes ago, remove them but mark as Sold out because some still have current timestamp
                    {
                        MpShoppingSeat[] ss1 = ze.MpShoppingSeat.Where(x => shoppingSeatIdToDelete.Contains(x.ShoppingSeatId)).ToArray();
                        ze.MpShoppingSeat.RemoveRange(ss1);
                        ze.SaveChanges();
                        return true;  // Mark Sold out, some shopping cart record are still current
                    }
                    else
                        return true; // Mark sold out because all shopping cart records for this course are timestamped less than 25 minutes ago - **Code will not ever fall into here because itwould have gone to *), but just in case **
                }
                else
                    return false;  // This course is not marked as Not Sold out, so return false (Course isstill available)
            }
        }

        public static List<SelectListItem> GetVendorTypes()
        {
            using (ZipMartEntities ze = new ZipMartEntities())
            {
                MpVendorType[] vendorTypes = ze.MpVendorType.OrderBy(x => x.VendorTypeId).ToArray();
                List<SelectListItem> vt = new List<SelectListItem>();
                foreach (MpVendorType v in vendorTypes)
                {
                    vt.Add(new SelectListItem() { Text = v.Description, Value = v.VendorTypeId.ToString() });
                }
                return vt;
            }
        }

        #endregion


        #region ' MpCourseRating '
        public static string GetStars(this MpCourseRating courseRating)
        {
            StringBuilder sb = new StringBuilder();
            if (courseRating != null)
            {
                for (int i = 0; i < (int)Math.Round(courseRating.AverageStars); i++)
                {
                    sb.Append("<img class=\"imgStars\" src=/Contents/Images/starAA.png />");
                }
                for (int i = (int)Math.Round(courseRating.AverageStars); i < 5; i++)
                {
                    sb.Append("<img class=\"imgStars\" src=/Contents/Images/starCC.png />");
                }

                sb.Append(" (" + courseRating.RecCount + ") Reviews");
            }
            else
            {
                for (int i = 0; i < 5; i++)
                    sb.Append("<img class=\"imgStars\" src=/Contents/Images/starCC.png />");

                sb.Append(" (0) Reviews");
            }
            return sb.ToString();
        }
        #endregion

        #region ' System.Security.Principal '

        public static bool IsPreAuthorized(this System.Security.Principal.IPrincipal user, BLL.ZetGatewayService.TransactionResponse transactionResponse, int vendorId)
        {
            if (!user.Identity.IsAuthenticated)
                return false;
            else if (transactionResponse == null)
                return false;
            else if (!transactionResponse.Approved)
                return false;

            using (ZipMartEntities ze = new ZipMartEntities())
            {
                MpUser mpUser = ze.MpUser.Get(long.Parse(user.Identity.GetUserData(UserDataKeys.UID)));
                decimal totalInShoppingCart = mpUser.TotalAmount(vendorId);
                if (Math.Round(totalInShoppingCart, 2, MidpointRounding.AwayFromZero) > transactionResponse.Amount)
                    return false;
            }
            return true;
        }

        #endregion

        #region ' CreditCard '

        public static BLL.ZetGatewayService.TransactionResponse PreAuthorizeCreditCard(this CreditCard cc)
        {
            using (BLL.ZetGatewayService.GatewayClient gatewayClient = new BLL.ZetGatewayService.GatewayClient())
            {
                if (!IsTestEnvironment())
                {
                    if (HttpContext.Current.Session["ZetCCTransaction"] != null)
                    {
                        var authoOnly = (BLL.ZetGatewayService.TransactionResponse)HttpContext.Current.Session["ZetCCTransaction"];
                        authoOnly.VoidTransaction(1);
                        HttpContext.Current.Session["ZetCCTransaction"] = null;
                    }
                }
                BLL.ZetGatewayService.TransactionRequest gt = new BLL.ZetGatewayService.TransactionRequest();
                gt.CardNumber = cc.CardNumber;
                gt.UserId = cc.UserId;
                gt.ExpirationDate = cc.ExpirationMonth + cc.ExpirationYear.ToString().Substring(2, 2);
                gt.CardCode = cc.CCV;
                gt.FirstName = cc.CardFirstName;
                gt.LastName = cc.CardLastName;
                gt.Address = new BLL.ZetGatewayService.Address();
                gt.Address.Address1 = cc.Address1;
                gt.Address.Address2 = cc.Address2;
                gt.Address.City = cc.City;
                gt.Address.StateId = cc.State;
                gt.Address.CountryId = cc.CountryId;
                gt.Address.Zip = cc.Zip;
                Order ord = cc.GetTotalAmount(cc.UserId, cc.VendorId);
                gt.Amount = ord.Total;
                gt.Tax = ord.Tax;
                gt.Email = cc.Email;
                gt.Description = "ZipMart Course MarketPlace";
                if (IsTestEnvironment())
                    return FakePreauthorize(gt);
                else
                    return gatewayClient.AuthorizationOnly(gt, gatewayClient.GetToken());
            }
        }

        public static BLL.ZetGatewayService.TransactionResponse ChargeCard(this CreditCard cc)
        {
            using (BLL.ZetGatewayService.GatewayClient gatewayClient = new BLL.ZetGatewayService.GatewayClient())
            {
                if (!IsTestEnvironment())
                {
                    if (HttpContext.Current.Session["ZetCCTransaction"] != null)
                    {
                        var authoOnly = (BLL.ZetGatewayService.TransactionResponse)HttpContext.Current.Session["ZetCCTransaction"];
                        authoOnly.VoidTransaction(1);
                        HttpContext.Current.Session["ZetCCTransaction"] = null;
                    }
                }
                BLL.ZetGatewayService.TransactionRequest gt = new BLL.ZetGatewayService.TransactionRequest();
                gt.CardNumber = cc.CardNumber;
                gt.UserId = cc.UserId;
                gt.ExpirationDate = cc.ExpirationMonth + cc.ExpirationYear.ToString().Substring(2, 2);
                gt.CardCode = cc.CCV;
                gt.FirstName = cc.CardFirstName;
                gt.LastName = cc.CardLastName;
                gt.Address = new BLL.ZetGatewayService.Address();
                gt.Address.Address1 = cc.Address1;
                gt.Address.Address2 = cc.Address2;
                gt.Address.City = cc.City;
                gt.Address.StateId = cc.State;
                gt.Address.CountryId = cc.CountryId;
                gt.Address.Zip = cc.Zip;
                Order ord = cc.GetTotalAmount(cc.UserId, cc.VendorId);
                gt.Amount = ord.Total;
                gt.Tax = ord.Tax;
                gt.Email = cc.Email;
                gt.Description = "ZipMart Course MarketPlace";
                if (IsTestEnvironment())
                    return FakePreauthorize(gt);
                else
                    return gatewayClient.AuthorizationAndProcess(gt, gatewayClient.GetToken());
            }
        }


        public static BLL.ZetGatewayService.TransactionResponse VoidTransaction(this BLL.ZetGatewayService.TransactionResponse transaction, byte transactionType = 1)
        {
            using (BLL.ZetGatewayService.GatewayClient gatewayClient = new BLL.ZetGatewayService.GatewayClient())
            {
                if (transactionType == 1)
                    return gatewayClient.VoidByTransactionId(transaction.TransactionId, gatewayClient.GetToken());
                else
                    return gatewayClient.VoidByInvoceNum(long.Parse(transaction.InvoiceNumber), gatewayClient.GetToken());
            }
        }

        public static BLL.ZetGatewayService.TransactionResponse ChargeCreditCard(this BLL.ZetGatewayService.TransactionResponse transaction)
        {
            using (BLL.ZetGatewayService.GatewayClient gatewayClient = new BLL.ZetGatewayService.GatewayClient())
            {
                if (IsTestEnvironment())
                {
                    transaction.InvoiceNumber = DateTime.Now.Ticks.ToString();
                    return FakeCharge(transaction);
                }
                else
                    return gatewayClient.PriorAuthorizationProcess(transaction.TransactionId, transaction.Amount, transaction.Tax, gatewayClient.GetToken());
            }
        }


        private static ZetGatewayService.TransactionResponse FakePreauthorize(ZetGatewayService.TransactionRequest gt)
        {
            ZetGatewayService.TransactionResponse resp = new ZetGatewayService.TransactionResponse() { Address = gt.Address, Amount = gt.Amount, Approved = true, AuthorizationCode = "FAKE-AUTH", AvsResponse = "", CardCode = gt.CardCode, CardNumber = gt.CardNumber, CardType = "Visa", CcvResponse = "", CcvResponseCode = null, Description = "", Email = gt.Email, ExpirationDate = gt.ExpirationDate, ExtensionData = gt.ExtensionData, FirstName = gt.FirstName, InvoiceNumber = "", LastName = gt.LastName, Message = "Test Successfull", ResponseCode = null, Tax = gt.Tax, TransactionId = "1", TransactionTypeId = 1, UserId = gt.UserId };
            return resp;
        }

        private static ZetGatewayService.TransactionResponse FakeCharge(ZetGatewayService.TransactionResponse transaction)
        {
            transaction.Approved = true;
            return transaction;
        }

        private static bool IsTestEnvironment()
        {
            using (ZipMartEntities ze = new ZipMartEntities())
            {
                MpKeyValue kv = ze.MpKeyValue.FirstOrDefault(x => x.Key == "ByPassCreditCard");
                if (kv != null && kv.Value.ToLower() == "yes")
                    return true;
                else
                    return false;
            }
        }

        #endregion

        #region ' MpCategory '

        public static MpCategory GetCategory(this System.Data.Entity.DbSet<MpCategory> mpCategory, int categoryId)
        {
            return mpCategory.Where(x => x.CategoryId == categoryId).SingleOrDefault();
        }

        public static MpCategory[] GetParentHierarchy(this MpCategory mpCategory)
        {
            List<MpCategory> catList = new List<MpCategory>();
            using (ZipMartEntities zetMpEntitiy = new ZipMartEntities())
            {
                while (mpCategory != null)
                {
                    catList.Insert(0, mpCategory);
                    mpCategory = zetMpEntitiy.MpCategory.Where(c => c.CategoryId == mpCategory.ParentId).FirstOrDefault();
                }
                return catList.ToArray();
            }
        }

        public static MpCategory[] GetCategories(this System.Data.Entity.DbSet<MpCategory> mpCategory, int? parentId)
        {
            return mpCategory.Where(x => x.ParentId == parentId).ToArray();
        }

        public static MpCourse[] GetCourses(this System.Data.Entity.DbSet<MpCategory> mpCategory, int categoryId)
        {
            MpCategory category = mpCategory.Where(x => x.CategoryId == categoryId).SingleOrDefault();
            return category.MpCourse.OrderBy(x => x.Name).ToArray();
        }
        #endregion

        #region ' MpCourse '

        public static int BuildCourseCategories(this MpCourse course)
        {
            int nbrOfCat = 1;
            int parentId;
            using (ZipMartEntities ze = new ZipMartEntities())
            {
                MpCourse newCourse = ze.MpCourse.Where(x => x.CourseId == course.CourseId).Single();
                MpCategory cat = course.MpCategory.FirstOrDefault();
                while (cat.ParentId != null)
                {
                    parentId = (int)cat.ParentId;
                    cat = course.MpCategory.Where(x => x.CategoryId == parentId).FirstOrDefault();
                    if (cat == null)
                    {
                        cat = ze.MpCategory.Where(x => x.CategoryId == parentId).Single();
                        newCourse.MpCategory.Add(cat);
                    }
                }
                nbrOfCat += ze.SaveChanges();
                return nbrOfCat;
            }
        }

        public static MpCourse AdminGet(this System.Data.Entity.DbSet<MpCourse> mpCourses, int courseId)
        {
            return mpCourses.Where(x => x.CourseId == courseId).SingleOrDefault();
        }

        public static MpCourse Get(this System.Data.Entity.DbSet<MpCourse> mpCourses, int courseId)
        {
            return mpCourses.Where(x => x.CourseId == courseId).SingleOrDefault();
        }

        public static int GetImageId(this MpCourse course)
        {
            if (course.MpImage != null)
                return course.MpImage.ImageId;
            else
                return 38;
        }

        public static MpCategory[] CategoriesArray(this MpCourse course)
        {
            return course.MpCategory.OrderBy(x => x.ParentId).ToArray();
        }

        public static string CourseCategories(this MpCourse course)
        {
            MpCategory[] categories = course.MpCategory.OrderBy(x => x.ParentId).ToArray();
            List<string> cats = new List<string>();
            foreach (MpCategory cat in categories)
            {
                cats.Add(cat.Name);
            }

            return String.Join(", ", cats.ToArray());
        }

        public static string PriceRange(this MpCourse course)
        {
            decimal minPrice = course.MpCourseInstance.Min(x => x.Price);
            decimal maxPrice = course.MpCourseInstance.Max(x => x.Price);
            string PriceRange = minPrice.ToString("C2") + " - " + maxPrice.ToString("C2");
            if (minPrice == maxPrice)
            {
                PriceRange = minPrice.ToString("C2");
            }
            return PriceRange;
        }



        public static string HoursDisp(this MpCourse course)
        {

            if (course.Hours == null)
                return "No credit hours";
            else if (course.Hours >= 1.0)
                return ((double)course.Hours).ToString("N2") + " Hrs";
            else if (course.Hours > 0)
                return (((double)course.Hours) * 60.0).ToString("N0") + " Min";
            else
                return "No credit hours";
        }

        public static string GetKeywords(this MpCourse course)
        {
            StringBuilder sb = new StringBuilder();

            if (course.MpKeyword.Count > 0)
            {
                foreach (MpKeyword k in course.MpKeyword.ToArray())
                    sb.Append(k.Keyword + ",");
            }
            else
            {
                string[] words = course.Name.Split(' ');
                sb.Append(string.Join(",", words));
            }
            return sb.ToString();
        }

        #endregion

        #region ' MpUser '

        public static MpUser Get(this System.Data.Entity.DbSet<MpUser> mpUsers, long userId)
        {
            return mpUsers.Where(x => x.UserId == userId).SingleOrDefault();
        }
        public static string GetAuthTicket(this System.Data.Entity.DbSet<MpUser> mpUsers, long userId)
        {
            using (BLL.ZetUserService.UserClient zetUserClient = new BLL.ZetUserService.UserClient())
            {
                return zetUserClient.CreateAuthTicket(userId, 1, zetUserClient.GetToken());
            }
        }
        #endregion

        #region ' MpCourseInstance '

        public static MpCourseInstance Get(this System.Data.Entity.DbSet<MpCourseInstance> mpCourseInstances, long courseInstanceId)
        {
            return mpCourseInstances.Where(x => x.CourseInstanceId == courseInstanceId).SingleOrDefault();
        }

        public static MpCourseInstance GetInstance(this MpCourse course, long courseInstanceId)
        {
            return course.MpCourseInstance.Where(x => x.CourseInstanceId == courseInstanceId).SingleOrDefault();
        }

        #endregion

        #region ' MpShoppingCart '

        public static bool UpdateItemQty(this MpShoppingCart sCart, ZipMartEntities ze, long courseInstanceId, int qty, int vendorId)
        {
            if (qty == 0)
            {
                ze.uspDeleteShoppingSeats(sCart.CartId, courseInstanceId, vendorId);
                return true;
            }
            else
            {
                MpShoppingSeat[] seats = sCart.MpShoppingSeat.Where(x => x.CourseInstanceId == courseInstanceId).ToArray();
                int diff = qty - seats.Count();

                if (diff > 0)
                {
                    for (int i = 0; i < diff; i++)
                    {
                        MpShoppingSeat newSeat = new MpShoppingSeat();
                        newSeat.CartId = sCart.CartId;
                        newSeat.CourseInstanceId = courseInstanceId;
                        newSeat.VendorId = vendorId;
                        sCart.MpShoppingSeat.Add(newSeat);
                    }
                }
                else if (diff < 0)
                {
                    foreach (MpShoppingSeat s in seats)
                    {
                        if (diff == 0)
                            break;
                        else
                        {
                            ze.MpShoppingSeat.Remove(s);
                            diff++;
                        }
                    }
                }

                return (ze.SaveChanges() > 0);
            }
        }


        public static MpShoppingCart Get(this System.Data.Entity.DbSet<MpShoppingCart> mpShoppingCarts, long cartId)
        {
            return mpShoppingCarts.Where(x => x.CartId == cartId).SingleOrDefault();
        }

        public static int GetItemCount(this System.Data.Entity.DbSet<MpShoppingCart> mpShoppingCarts, long cartId, int vendorId)
        {
            return mpShoppingCarts.Where(x => x.CartId == cartId).Single().MpShoppingSeat.Where(x => x.VendorId == vendorId).Count();
        }

        /// <summary>
        /// Copies the content of source cart object into the destination cart object. 
        /// This will asign all chiled refernces to the destination object.
        /// </summary>
        /// <param name="sourceCart">The source object to copy from.</param>
        /// <param name="destinationCart">The destination object to copy to.</param>
        public static MpShoppingCart CopyTo(this MpShoppingCart sourceCart, MpShoppingCart destinationCart)
        {
            if (sourceCart.MpPromotion != null)
                destinationCart.MpPromotion = sourceCart.MpPromotion;

            DateTime curDate = DateTime.Now;

            foreach (MpShoppingSeat seat in sourceCart.MpShoppingSeat)
            {
                if (seat.MpCourseInstance.MpCourse.FormatId == 4)
                    seat.RCD = curDate;
                destinationCart.MpShoppingSeat.Add(seat);
            }
            foreach (MpShoppingGiftCard giftCard in sourceCart.MpShoppingGiftCard)
            {
                destinationCart.MpShoppingGiftCard.Add(giftCard);
            }
            return destinationCart;
        }

        public static MpOrder ToMpOrder(this MpShoppingCart sourceCart, int vendorId, long userId, int vendorTypeId)
        {
            MpOrder mpOrder = new MpOrder();
            // mpOrder.PromotionCode = sourceCart.PromotionCode; (x => x.VendorId == vendorId).FirstOrDefault();
            var sSeats = sourceCart.MpShoppingSeat.Where(x => x.VendorId == vendorId);
            foreach (MpShoppingSeat seat in sSeats)
            {
                MpOrderSeat newOrderSeat = new MpOrderSeat();
                newOrderSeat.CourseInstanceId = seat.CourseInstanceId;
                newOrderSeat.PromoApplied = (sourceCart.MpPromotion != null && seat.MpCourseInstance.MpCourse.MpPromotion.Any(x => x.PromotionCode == sourceCart.PromotionCode));
                //newOrderSeat.Price = seat.MpCourseInstance.Price;
                var courseSeats = sSeats.Where(x => x.CourseInstanceId == seat.CourseInstanceId).ToList();
                newOrderSeat.Price = BLL.CoursePricingClass.GetOriginalCoursePrice(vendorId, seat.MpCourseInstance.CourseId, seat.CourseInstanceId, courseSeats.Count(), vendorTypeId); // Tier Pricing without discount
                newOrderSeat.VendorId = seat.VendorId;
                mpOrder.MpOrderSeat.Add(newOrderSeat);
            }
            return mpOrder;
        }

        public static decimal TotalAmount(this MpUser user, int vendorId)
        {
            Order order = new Order(user.UserId, vendorId);
            return order.Total;
        }

        #endregion

        #region ' MpVendor '

        public static MpVendor Get(this System.Data.Entity.DbSet<MpVendor> mpVendors, int vendorId)
        {
            return mpVendors.Where(x => x.VendorId == vendorId).SingleOrDefault();
        }

        #endregion

        #region ' MpKeyword '

        public static string ToHashtags(this ICollection<MpKeyword> mpKeywords)
        {
            StringBuilder sb = new StringBuilder();
            foreach (MpKeyword kw in mpKeywords)
            {
                if (sb.Length > 0)
                    sb.Append("," + kw.Keyword.Replace(" ", string.Empty));
                else
                    sb.Append(kw.Keyword.Replace(" ", string.Empty));
            }
            return sb.ToString();
        }

        #endregion

        #region ' MpPromotion '

        public static decimal PriceCheck(this MpPromotion promo, ZipMartEntities ze, int vendorId, int courseId, double coursePrice)
        {
            if (promo != null && (promo.MpCourse.Count() == 0 && ze.MpCourse.Get(courseId).MpPublisher.AcceptAllCoursePromo == true || promo.MpCourse.Any(x => x.CourseId == courseId)))
            {
                return (decimal)(coursePrice - Math.Round(coursePrice * (double)promo.Discount, 2));
            }
            else
                return (decimal)coursePrice;
        }

        public static MpPromotion Get(this System.Data.Entity.DbSet<MpPromotion> mpPromotions, string promoCode, int vendorId)
        {
            if (string.IsNullOrEmpty(promoCode))
                return null;

            MpPromotion promotion = mpPromotions.Where(x => x.PromotionCode == promoCode).SingleOrDefault();
            if (!promotion.IsValid(vendorId))
                return null;

            return promotion;
        }

        public static bool IsValid(this MpPromotion promotion, int vendorId)
        {
            if (promotion == null)
                return false;

            if (string.IsNullOrEmpty(promotion.PromotionCode))
                return false;

            DateTime currDate = DateTime.Now;
            if (promotion.StartDate != null && promotion.StartDate.Value.Date >= currDate.Date)
                return false;

            if (promotion.EndDate != null && promotion.EndDate.Value.Date <= currDate.Date)
                return false;

            MpVendor promoVendor = promotion.MpVendor.SingleOrDefault(x => x.VendorId == vendorId);

            if (promoVendor != null || promotion.MpVendor.Count() == 0)
                return true;
            else
                return false;

        }

        #endregion

        #region ' MpCourseLocation '

        public static string MapLocation(this MpCourseLocation loc)
        {
            if (loc == null)
                return string.Empty;

            StringBuilder sb = new StringBuilder();
            sb.Append("<a target=\"_blank\" href=http://maps.apple.com/?q=");
            if (!string.IsNullOrEmpty(loc.Address) && loc.Address != "Training Facility")
                sb.Append(loc.Address.Replace(" ", "+"));

            if (!string.IsNullOrEmpty(loc.City))
                sb.Append("+" + loc.City.Replace(" ", "+"));

            if (!string.IsNullOrEmpty(loc.State))
                sb.Append("+" + loc.State.Replace(" ", "+"));

            if (!string.IsNullOrEmpty(loc.Zip))
                sb.Append("+" + loc.Zip.Replace(" ", "+"));

            sb.Append(">");

            sb.Append(loc.Address);

            if (!string.IsNullOrEmpty(loc.Room))
                sb.Append(" Room: " + loc.Room);

            sb.Append("<br />");

            sb.Append(loc.City + ",&nbsp;" + loc.State + "&nbsp;" + loc.Zip + "</a>");

            return sb.ToString();
        }

        #endregion

        #region ' MpOrderSeat '
        public static bool Promotion(this MpOrderSeat orderSeat, string promoCode)
        {
            using (ZipMartEntities ze = new ZipMartEntities())
            {
                MpCourseInstance courseInstance = ze.MpCourseInstance.Get(orderSeat.CourseInstanceId);
                MpPromotion pr = courseInstance.MpCourse.MpPromotion.Where(x => x.PromotionCode == promoCode).FirstOrDefault();

                return (pr != null);
            }
        }
        #endregion

        #region ' String '

        public static string Truncate(this string text, int maxlength, bool forSEODescription)
        {
            StringBuilder sb = new StringBuilder();
            string[] words;
            string tmp;

            int nStart = text.ToLower().IndexOf("<iframe");
            int nEnd = text.ToLower().IndexOf("</iframe");

            if (nStart >= 0)
            {
                tmp = text.Substring(nStart, nEnd - nStart + ("</iframe>").Length);
                text = text.Replace(tmp, "");
            }

            words = text.Split(' ');

            foreach (string w in words)
            {
                if (!w.Contains("<") && !w.Contains(">"))
                    sb.Append(w + " ");
                else if (w.Contains(">") && !w.Contains("\n") && forSEODescription)
                {
                    try
                    {
                        sb.Append(w.Substring(w.IndexOf(">") + 1, w.Length - w.IndexOf(">") - 1) + " ");
                    }
                    catch
                    {

                    }
                }
                else if (!forSEODescription)
                    sb.Append(w + " ");

                if (sb.Length > maxlength)
                    return sb.ToString();
            }
            return sb.ToString();
        }

        public static string RemoveFromReturnUrl(this string returnUrl, bool urlEncode = false, string[] itemToRemove = null)
        {
            StringBuilder sb = new StringBuilder();
            int index = returnUrl.IndexOf("?");
            string queryStr = returnUrl.Substring(index + 1);

            string[] keyValueList = queryStr.Split(new string[] { "&" }, StringSplitOptions.RemoveEmptyEntries);
            foreach (string kvStr in keyValueList)
            {
                string[] kv = kvStr.Split(new string[] { "=" }, StringSplitOptions.RemoveEmptyEntries);
                if (kv.Length == 2)
                {
                    string key = kv[0].Trim();
                    string value = kv[1].Trim();
                    bool flag = !(itemToRemove != null && itemToRemove.Any(x => x.ToLowerInvariant() == key.ToLowerInvariant()));
                    if (flag)
                    {
                        if (sb.Length > 0)
                            sb.Append("&");
                        else
                            sb.Append("?");

                        if (urlEncode)
                            sb.AppendFormat("{0}={1}", key, HttpUtility.UrlEncode(value));
                        else
                            sb.AppendFormat("{0}={1}", key, value);
                    }
                }
            }
            sb.Insert(0, returnUrl.Substring(0, index));
            return sb.ToString();
        }

        public static bool IsDate(this string x)
        {
            if (x == null)
                return false;
            else
            {
                DateTime dt;
                bool isDate = true;
                try
                {
                    dt = DateTime.Parse(x);
                }
                catch
                {
                    isDate = false;
                }
                return isDate;
            }
        }

        public static bool IsNum(this string x)
        {
            if (string.IsNullOrEmpty(x))
                return false;
            else
            {
                double Num;
                return double.TryParse(x, out Num);
            }
        }

        public static bool IsValidPassword(this string password)
        {
            char[] specialCharacters = new char[] { '!', '@', '#', '$', '%', '^', '&', '*' };
            foreach (char s in password)
            {
                if (!char.IsLetter(s) & !char.IsNumber(s) & !specialCharacters.Contains(s))
                    return false;
            }

            return true;
        }

        public static bool IsEmailAddress(this string x)
        {
            if (x == null)
                return false;
            else
                return (Regex.IsMatch(x, @"\A(?:[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?)\Z", RegexOptions.IgnoreCase));

        }

        public static bool IsPhoneNo(this string x)
        {
            if (x == null)
                return false;
            else if (x.IsNum())
                return true;
            else if (Regex.IsMatch(x, @"^((\(\d{3}\) ?)|(\d{3}-))?\d{3}-\d{4}$"))
                return true;
            else
                return false;
        }

        public static string Encrypt(this string x)
        {
            UnicodeEncoding unicodeEncoding = new UnicodeEncoding();
            byte[] secret = ProtectedData.Protect(unicodeEncoding.GetBytes(x), null, DataProtectionScope.LocalMachine);
            return Convert.ToBase64String(secret);
        }

        public static string Decrypt(this string x)
        {
            byte[] backagain = Convert.FromBase64String(x);
            UnicodeEncoding unicodeEncoding1 = new UnicodeEncoding();
            byte[] clearbytes = ProtectedData.Unprotect(backagain, null, DataProtectionScope.CurrentUser);
            return unicodeEncoding1.GetString(clearbytes);
        }

        public static byte[] AesEncrypt(this string plainText)
        {
            using (ZipMartEntities zet = new ZipMartEntities())
            {
                return zet.usp_AesEncrypt(plainText).FirstOrDefault();
            }
        }

        public static int? ToNullableInt(this string x)
        {
            if (x.IsNum())
                return int.Parse(x);
            else
                return null;
        }

        public static string PhoneFormat(this string str)
        {
            if (string.IsNullOrEmpty(str))
                return String.Empty;

            if (!str.IsNum())
                return String.Empty;

            return String.Format("{0:(###) ###-####}", Int64.Parse(str));
        }

        public static Dictionary<string, string> ToKeyValue(this string source, string[] exterDelimiter = null, string inerDelimiter = null)
        {
            if (exterDelimiter == null)
                exterDelimiter = new string[] { ";" };

            if (string.IsNullOrEmpty(inerDelimiter))
                inerDelimiter = "=";

            Dictionary<string, string> values = new Dictionary<string, string>();
            string[] keyValueList = source.Split(exterDelimiter, StringSplitOptions.RemoveEmptyEntries);
            foreach (string keyValue in keyValueList)
            {
                string[] kv = keyValue.Split(new string[] { inerDelimiter }, StringSplitOptions.RemoveEmptyEntries);
                values.Add(kv[0].Trim(), kv[1].Trim());
            }
            return values;
        }

        public static string GetQueryStringValue(this string str, string key, string viewBagItem = null)
        {
            if (string.IsNullOrEmpty(str))
            {
                if (viewBagItem != null)
                    return viewBagItem;
                else
                    return "1";
            }

            string[] segments = str.Split('/');

            if (segments.Length > 1)
            {
                for (int i = 0; i < segments.Length; i++)
                {
                    if (segments[i] == key && i < segments.Length)
                        return segments[i + 1];
                }
            }

            segments = str.Split('&');
            if (segments.Length > 1)
            {
                for (int i = 0; i < segments.Length; i++)
                {
                    if (segments[i].Contains(key) && i <= segments.Length)
                        return segments[i].Replace(key + "=", "");
                }
            }

            return "1";
        }

        public static string GetContents(this string key)
        {
            using (ZipMartEntities ze = new ZipMartEntities())
            {
                MpKeyValue mpk = ze.MpKeyValue.Where(x => x.Key == key).FirstOrDefault();
                if (mpk == null)
                    return "Page Contents not found";
                else
                    return mpk.Value;
            }
        }

        public static Dictionary<string, string> ParseJson(this string json)
        {
            if (string.IsNullOrEmpty(json))
                return new Dictionary<string, string>();

            JavaScriptSerializer serializer = new JavaScriptSerializer();
            return serializer.Deserialize<Dictionary<string, string>>(json);
        }

        public static string NormalizeName(this string str)
        {
            if (string.IsNullOrEmpty(str))
                return str;

            str = str.ToLowerInvariant();
            string name = string.Empty;
            string[] parts = str.Split(new string[] { " " }, StringSplitOptions.RemoveEmptyEntries);
            string[] suffixes = { "ii", "iii", "iv", "v", "vi", "vii", "viii", "ix", "x" };
            str = string.Empty;
            for (int i = 0; i < parts.Length; i++)
            {
                if (i > 0)
                {
                    if (suffixes.Contains(parts[i].Trim()))
                        str += " " + parts[i].ToUpperInvariant();
                    else
                        str += " " + parts[i].Substring(0, 1).ToUpperInvariant() + parts[i].Substring(1);
                }
                else
                    str += parts[i].Substring(0, 1).ToUpperInvariant() + parts[i].Substring(1);
            }
            parts = str.Split(new string[] { "-" }, StringSplitOptions.RemoveEmptyEntries);
            str = string.Empty;
            for (int i = 0; i < parts.Length; i++)
            {
                if (i > 0)
                    str += "-" + parts[i].Substring(0, 1).ToUpperInvariant() + parts[i].Substring(1);
                else
                    str += parts[i].Substring(0, 1).ToUpperInvariant() + parts[i].Substring(1);
            }
            return str;
        }

        public static System.Drawing.Imaging.ImageFormat GetImageFormat(this string extension)
        {
            System.Drawing.Imaging.ImageFormat fmt;
            switch (extension)
            {
                case ".jpg":
                case ".jpeg":
                    fmt = System.Drawing.Imaging.ImageFormat.Jpeg;
                    break;
                case ".png":
                    fmt = System.Drawing.Imaging.ImageFormat.Png;
                    break;
                case ".gif":
                    fmt = System.Drawing.Imaging.ImageFormat.Gif;
                    break;
                default:
                    fmt = System.Drawing.Imaging.ImageFormat.Jpeg;
                    break;
            }
            return fmt;
        }

        public static HtmlString ToHtmlString(this String str)
        {
            return new HtmlString(str);
        }

        #endregion

        #region ' int '
        public static Dictionary<byte, string> GetVendorInfo(this int vendorId)
        {
            using (ZipMartEntities ze = new ZipMartEntities())
            {
                MpVendor vendor = ze.MpVendor.Where(x => x.VendorId == vendorId).SingleOrDefault();
                if (vendor == null)
                    vendor = ze.MpVendor.Single(x => x.VendorId == 1);

                if (vendor != null)
                {
                    Dictionary<byte, string> vendorInfo = new Dictionary<byte, string>();
                    vendorInfo.Add(1, vendor.Name);
                    vendorInfo.Add(2, vendor.VendorTypeId.ToString());
                    MpVendorKeyValue vkv = vendor.MpVendorKeyValue.Where(x => x.Key == "Logo").SingleOrDefault();
                    if (vkv != null)
                        vendorInfo.Add(3, vkv.Value);
                    else
                        vendorInfo.Add(3, "No");

                    MpImage img = vendor.MpImage.Where(x => x.Description == "ZipMart Vendor Logo").SingleOrDefault();
                    if (img != null)
                        vendorInfo.Add(4, img.ImageId.ToString());
                    else
                        vendorInfo.Add(4, null);

                    vendorInfo.Add(5, vendor.Description);

                    vendorInfo.Add(6, vendor.IsSubscription.ToString());

                    vkv = vendor.MpVendorKeyValue.Where(x => x.Key == "AdSense").SingleOrDefault();
                    if (vkv != null)
                        vendorInfo.Add(7, vkv.Value);
                    else
                        vendorInfo.Add(7, null);

                    vkv = vendor.MpVendorKeyValue.Where(x => x.Key == "AdSenseHead").SingleOrDefault();
                    if (vkv != null)
                        vendorInfo.Add(8, vkv.Value);
                    else
                        vendorInfo.Add(8, null);

                    vkv = vendor.MpVendorKeyValue.Where(x => x.Key == "StretchBanner").SingleOrDefault();
                    if (vkv != null)
                        vendorInfo.Add(9, vkv.Value);
                    else
                        vendorInfo.Add(9, null);

                    vendorInfo.Add(10, vendor.VendorTypeId.ToString());

                    var kv = ze.MpKeyValue.SingleOrDefault(x => x.Key == "WhyPhone");
                    if (kv != null)
                        vendorInfo.Add(11, kv.Value);
                    else
                        vendorInfo.Add(11, null);

                    vkv = vendor.MpVendorKeyValue.Where(x => x.Key == "StripePayment").SingleOrDefault();
                    if (vkv != null)
                        vendorInfo.Add(12, vkv.Value);
                    else
                        vendorInfo.Add(12, null);

                    return vendorInfo;
                }
                else
                    return null;
            }
        }
        #endregion

        #region ' Datetime '
        public static DateTime? MergeDateAndTime(this DateTime? date, string time)
        {
            if (date == null)
                return null;

            if (string.IsNullOrEmpty(time.Trim()))
                return date;

            string dteTimeMerged = ((DateTime)date).ToString("d") + " " + time;
            if (dteTimeMerged.IsDate())
                return DateTime.Parse(dteTimeMerged);
            else
                return date;
        }
        #endregion

        #region ' Byte[] '

        public static string AesDecrypt(this byte[] cipherText)
        {
            using (ZipMartEntities zet = new ZipMartEntities())
            {
                return zet.usp_AesDecrypt(cipherText).FirstOrDefault();
            }
        }

        public static byte[] ConvertToBinary(this HttpPostedFileBase file)
        {
            byte[] buffer = new byte[file.ContentLength];
            file.InputStream.Read(buffer, 0, file.ContentLength);

            return buffer;
        }

        #endregion

        #region ' MpState '

        public static List<SelectListItem> ToSelectListItems(this System.Data.Entity.DbSet<MpState> mpStates, string countryId)
        {
            List<SelectListItem> stateList = new List<SelectListItem>();
            foreach (var r in mpStates.Where(x => x.CountryId == countryId).OrderBy(y => y.StateId))
            {
                stateList.Add(new SelectListItem { Text = r.StateId, Value = r.StateId });
            }
            return stateList;
        }

        #endregion

        #region ' MpCountry '

        public static List<SelectListItem> ToSelectListItems(this System.Data.Entity.DbSet<MpCountry> mpCountries)
        {
            List<SelectListItem> stateList = new List<SelectListItem>();
            foreach (var r in mpCountries.OrderBy(x => x.Name))
            {
                stateList.Add(new SelectListItem { Text = r.Name, Value = r.CountryId });
            }
            SelectListItem us = stateList.Where(x => x.Value == "US").Single();
            stateList.Remove(us);
            stateList.Insert(0, us);
            return stateList;
        }

        #endregion

        #region ' Controller '

        /// <summary>
        /// Gets Shopping Cart Cookie
        /// </summary>
        public static HttpCookie GetShoppingCartCookie(this Controller controller, string brandId)
        {
            return controller.Request.Cookies["ShoppingCartId-" + brandId];
        }

        /// <summary>
        /// Sets shopping cart of the user by merging cookie and user shopping carts if they are different, use user shopping cart if they are the same.
        /// </summary>
        /// <param name="zetEntity">Entity context.</param>
        /// <param name="username">User name</param>
        public static long SetShoppingCart(this Controller controller, ZipMartEntities zetEntity, System.Security.Principal.IPrincipal loggedInUser, string brandId)
        {
            long sCartId = 0;
            MpUser user = null;
            string userId = loggedInUser.Identity.GetUserData(UserDataKeys.UID);
            if (userId != null)
                user = zetEntity.MpUser.Get(long.Parse(userId));

            MpShoppingCart cookieCart = null;
            HttpCookie cookie = controller.GetShoppingCartCookie(brandId);
            if (cookie != null)
            {
                long cookieId = long.Parse(cookie.Value.ToString().Decrypt());
                cookieCart = zetEntity.MpShoppingCart.Get((long)cookieId);
            }

            if (user != null) // If the user is authenticated ****
            {
                if (cookieCart != null && user.MpShoppingCart != null && cookieCart.CartId != user.MpShoppingCart.CartId) // if there is a cookie cart and user shopping cart, and cart Ids are different, merge them and expire the cookie cart
                {
                    cookieCart.CopyTo(user.MpShoppingCart);
                    zetEntity.MpShoppingCart.Remove(cookieCart);
                    zetEntity.SaveChanges();
                    sCartId = user.MpShoppingCart.CartId;
                    cookie.Expires = DateTime.Now.AddDays(-1);
                    controller.Response.SetCookie(cookie);
                }
                else if (cookieCart != null) // if there is a cookie cart and no user shopping cart, put all items from cookie cart to user cart
                {
                    user.MpShoppingCart = cookieCart;
                    zetEntity.SaveChanges();
                    sCartId = user.MpShoppingCart.CartId;
                }
                else if (user.MpShoppingCart != null) // if there is a no cookie cart but there is user shopping cart, just get the cart id
                {
                    sCartId = user.MpShoppingCart.CartId;
                }
                else // if there is a no cookie cart and no user shopping cart, create a new cart
                {
                    sCartId = zetEntity.CreateNewShoppingCart();
                }
            }
            else // If the user is NOT authenticated ****
            {
                if (cookieCart != null)   // If there is a cookie cart, get the cookie cart Id
                    sCartId = cookieCart.CartId;
                else  // if there is no cookie cart, create a new cart
                    sCartId = zetEntity.CreateNewShoppingCart();
            }
            return sCartId;
        }

        /// <summary>
        /// Sets shopping cart of the user by merging cookie and user shopping carts if they are different, use user shopping cart if they are the same.
        /// </summary>
        /// <param name="zetEntity">Entity context.</param>
        /// <param name="username">User name</param>
        public static long SetShoppingCart(this Controller controller, ZipMartEntities zetEntity, long userId, string brandId, long sCartId = 0)
        {
            MpUser user = null;
            if (userId > 0)
                user = zetEntity.MpUser.Get(userId);

            MpShoppingCart cookieCart = null;
            HttpCookie cookie = controller.GetShoppingCartCookie(brandId);
            if (cookie != null)
            {
                long cookieId = long.Parse(cookie.Value.ToString().Decrypt());
                cookieCart = zetEntity.MpShoppingCart.Get((long)cookieId);
            }

            if (user != null) // If the user is authenticated ****
            {
                if (cookieCart != null && user.MpShoppingCart != null && cookieCart.CartId != user.MpShoppingCart.CartId) // if there is a cookie cart and user shopping cart, and cart Ids are different, merge them and expire the cookie cart
                {
                    cookieCart.CopyTo(user.MpShoppingCart);
                    zetEntity.MpShoppingCart.Remove(cookieCart);
                    zetEntity.SaveChanges();
                    sCartId = user.MpShoppingCart.CartId;
                    cookie.Expires = DateTime.Now.AddDays(-1);
                    controller.Response.SetCookie(cookie);
                }
                else if (cookieCart != null) // if there is a cookie cart and no user shopping cart, put all items from cookie cart to user cart
                {
                    user.MpShoppingCart = cookieCart;
                    zetEntity.SaveChanges();
                    sCartId = user.MpShoppingCart.CartId;
                }
                else if (user.MpShoppingCart != null) // if there is a no cookie cart but there is user shopping cart, just get the cart id
                {
                    sCartId = user.MpShoppingCart.CartId;
                }
                else if (sCartId > 0) // if the shopping cart was just created
                {
                    MpShoppingCart shoppingCart = zetEntity.MpShoppingCart.Get(sCartId);
                    user.MpShoppingCart = shoppingCart;
                    zetEntity.SaveChanges();
                }
                else // if there is a no cookie cart and no user shopping cart, create a new cart
                {
                    sCartId = zetEntity.CreateNewShoppingCart();
                }
            }
            else // If the user is NOT authenticated ****
            {
                if (cookieCart != null)   // If there is a cookie cart, get the cookie cart Id
                    sCartId = cookieCart.CartId;
                else  // if there is no cookie cart, create a new cart
                    sCartId = zetEntity.CreateNewShoppingCart();
            }
            return sCartId;
        }


        /// <summary>
        /// Add to Cart, can be called from any controller.
        /// </summary>
        public static long AddToCart(this Controller controller, MpCourseInstance courseInstance, int qty, int vendorId, byte formatId, bool dontAddIfExist = false)  // dontAddIfExist is only true when "Take course" button is clicked in Course Page - to prevent keep adding more and more seat inadvertently
        {
            using (ZipMartEntities ze = new ZipMartEntities())
            {
                MpShoppingCart sCart;
                MpUser user = null;
                long cartId;
                HttpCookie cookie = controller.GetShoppingCartCookie(vendorId.ToString());
                if (controller.User.Identity.IsAuthenticated)   // If user is logged in
                {
                    user = ze.MpUser.Get(long.Parse(controller.User.Identity.GetUserData(UserDataKeys.UID)));
                    if (user.MpShoppingCart != null)   // If the user has a shopping cart
                        cartId = user.MpShoppingCart.CartId;
                    else                               // If user has no shopping cart 
                        cartId = controller.SetShoppingCart(ze, controller.User, vendorId.ToString());
                }
                else  // If user is NOT logged in
                {
                    if (cookie != null)   // If there is a cookie
                    {
                        cartId = long.Parse(cookie.Value.ToString().Decrypt()); // If there is a shopping cart cookie, get shopping cart From Cookie -----
                        sCart = ze.MpShoppingCart.Where(x => x.CartId == cartId).SingleOrDefault();
                        if (sCart == null) // If the shopping cart record in the database has been somehow deleted, create a new shopping cart
                        {
                            cookie.Expires = DateTime.Now.AddDays(-1);
                            controller.Response.SetCookie(cookie);

                            cartId = ze.CreateNewShoppingCart();
                        }
                    }
                    else // If there is no gookie, create a new shopping cart
                    {
                        cartId = ze.CreateNewShoppingCart();
                    }
                }

                sCart = ze.MpShoppingCart.Get(cartId);

                if (dontAddIfExist)
                {
                    if (!sCart.MpShoppingSeat.Where(x => x.CourseInstanceId == courseInstance.CourseInstanceId).Any())
                    {
                        MpShoppingSeat sSeat = new MpShoppingSeat();
                        sSeat.CourseInstanceId = courseInstance.CourseInstanceId;
                        sSeat.VendorId = vendorId;
                        if (formatId == 4)
                            sSeat.RCD = DateTime.Now;
                        sCart.MpShoppingSeat.Add(sSeat);
                    }
                }
                else
                {
                    for (int i = 0; i < qty; i++)
                    {
                        MpShoppingSeat sSeat = new MpShoppingSeat();
                        sSeat.CourseInstanceId = courseInstance.CourseInstanceId;
                        sSeat.VendorId = vendorId;
                        if (formatId == 4)
                            sSeat.RCD = DateTime.Now;
                        sCart.MpShoppingSeat.Add(sSeat);
                    }
                }

                if (user != null)
                {
                    // After adding to shopping cart, removes the items from wishlist.
                    MpUserWishList[] userWishlist = user.MpUserWishList.Where(x => x.CourseInstanceId == courseInstance.CourseInstanceId).ToArray();
                    if (userWishlist.Length > 0)
                        ze.MpUserWishList.RemoveRange(userWishlist);
                }
                ze.SaveChanges();

                if (user == null || (user != null && user.MpShoppingCart == null)) // If user is not logged in or user is logged in but has no shopping cart
                {
                    HttpCookie newCookie = new HttpCookie("ShoppingCartId-" + vendorId, sCart.CartId.ToString().Encrypt());
                    newCookie.Expires = DateTime.Now.AddDays(2);
                    controller.Response.SetCookie(newCookie);
                }
                return sCart.CartId;
            }
        }

        #endregion

        #region ' HttpContextBase '

        public static HttpCookie GetShoppingCartCookie(this HttpContextBase context, string brandId)
        {
            return context.Request.Cookies["ShoppingCartId-" + brandId];
        }

        public static int GetShoppingCartItemCount(this HttpContextBase httpContext, string brandId)
        {
            using (ZipMartEntities ze = new ZipMartEntities())
            {
                long? sCartId = ze.GetShoppingCartId(httpContext, brandId);
                if (sCartId != null)
                {
                    MpShoppingCart sCart = ze.MpShoppingCart.Get((long)sCartId);
                    if (sCart != null)
                        return sCart.MpShoppingSeat.Where(s => s.VendorId == Convert.ToInt32(brandId)).Count();
                }
                return 0;
            }
        }

        #endregion

        #region ' ZipMartEntity '

        public static long? GetShoppingCartId(this ZipMartEntities zipEntity, HttpContextBase httpContext, string brandId, string userId1 = null)
        {
            string userId = userId1;
            if (httpContext.User.Identity.IsAuthenticated)
                userId = httpContext.User.Identity.GetUserData(UserDataKeys.UID);

            MpUser mpUser = null;
            if (!string.IsNullOrEmpty(userId))
                mpUser = zipEntity.MpUser.Get(long.Parse(userId));

            HttpCookie cookie = httpContext.GetShoppingCartCookie(brandId);

            if (cookie != null)
            {
                long sCartId = long.Parse(cookie.Value.ToString().Decrypt());
                MpShoppingCart sCart = zipEntity.MpShoppingCart.Get(sCartId);
                if (sCart != null)
                {
                    if (mpUser != null && mpUser.MpShoppingCart == null)
                    {
                        mpUser.MpShoppingCart = sCart;
                        zipEntity.SaveChanges();
                    }
                    return sCart.CartId;
                }
            }

            if (mpUser != null && mpUser.MpShoppingCart != null)
            {
                return mpUser.MpShoppingCart.CartId;
            }
            return null;
        }

        public static long CreateNewShoppingCart(this ZipMartEntities zipEntity)
        {
            MpShoppingCart sCart = new MpShoppingCart();
            sCart.RCD = DateTime.Now;
            zipEntity.MpShoppingCart.Add(sCart);
            zipEntity.SaveChanges();
            return sCart.CartId;
        }

        #endregion

        #region ' ZetUserService.UserClient '

        public static string GetToken(this ZetUserService.UserClient userClient)
        {
            string token = null;
            if (System.Web.HttpContext.Current.Session["UserToken"] != null)
                token = System.Web.HttpContext.Current.Session["UserToken"].ToString();

            int timeout = System.Web.HttpContext.Current.Session.Timeout;
            if (string.IsNullOrEmpty(token))
            {
                token = userClient.CreateToken("zipmart_user", "ziP*Pass$", timeout);

            }
            else
            {
                if (userClient.HasTokenExpired(token))
                    token = userClient.CreateToken("zipmart_user", "ziP*Pass$", timeout);
            }
            System.Web.HttpContext.Current.Session["UserToken"] = token;
            return token;
        }

        public static string TemporaryPassword()
        {
            string[] letters = new string[] { "A", "B", "C", "D", "E", "F", "G", "H", "J", "K", "M", "N", "O", "P", "R", "S", "T", "U", "V", "W", "Y", "Z" };
            byte lettersCount = (byte)(letters.Count() - 1);
            Random rnd = new Random(DateTime.Now.Millisecond);
            StringBuilder sb = new StringBuilder();
            sb.Append(letters[rnd.Next(0, lettersCount)]);
            sb.Append(letters[rnd.Next(0, lettersCount)].ToLower());
            sb.Append(letters[rnd.Next(0, lettersCount)]);
            sb.Append(letters[rnd.Next(0, lettersCount)].ToLower());
            sb.Append(rnd.Next(1000, 9999));
            return sb.ToString();
        }

        /// <summary>
        /// This method is wirtten to verify/Enroll user in Zlearning system.
        /// </summary>
        /// <param name="userName"></param>
        public static ErrorCodes? EnrollTo(this ZetUserService.ZetUser zUser, long zlearnId)
        {
            using (ZlearnClient zLearnClient = new ZlearnClient())
            {
                string zToken = zLearnClient.GetToken();
                ZlearnUser zLearnUser = zLearnClient.UserGetUserByUsername(zUser.UserId.ToString(), zToken);
                if (zLearnUser == null)
                {
                    zLearnUser = new ZlearnUser();
                    zLearnUser.Username = zUser.UserId.ToString();
                    zLearnUser.FirstName = zUser.FirstName;
                    zLearnUser.LastName = zUser.LastName;
                    zLearnUser.Password = "12345678";
                    zLearnUser.Email = zUser.Username;
                    zLearnClient.UserCreateUser(zLearnUser, zToken);
                    zLearnUser = zLearnClient.UserGetUserByUsername(zUser.UserId.ToString(), zToken);
                    if (zLearnUser == null)
                        return ErrorCodes.Error_3;
                }
                ZlearnCourse course = zLearnClient.CourseGetCourseByCourseId(zlearnId, zToken);
                if (course == null)
                    return ErrorCodes.Error_4;

                //For student roleId=5
                zLearnClient.CourseEnrollUsers(course, new long[] { (long)zLearnUser.Id }, 5, null, null, false, zToken);
                return null;
            }
        }

        /// <summary>
        /// This method is wirtten to UnEnroll users in Zlearning system.
        /// </summary>
        /// <param name="userName"></param>
        public static ErrorCodes? UnEnrollUser(this long userId, long zlearnId)
        {
            using (ZlearnClient zLearnClient = new ZlearnClient())
            {
                string zToken = zLearnClient.GetToken();
                ZlearnCourse zCourse = zLearnClient.CourseGetCourseByCourseId(zlearnId, zToken);
                if (zCourse == null)
                    return ErrorCodes.Error_4;
                ZlearnUser zUser = zLearnClient.UserGetUserByUsername(userId.ToString(), zToken);
                if (zUser != null)
                {
                    bool result = zLearnClient.CourseUnenrollUsers(zCourse, new long[] { (long)zUser.Id }, zToken);
                }
                return null;
            }
        }
        #endregion

        #region ' ZetCertificateService.CertificateClient '

        public static string GetToken(this ZetCertificationService.CertificationClient certificationClient)
        {
            string token = null;
            if (System.Web.HttpContext.Current.Session["CertificateToken"] != null)
                token = System.Web.HttpContext.Current.Session["CertificateToken"].ToString();

            int timeout = System.Web.HttpContext.Current.Session.Timeout;
            if (string.IsNullOrEmpty(token))
            {
                token = certificationClient.CreateToken("zipmart_certificate", "zip?c59!", timeout);
            }
            else
            {
                if (certificationClient.HasTokenExpired(token))
                    token = certificationClient.CreateToken("zipmart_certificate", "zip?c59!", timeout);
            }
            System.Web.HttpContext.Current.Session["CertificateToken"] = token;
            return token;
        }

        #endregion

        #region ' ZetZlearnService.ZlearnClient '

        public static string GetToken(this ZetZlearnService.ZlearnClient zlearnClient)
        {
            string token = null;
            if (System.Web.HttpContext.Current.Session["ZlearnToken"] != null)
                token = System.Web.HttpContext.Current.Session["ZlearnToken"].ToString();

            int timeout = System.Web.HttpContext.Current.Session.Timeout;
            string zlearnName = WebConfigurationManager.AppSettings["zlearn_user"].ToString();

            if (string.IsNullOrEmpty(token))
            {
                token = zlearnClient.CreateToken(zlearnName, "$et14Rajm", timeout);
            }
            else
            {
                if (zlearnClient.HasTokenExpired(token))
                    token = zlearnClient.CreateToken(zlearnName, "$et14Rajm", timeout);
            }
            System.Web.HttpContext.Current.Session["ZlearnToken"] = token;
            return token;
        }

        public static string GetToken(this ZetAppSettingsService.AppsClient appClient)
        {
            string token = null;
            if (System.Web.HttpContext.Current.Session["AppToken"] != null)
                token = System.Web.HttpContext.Current.Session["AppToken"].ToString();

            int timeout = System.Web.HttpContext.Current.Session.Timeout;

            if (string.IsNullOrEmpty(token))
            {
                token = appClient.CreateToken("zipmart_settings", "rt#h5n@4", timeout);
            }
            else
            {
                if (appClient.HasTokenExpired(token))
                    token = appClient.CreateToken("zipmart_settings", "rt#h5n@4", timeout);
            }
            System.Web.HttpContext.Current.Session["AppToken"] = token;
            return token;
        }

        #endregion

        #region ' ZetGatewayService.GatewayClient '

        public static string GetToken(this ZetGatewayService.GatewayClient gatewayClient)
        {
            string token = null;
            if (System.Web.HttpContext.Current.Session["GatewayToken"] != null)
                token = System.Web.HttpContext.Current.Session["GatewayToken"].ToString();

            int timeout = System.Web.HttpContext.Current.Session.Timeout;
            if (string.IsNullOrEmpty(token))
            {
                token = gatewayClient.CreateToken("zipmart_gateway", "ze*ip?c59!", timeout);
            }
            else
            {
                if (gatewayClient.HasTokenExpired(token))
                    token = gatewayClient.CreateToken("zipmart_gateway", "ze*ip?c59!", timeout);
            }
            System.Web.HttpContext.Current.Session["GatewayToken"] = token;
            return token;
        }

        #endregion

        #region 'Image'

        public static byte[] ConvertImageToByteArray(this System.Drawing.Image imageToConvert, System.Drawing.Imaging.ImageFormat formatOfImage)
        {
            byte[] Ret;
            try
            {
                using (System.IO.MemoryStream ms = new System.IO.MemoryStream())
                {
                    imageToConvert.Save(ms, formatOfImage);
                    Ret = ms.ToArray();
                }
            }
            catch (Exception) { throw; }
            return Ret;
        }

        #endregion

        #region 'System.Collections.Specialized.NameValueCollection'

        public static List<SelectListItem> ToSelectListItems(this System.Collections.Specialized.NameValueCollection nameValueColl)
        {
            List<SelectListItem> dropdownList = new List<SelectListItem>();
            foreach (string r in nameValueColl)
            {
                dropdownList.Add(new SelectListItem { Text = nameValueColl[r], Value = r });
            }
            return dropdownList;
        }

        public static string ToQueryString(this NameValueCollection queryString, bool urlEncode = false, string[] itemToRemove = null)
        {
            StringBuilder sb = new StringBuilder();
            bool flag;
            foreach (string key in queryString)
            {
                if (!string.IsNullOrEmpty(queryString[key]))
                {
                    flag = !(itemToRemove != null && itemToRemove.Any(x => x.ToLowerInvariant() == key.ToLowerInvariant()));
                    if (flag)
                    {
                        if (sb.Length > 0)
                            sb.Append("&");
                        else
                            sb.Append("?");

                        if (urlEncode)
                            sb.AppendFormat("{0}={1}", key, HttpUtility.UrlEncode(queryString[key]));
                        else
                            sb.AppendFormat("{0}={1}", key, queryString[key]);
                    }
                }
            }
            return sb.ToString();
        }

        #endregion

        #region ' System.Security.Principal.IIdentity '

        public static string GetUserData(this System.Security.Principal.IIdentity identity, UserDataKeys key)
        {
            if (identity.IsAuthenticated)
            {
                FormsIdentity fi = (FormsIdentity)identity;
                return fi.Ticket.UserData.ToKeyValue()[key.ToString().ToLowerInvariant()];
            }
            return null;
        }

        #endregion

        #region ' Enum '

        public static string GetName(this Enum value)
        {
            FieldInfo fi = value.GetType().GetField(value.ToString());
            if (fi != null)
            {
                DescriptionAttribute attr = (DescriptionAttribute)fi.GetCustomAttribute(typeof(DescriptionAttribute));
                if (attr != null)
                    return attr.Description;
            }
            return value.ToString();
        }
        #endregion

        #region ' HttpSessionStateBase '

        public static int GetPageNum(this HttpSessionStateBase session)
        {
            int pageNum = 1;
            if (session == null || session["PageNum"] == null)
                session["PageNum"] = pageNum;
            else if (!session["PageNum"].ToString().IsNum())
                session["PageNum"] = pageNum;

            return (int)session["PageNum"];
        }

        #endregion
    }
}
