﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DAL;

namespace BLL
{
    public class GaOrder
    {
        #region ' Properties '

        public long? InvoiceId { get; set; }
        public decimal Revenue { get; set; }
        public decimal Tax { get; set; }
        public List<GaOrderItem> OrderItems { get; set; }

        #endregion

        #region ' Methods '

        private GaOrder()
        {
            this.OrderItems = new List<GaOrderItem>();
        }

        public static GaOrder GetOrder(long invoiceId)
        {
            using (ZipMartEntities zipMartEntity = new ZipMartEntities())
            {
                MpOrder mpOrder = zipMartEntity.MpOrder.Where(x => x.InvoiceId == invoiceId).SingleOrDefault();
                if (mpOrder != null)
                {
                    GaOrder order = new GaOrder();
                    decimal total = mpOrder.MpOrderPayment.Sum(x => x.Amount);
                    decimal tax = mpOrder.MpOrderPayment.Sum(x => x.Tax);
                    order.InvoiceId = mpOrder.InvoiceId;
                    order.Tax = tax;
                    order.Revenue = total - order.Tax;

                    foreach (var osg in mpOrder.MpOrderSeat.GroupBy(x => x.CourseInstanceId))
                    {
                        MpOrderSeat mpOrderSeatGroup = osg.ElementAt(0);
                        GaOrderItem gaOrderItem = new GaOrderItem();

                        gaOrderItem.Id = mpOrderSeatGroup.CourseInstanceId.ToString();
                        MpCourse mpCourse = mpOrderSeatGroup.MpCourseInstance.MpCourse;
                        gaOrderItem.Name = mpCourse.Name;
                        foreach (MpCategory mpCategory in mpCourse.MpCategory)
                        {
                            if (string.IsNullOrEmpty(gaOrderItem.Category))
                                gaOrderItem.Category = mpCategory.Name;
                            else
                                gaOrderItem.Category += ", " + mpCategory.Name;
                        }
                        gaOrderItem.Price = mpOrderSeatGroup.MpCourseInstance.Price;
                        gaOrderItem.Quantity = osg.Count();
                        order.OrderItems.Add(gaOrderItem);
                    }
                    return order;
                }
                return null;
            }
        }

        #endregion
    }
}