﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DAL;

namespace BLL
{
    public class StripeClass
    {
        #region ' Properties '
        public string ErrorMessage { get; set; }
        public decimal AmountCharged { get; set; }
        public DateTime TransactionDateTime { get; set; }
        public string ReceiptNumber { get; set; }
        public string Status { get; set; }
        public long InvoiceNumber { get; set; }
        #endregion

        public bool PostPayment(int vendorId, long userId, long shoppingCartId, decimal totalAmount, decimal tax, string stripeToken, string email, string custName)
        {
            string vendorName = "";
            using (ZipMartEntities ze = new ZipMartEntities())
            {
                MpVendor vendor = ze.MpVendor.Get(vendorId);
                vendorName = vendor.Name;

                Dictionary<string, string> customerInfo = new Dictionary<string, string>();
                customerInfo.Add("UserId", userId.ToString());
                customerInfo.Add("VendorId", vendorId.ToString());
                customerInfo.Add("ShoppingCartId", shoppingCartId.ToString());
                customerInfo.Add("TotalAmount", totalAmount.ToString("C2"));
                customerInfo.Add("Email", email);

                Stripe.StripeChargeService stripeChargeSvc = new Stripe.StripeChargeService(System.Configuration.ConfigurationManager.AppSettings["StripeApiKey"]);
                try
                {
                    var charge = stripeChargeSvc.Create(new Stripe.StripeChargeCreateOptions() { Metadata = customerInfo, Amount = (int)Math.Round(totalAmount * 100, 0), SourceTokenOrExistingSourceId = stripeToken, Currency = "usd", Description = string.Format("{0} online course purchase", vendorName) });

                    CustomerContactInfo zCust = ze.CustomerContactInfo.SingleOrDefault(x => x.CustomerEmailAddress == email);
                    if (zCust == null || zCust.StripeCustId == null)
                    {
                        var customer = new Stripe.StripeCustomerCreateOptions() { Email = email, Description = vendorName + " customer" };
                        Stripe.StripeCustomerService sc = new Stripe.StripeCustomerService(System.Configuration.ConfigurationManager.AppSettings["StripeApiKey"]);
                        var newCust = sc.Create(customer);

                        if (zCust == null)
                        {
                            zCust = new CustomerContactInfo() { CustomerEmailAddress = email, CustomerName = custName, RCD = DateTime.Now, CustomerPhone = "N/A", StripeCustId = newCust.Id };
                            ze.CustomerContactInfo.Add(zCust);
                        }
                        else
                            zCust.StripeCustId = newCust.Id;
    
                        ze.SaveChanges();
                    }
                    this.AmountCharged = (decimal)((double)charge.Amount / 100.00);
                    this.TransactionDateTime = charge.Created;
                    this.ReceiptNumber = charge.ReceiptNumber;
                    this.Status = charge.Status;
                    string message = string.Format("Card: {0}, Last4: {1} Exp: {2}/{3}, Funding:{4}", charge.Source.Card.Brand, charge.Source.Card.Last4, charge.Source.Card.ExpirationMonth, charge.Source.Card.ExpirationYear, charge.Source.Card.Funding);

                    if (string.IsNullOrEmpty(charge.FailureCode))
                    {
                        using (ZET_DATAEntities zd = new ZET_DATAEntities())
                        {
                            GatewayInvoice gi = new GatewayInvoice()
                            {
                                UserId = userId
                                ,
                                Amount = this.AmountCharged
                                ,
                                Approved = true
                                ,
                                AuthorizationCode = this.Status
                                ,
                                Email = email
                                ,
                                CreditCard = charge.Source.Card.Last4
                                ,
                                TransactionId = charge.Id
                                ,
                                TransactionTypeId = 1
                                ,
                                ResponseCode = charge.Paid ? "Paid" : "Unpaid"
                                ,
                                Tax = tax
                                ,
                                Message = message
                                ,
                                ServiceClientId = 2
                                ,
                                RCD = charge.Created
                                ,
                                RMD = DateTime.Now
                            };
                            zd.GatewayInvoice.Add(gi);
                            zd.SaveChanges();
                            this.InvoiceNumber = gi.InvoiceNumber;
                        }
                        return true;
                    }
                    else
                    {
                        this.ErrorMessage = charge.FailureMessage;
                        return false;
                    }
                }
                catch (Exception ex)
                {
                    this.ErrorMessage = StaticMethods.ExceptionMessage(ex).Message;
                    return false;
                }
            }
        }

        public string UpdateCreditCardInfo(string stripeToken, string email)
        {
            string last4;
            string rtnMessage = "No update took place. Please be sure to check Remember Me to save your credit card information. ";
            using (ZipMartEntities ze = new ZipMartEntities())
            {
                CustomerContactInfo zCust = ze.CustomerContactInfo.SingleOrDefault(x => x.CustomerEmailAddress == email);
                if (zCust != null && zCust.StripeCustId != null)
                {
                    Stripe.StripeCustomerService sc = new Stripe.StripeCustomerService(System.Configuration.ConfigurationManager.AppSettings["StripeApiKey"]);
                    try
                    {
                        sc.Update(zCust.StripeCustId, new Stripe.StripeCustomerUpdateOptions() { SourceToken = stripeToken });
                        var resp = sc.Get(zCust.StripeCustId);
                        rtnMessage = "Your new card information has been stored for future purchases.";
                    }
                    catch (Exception ex)
                    {
                        rtnMessage = rtnMessage + ex.Message;
                    }
                }
            }
            return rtnMessage;
        }
    }
}
