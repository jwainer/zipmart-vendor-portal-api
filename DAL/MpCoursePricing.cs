//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace DAL
{
    using System;
    using System.Collections.Generic;
    
    public partial class MpCoursePricing
    {
        public long CoursePricingId { get; set; }
        public int CourseId { get; set; }
        public long CourseInstanceId { get; set; }
        public Nullable<int> VendorId { get; set; }
        public Nullable<int> StartQuantity { get; set; }
        public Nullable<int> EndQuantity { get; set; }
        public Nullable<decimal> Price { get; set; }
    
        public virtual MpCourseInstance MpCourseInstance { get; set; }
        public virtual MpCourse MpCourse { get; set; }
    }
}
