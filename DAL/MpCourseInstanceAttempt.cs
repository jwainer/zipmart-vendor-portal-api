//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace DAL
{
    using System;
    using System.Collections.Generic;
    
    public partial class MpCourseInstanceAttempt
    {
        public long CourseInstanceId { get; set; }
        public byte AttemptsAllowed { get; set; }
        public long ZlearnQuizId { get; set; }
    
        public virtual MpCourseInstance MpCourseInstance { get; set; }
    }
}
