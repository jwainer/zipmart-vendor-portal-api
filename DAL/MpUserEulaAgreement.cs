//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace DAL
{
    using System;
    using System.Collections.Generic;
    
    public partial class MpUserEulaAgreement
    {
        public long CourseSeatId { get; set; }
        public bool Over13 { get; set; }
        public bool AgreeToEULA { get; set; }
        public System.DateTime DateTimeStamp { get; set; }
    
        public virtual MpCourseSeat MpCourseSeat { get; set; }
    }
}
