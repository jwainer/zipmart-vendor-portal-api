//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace DAL
{
    using System;
    
    public partial class uspGetFranchiseUsers_Result
    {
        public long UserId { get; set; }
        public int VendorId { get; set; }
        public Nullable<int> FranchiseId { get; set; }
        public Nullable<int> FranchiseVendorId { get; set; }
        public string Username { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string FranchiseName { get; set; }
    }
}
