﻿using System;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Collections.Generic;
using System.Web.Http;
using Newtonsoft.Json.Linq;
using System.Text;
using VendorApi.Models;

namespace VendorApi.Controllers {
    public class CourseInstanceController : ApiController {
        private string connection = System.Configuration.ConfigurationManager.ConnectionStrings["ziped"].ConnectionString;
        private CourseInstance courseInstances = null;

        [HttpGet]
        [Route("CourseInstance/GetCourseInstanceList/{id}")]
        public List<CourseInstance> GetCourseInstanceList(int id) {
            try {
                string query = @"SELECT MCI.*, MC.Name FROM MpCourseInstance MCI LEFT JOIN MpCourse MC ON MC.CourseId = MCI.CourseId WHERE MC.CourseId = " + id;
                courseInstances = new CourseInstance(this.connection);
                var data = courseInstances.GetCourseInstanceList(query);
                return data;
            } catch (Exception ex) { throw; }
        }

        [HttpGet]
        [Route("CourseInstance/GetCourseInstanceList")]
        public List<CourseInstance> GetCourseInstanceList(int courseId, string startDate, string endDate, bool? isSold, int vendorId) {
            try {
                string query = "SELECT MCI.*, MC.Name FROM MpCourseInstance MCI LEFT JOIN MpCourse MC ON MC.CourseId = MCI.CourseId " +
                                "LEFT JOIN MpCourseVendor MCV ON MC.CourseId = MCV.CourseId " +
                                "WHERE MC.CourseId = " + courseId + " AND MCV.VendorId = " + vendorId;

                if (isSold != null) {
                    query += " AND MCI.IsSessionSold = " + (isSold == true ? 1 : 0);
                }
                if (!string.IsNullOrEmpty(startDate)) {
                    query += " AND MCI.StartDate >= '" + Convert.ToDateTime(startDate).ToString() + "'";
                }
                if (!string.IsNullOrEmpty(endDate)) {
                    query += " AND MCI.EndDate >= '" + Convert.ToDateTime(endDate).ToString() + "'";
                }
                courseInstances = new CourseInstance(this.connection);
                var data = courseInstances.GetCourseInstanceList(query);
                return data;
            } catch (Exception ex) { throw; }
        }

        [HttpPost]
        [Route("CourseInstance/SaveCourseInstance")]
        public void SaveCourseInstance(CourseInstance courseInstance) {
            try {
                courseInstances = new CourseInstance(this.connection);
                courseInstances.SaveCourseInstance(courseInstance);
                //Dictionary<string, string> dict = new Dictionary<string, string>();
                //dict.Add("status", "success");
                //return dict;
            } catch (Exception ex) { throw; }
        }

        [HttpPost]
        [Route("CourseInstance/SaveCourseInstances")]
        public void SaveCourseInstances(CourseInstance courseInstance) {
            try {
                DateTime startDate = Convert.ToDateTime(courseInstance.StartDate);
                DateTime endDate = Convert.ToDateTime(courseInstance.EndDate);

                List<DateTime> instanceStartDates = new List<DateTime>();
                while (endDate >= startDate) {
                    string day = Convert.ToString(startDate.DayOfWeek);
                    if (courseInstance.DaysOfWeek.Contains(day)) {
                        foreach (var slot in courseInstance.TimeSlots) {
                            string strDate = startDate.Date.ToShortDateString() + " " + slot.ToString();
                            DateTime dt = DateTime.Parse(strDate, new System.Globalization.CultureInfo("en-US", true));
                            instanceStartDates.Add(dt);
                        }
                    }
                    startDate = startDate.AddDays(1);
                }

                courseInstances = new CourseInstance(this.connection);
                courseInstances.SaveCourseInstances(courseInstance, instanceStartDates);
                //Dictionary<string, string> dict = new Dictionary<string, string>();
                //dict.Add("status", "success");
                //return dict;
            } catch (Exception ex) { throw; }
        }

        [HttpGet]
        [Route("CourseInstance/GetCourseList")]
        public List<Course> GetCourseList() {
            try {
                string query = @"SELECT * FROM MpCourse";
                courseInstances = new CourseInstance(this.connection);
                var data = courseInstances.GetCourseList(query);
                return data;
            } catch (Exception ex) { throw; }
        }

    }
}
