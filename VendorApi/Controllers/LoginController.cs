﻿using System;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Collections.Generic;
using System.Web.Http;
using Newtonsoft.Json.Linq;
using System.Text;
using VendorApi.Models;
using DAL;

namespace VendorApi.Controllers {
    public class LoginController : ApiController {

        [HttpGet]
        [Route("Login/Authenticate")]
        public Response Authenticate(string userName, string password) {
            Response response = new Response();
            try {
                DAL.ZipMartEntities ze = new DAL.ZipMartEntities();
                var pUser = ze.MpVendorPortalUser.Where(u => u.UserEmail == userName).FirstOrDefault();
                if (pUser != null && pUser.VendorId > 0) {
                    response.message = "success";
                    response.success = true;
                } else {
                    response.message = "Sorry, user name and password not accepted.";                    
                }
            } catch (Exception ex) {
                response.message = "Sorry, Exception occured.";
            }

            return response;
        }

    }

    public class Response {
        public string message = "";
        public bool success = false;
    }
}
