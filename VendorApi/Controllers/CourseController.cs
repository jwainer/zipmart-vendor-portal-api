﻿using System;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Collections.Generic;
using System.Web.Http;
using Newtonsoft.Json.Linq;
using System.Text;
using VendorApi.Models;

namespace VendorApi.Controllers {
    public class CourseController : ApiController {
        private string connection = System.Configuration.ConfigurationManager.ConnectionStrings["ziped"].ConnectionString;
        private Course courseSessions = null;

        [HttpGet]
        [Route("Course/GetCourseList/{id}")] //id = vendorId
        public List<Course> GetCourseList(int id) {
            try {
                string query = @"SELECT C.*, CI.*, MCV.VendorId, MCI.ImageId FROM MpCourse C 
                                LEFT JOIN MpCourseInstance CI ON C.CourseId = CI.CourseId 
                                LEFT JOIN MpCourseImage MCI ON C.CourseId = MCI.CourseId
                                LEFT JOIN MpCourseVendor MCV ON C.CourseId = MCV.CourseId
                                WHERE MCV.VendorId = " + id;
                courseSessions = new Course(this.connection);
                var data = courseSessions.GetCourseList(query);
                return data;
            } catch (Exception ex) { throw; }
        }

        [HttpGet]
        [Route("Course/GetCourseList")]
        public List<Course> GetCourseList(int formatId, int vendorId) {
            try {
                string query = @"SELECT C.*, CI.*, MCV.VendorId, MCI.ImageId FROM MpCourse C 
                                LEFT JOIN MpCourseInstance CI ON C.CourseId = CI.CourseId 
                                LEFT JOIN MpCourseImage MCI ON C.CourseId = MCI.CourseId
                                LEFT JOIN MpCourseVendor MCV ON C.CourseId = MCV.CourseId
                                WHERE MCV.VendorId = " + vendorId + " AND FormatId = " + formatId;
                courseSessions = new Course(this.connection);
                var data = courseSessions.GetCourseList(query);
                return data;
            } catch (Exception ex) { throw; }
        }

        [HttpGet]
        [Route("Course/GetInstructorList")]
        public List<Instructor> GetInstructorList() {
            try {
                string query = @"SELECT * FROM MpInstructor ORDER BY FirstName";
                courseSessions = new Course(this.connection);
                var data = courseSessions.GetInstructorList(query);
                return data;
            } catch (Exception ex) { throw; }
        }

        [HttpPost]
        [Route("Course/CreateCourse")]
        public void CreateCourse(Course course) {
            try {
                courseSessions = new Course(this.connection);
                courseSessions.CreateCourse(course);
                //Dictionary<string, string> dict = new Dictionary<string, string>();
                //dict.Add("status", "success");
                //return dict;
            } catch (Exception ex) { throw; }
        }

        [HttpPost]
        [Route("Course/SaveCourse")]
        public void SaveCourse(Course course) {
            try {
                courseSessions = new Course(this.connection);
                courseSessions.SaveCourse(course);
                //Dictionary<string, string> dict = new Dictionary<string, string>();
                //dict.Add("status", "success");
                //return dict;
            } catch (Exception ex) { throw; }
        }

        [Route("Course/GetCourseCategories")]
        public List<CourseCategories> GetCourseCategories(int courseId) {
            try {
                List<CourseCategories> data = new List<CourseCategories>();
                courseSessions = new Course(this.connection);
                var selectedCategories = courseSessions.GetSelectedCourseCategories(courseId);
                var allCategories = courseSessions.GetAllCourseCategories();
                if (selectedCategories != null && selectedCategories.Count > 0) {
                    foreach (var cat in allCategories) {
                        var category = selectedCategories.Where(c => c.CategoryId == cat.CategoryId).FirstOrDefault();
                        if (category != null && category.CategoryId > 0) {
                            cat.Selected = true;
                            cat.CourseId = courseId;
                            data.Add(cat);
                        } else {
                            cat.Selected = false;
                            cat.CourseId = courseId;
                            data.Add(cat);
                        }
                    }
                } else {
                    data = allCategories;
                }
                return data;
            } catch (Exception ex) { throw; }
        }

        [Route("Course/LinkCtegories")]
        public void LinkCtegories(List<CourseCategories> categories) {
            try {
                foreach (var cat in categories) {
                    string query = string.Empty;
                    if (cat.Selected) {
                        query = @"IF NOT EXISTS (SELECT * FROM MpCourseCategory CC WHERE CC.CategoryId = @categoryId AND CC.CourseId = @courseId)
                            BEGIN
	                            INSERT INTO MpCourseCategory (CategoryId, CourseId) VALUES (@categoryId, @courseId)
                            END";
                    } else {
                        query = @"IF EXISTS (SELECT * FROM MpCourseCategory CC WHERE CC.CategoryId = @categoryId AND CC.CourseId = @courseId)
                            BEGIN
	                            DELETE FROM MpCourseCategory WHERE CategoryId = @categoryId AND CourseId = @courseId
                            END";
                    }
                    query = query.Replace("@categoryId", cat.CategoryId.ToString()).Replace("@courseId", cat.CourseId.ToString());
                    courseSessions = new Course(this.connection);
                    courseSessions.LinkCtegories(query);
                }

            } catch (Exception ex) { }
        }
                
    }
}
