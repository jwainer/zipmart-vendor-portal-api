﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Web;

namespace VendorApi.Models {
    public class Course : Library.Data.Transaction {

        #region " Properties " 

        public int CourseId = 0;
        public long CourseInstanceId = 0;
        public string Name = "";
        public int ImageId = 0;
        public string ImageURL = "";
        public string Summary = "";
        public string Description = "";
        public float Hours = 0;
        public float Price = 0;
        public int FormatId = 0;
        public string Curriculum = "";
        public bool Visible = false;
        public bool Active = false;
        public int PublisherId = 0;
        public bool IsBundle = false;
        public string Categories = "";
        public int Duration = 0;
        public string Outline = "";
        public string Disclaimer = "";

        public int RecordCount = 0;

        #endregion

        public Course() : base() { }

        public Course(string connection) : base(connection, "dbo.MpCourse") { return; }

        #region " Public Methods "

        public System.Collections.Generic.List<Course> GetCourseList(string query) {
            System.Collections.Generic.List<Course> courses = new System.Collections.Generic.List<Course>();
            Library.Data.DataSet data = new Library.Data.DataSet(this.connection);
            data.ExecuteStatement(query);
            this.RecordCount = data.RecordCount;
            if (data.EOF == true) { this.Clear(); return courses; }
            while (data.EOF == false) {
                Course course = new Course();
                course.CourseId = int.Parse(data["CourseId"]);
                course.CourseInstanceId = data["CourseInstanceId"] == "" ? 0 : long.Parse(data["CourseInstanceId"]);
                course.Name = data["Name"];
                course.ImageId = data["ImageId"] == "" ? 0 : int.Parse(data["ImageId"]); ;
                course.ImageURL = "https://zipmart.com/Images/Image?id=" + course.ImageId;
                //course.ImageURL = "http://igipess1.du.ac.in:8080/web/img/courses1.jpg";
                course.Summary = data["Summary"];
                course.Description = data["Description"];
                course.Price = data["Price"] == "" ? 0 : float.Parse(data["Price"]);
                course.Hours = data["Hours"] == "" ? 0 : float.Parse(data["Hours"]);
                course.FormatId = data["FormatId"] == "" ? 0 : int.Parse(data["FormatId"]);
                course.Curriculum = data["Curriculum"];
                course.Visible = data["Visible"] == "" ? false : bool.Parse(data["Visible"]);
                course.Active = data["Active"] == "" ? false : bool.Parse(data["Active"]);
                course.PublisherId = data["PublisherId"] == "" ? 0 : int.Parse(data["PublisherId"]);
                course.IsBundle = data["IsBundle"] == "" ? false : bool.Parse(data["IsBundle"]);
                course.Categories = data["Categories"];
                course.Duration = data["Duration"] == "" ? 0 : int.Parse(data["Duration"]);
                //course.Outline = data["Outline"];
                //course.Disclaimer = data["Disclaimer"];

                course.RecordCount = data.RecordCount;
                courses.Add(course);
                data.MoveNext();
            }
            return courses;
        }

        public System.Collections.Generic.List<Instructor> GetInstructorList(string query) {
            System.Collections.Generic.List<Instructor> instructors = new System.Collections.Generic.List<Instructor>();
            Library.Data.DataSet data = new Library.Data.DataSet(this.connection);
            data.ExecuteStatement(query);
            this.RecordCount = data.RecordCount;
            if (data.EOF == true) { this.Clear(); return instructors; }
            while (data.EOF == false) {
                Instructor instructor = new Instructor();
                instructor.InstructorId = int.Parse(data["InstructorId"]);
                instructor.Name = data["FirstName"] + " " + data["LastName"];

                instructor.RecordCount = data.RecordCount;
                instructors.Add(instructor);
                data.MoveNext();
            }
            return instructors;
        }

        public void CreateCourse(Course course) {
            Library.Data.DataSet data = new Library.Data.DataSet(this.connection);
            string query = string.Format("INSERT INTO MpCourse (Name,Summary,Description,Hours,FormatId,Visible,Active) VALUES ('{0}','{1}','{2}',{3},{4},{5},{6});",
            course.Name.Replace("'", @"\''"), course.Summary.Replace("'", @"\''"), course.Description.Replace("'", @"\''"), course.Hours, course.FormatId, (course.Visible == true ? 1 : 0), (course.Active == true ? 1 : 0));
            data.ExecuteStatement(query);
        }

        public void SaveCourse(Course course) {
            Library.Data.DataSet data = new Library.Data.DataSet(this.connection);
            string query = string.Format("UPDATE MpCourse SET Name='{0}',Summary='{1}',Description='{2}',Hours={3},FormatId={4},Visible={5},Active={6} Where CourseId={7};",
            course.Name.Replace("'", @"\''"), course.Summary.Replace("'", @"\''"), course.Description.Replace("'", @"\''"), course.Hours, course.FormatId, (course.Visible == true ? 1 : 0), (course.Active == true ? 1 : 0), course.CourseId);
            data.ExecuteStatement(query);
        }

        public List<CourseCategories> GetSelectedCourseCategories(int courseId) {

            string query = @"SELECT C.CourseId, MC.* FROM MpCourse C LEFT JOIN MpCourseCategory CC ON C.CourseId = CC.CourseId
                            LEFT JOIN MpCategory MC ON CC.CategoryId = MC.CategoryId WHERE C.CourseId = " + courseId;

            System.Collections.Generic.List<CourseCategories> selectedCategories = new System.Collections.Generic.List<CourseCategories>();
            Library.Data.DataSet data = new Library.Data.DataSet(this.connection);
            data.ExecuteStatement(query);
            this.RecordCount = data.RecordCount;
            if (data.EOF == true) { this.Clear(); return selectedCategories; }
            while (data.EOF == false) {
                CourseCategories category = new CourseCategories();
                category.CourseId = int.Parse(data["CourseId"]);
                category.CategoryId = int.Parse(data["CategoryId"]);
                category.Name = data["Name"];
                category.ParentId = data["ParentId"] == "" ? 0 : int.Parse(data["ParentId"]);

                category.RecordCount = data.RecordCount;
                selectedCategories.Add(category);
                data.MoveNext();
            }

            return selectedCategories;
        }

        public List<CourseCategories> GetAllCourseCategories() {

            string query = @"SELECT * FROM MpCategory ORDER BY CategoryId, ParentId";

            System.Collections.Generic.List<CourseCategories> allCategories = new System.Collections.Generic.List<CourseCategories>();
            Library.Data.DataSet data = new Library.Data.DataSet(this.connection);
            data.ExecuteStatement(query);
            this.RecordCount = data.RecordCount;
            if (data.EOF == true) { this.Clear(); return allCategories; }
            while (data.EOF == false) {
                CourseCategories category = new CourseCategories();
                category.CategoryId = int.Parse(data["CategoryId"]);
                category.Name = data["Name"];
                category.ParentId = data["ParentId"] == "" ? 0 : int.Parse(data["ParentId"]);

                category.RecordCount = data.RecordCount;
                allCategories.Add(category);
                data.MoveNext();
            }

            return allCategories;
        }

        public void LinkCtegories(string query) {
            Library.Data.DataSet data = new Library.Data.DataSet(this.connection);
            data.ExecuteStatement(query);
        }

        #endregion
    }

    public class Instructor {

        public int InstructorId { get; set; }
        public string Name { get; set; }
        public int RecordCount = 0;

    }

    public class CourseCategories {
        public int CourseId { get; set; }
        public int CategoryId { get; set; }
        public string Name { get; set; }
        public int? ParentId { get; set; }
        public bool Selected { get; set; }
        public int RecordCount = 0;
    }
}