﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace VendorApi.Models {
    public class CourseInstance : Library.Data.Transaction {

        #region " Properties " 

        public int CourseId = 0;
        public string Name = "";
        public long CourseInstanceId = 0;
        public object InstructorId = null;
        public float Price = 0;
        public string StartDate = "";
        public string EndDate = "";
        public bool Active = false;
        public bool IsSessionSold = true;

        public string[] DaysOfWeek;
        public string[] TimeSlots;

        public int RecordCount = 0;

        #endregion

        public CourseInstance() : base() { }

        public CourseInstance(string connection) : base(connection, "dbo.MpCourseInstance") { return; }

        #region " Public Methods "

        public System.Collections.Generic.List<CourseInstance> GetCourseInstanceList(string query) {
            System.Collections.Generic.List<CourseInstance> courseInstances = new System.Collections.Generic.List<CourseInstance>();
            Library.Data.DataSet data = new Library.Data.DataSet(this.connection);
            data.ExecuteStatement(query);
            this.RecordCount = data.RecordCount;
            if (data.EOF == true) { this.Clear(); return courseInstances; }
            while (data.EOF == false) {
                CourseInstance courseInstance = new CourseInstance();
                courseInstance.CourseId = int.Parse(data["CourseId"]);
                courseInstance.Name = data["Name"];
                courseInstance.CourseInstanceId = data["CourseInstanceId"] == "" ? 0 : long.Parse(data["CourseInstanceId"]);
                courseInstance.InstructorId = data["InstructorId"] == "" ? 0 : int.Parse(data["InstructorId"]);
                courseInstance.Price = data["Price"] == "" ? 0 : float.Parse(data["Price"]);
                courseInstance.StartDate = data["StartDate"];
                courseInstance.EndDate = data["EndDate"];
                courseInstance.Active = data["Active"] == "" ? false : bool.Parse(data["Active"]);
                courseInstance.IsSessionSold = data["IsSessionSold"] == "" ? false : bool.Parse(data["IsSessionSold"]);

                courseInstance.RecordCount = data.RecordCount;
                courseInstances.Add(courseInstance);
                data.MoveNext();
            }
            return courseInstances;
        }

        public void SaveCourseInstance(CourseInstance courseInstance) {
            Library.Data.DataSet data = new Library.Data.DataSet(this.connection);
            string query = "";
            if (courseInstance.CourseInstanceId > 0) {
                query = string.Format("UPDATE MpCourseInstance SET CourseId={0},Price={1},StartDate='{2}',EndDate='{3}',Active={4},IsSessionSold={5} Where CourseInstanceId={6};",
                courseInstance.CourseId, courseInstance.Price, courseInstance.StartDate, courseInstance.EndDate, (courseInstance.Active == true ? 1 : 0), (courseInstance.IsSessionSold == true ? 1 : 0), courseInstance.CourseInstanceId);
            } else {
                query = string.Format("INSERT INTO MpCourseInstance (CourseId,Price,StartDate,EndDate,Active,IsSessionSold) VALUES ({0},{1},'{2}','{3}','{4}',{5});",
                courseInstance.CourseId, courseInstance.Price, courseInstance.StartDate, courseInstance.EndDate, (courseInstance.Active == true ? 1 : 0), (courseInstance.IsSessionSold == true ? 1 : 0));
            }
            data.ExecuteStatement(query);
        }

        public void SaveCourseInstances(CourseInstance courseInstance, List<DateTime> instanceStartDates) {
            Library.Data.DataSet data = new Library.Data.DataSet(this.connection);
            foreach (var instanceStartDate in instanceStartDates) {
                string query = string.Format("INSERT INTO MpCourseInstance (CourseId,InstructorId,Price,StartDate,EndDate,Active,IsSessionSold) VALUES ({0},{1},{2},'{3}','{4}',{5},{6});",
                courseInstance.CourseId, courseInstance.InstructorId, courseInstance.Price, instanceStartDate, courseInstance.EndDate, (courseInstance.Active == true ? 1 : 0), (courseInstance.IsSessionSold == true ? 1 : 0));
                data.ExecuteStatement(query);
            }
        }

        public System.Collections.Generic.List<Course> GetCourseList(string query) {
            System.Collections.Generic.List<Course> courses = new System.Collections.Generic.List<Course>();
            Library.Data.DataSet data = new Library.Data.DataSet(this.connection);
            data.ExecuteStatement(query);
            this.RecordCount = data.RecordCount;
            if (data.EOF == true) { this.Clear(); return courses; }
            while (data.EOF == false) {
                Course course = new Course();
                course.CourseId = int.Parse(data["CourseId"]);
                course.Name = data["Name"];

                course.RecordCount = data.RecordCount;
                courses.Add(course);
                data.MoveNext();
            }
            return courses;
        }

        #endregion
    }
}